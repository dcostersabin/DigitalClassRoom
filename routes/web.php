<?php
use Illuminate\Support\Facades\Route;
Route::get('/', function () {
    return view('auth.login');
});
Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
Route::get('/create/{csrf}','PostController@create');
Route::get('/user/profile/{id}/{csrf}','UserInfoController@profile');
Route::post('/post/create/','PostController@save');
Route::get('/post/showpost/{id}/{csrf}/{userid}','PostController@showPost');
Route::post('/post/reply','PostController@reply');
Route::get('/batch/{batch_id}/{csrf}','PostController@batch');
Route::post('/profile/changeImage','UserInfoController@changeProfileImage');
Route::post('/user/follow/request','UserInfoController@followRequest');
Route::get('/query/search/','PostController@search');
Route::get('/personal/storage/{csrf}','PersonalStorageController@index');
Route::post('/submit/file/personal/storage','PersonalStorageController@store');
Route::post('/download/personal/storage','PersonalStorageController@download');
Route::post('/moveToTrash/personal/storage','PersonalStorageController@moveToTrash');
Route::post('/restore/personal/storage','PersonalStorageController@restore');
Route::post('/delete/personal/storage','PersonalStorageController@remove');
Route::get('/bookmarks/{csrf}/{user_id}','PostController@bookmark');
Route::post('/bookmark/store','PostController@bookmarkStore');
Route::get('/post/notice/college/{csrf}','CollegeController@postNoticeIndex');
Route::post('/post/notice/store','CollegeController@noticeStore');
Route::get('/syllabus/view/{csrf}','CollegeController@viewSyllabusStudent');
Route::get('/syllabus/view/college/{csrf}','CollegeController@viewSyllabusCollege');
Route::get('/edit/subject/college/{csrf}','CollegeController@viewSubject');
Route::post('/add/subject','SuperAdminController@addSubject');
Route::get('/edit/syllabus/{csrf}/{id}','SuperAdminController@editSyllabus')->name('editSyllabus');
Route::get('/generate/form','SuperAdminController@generateForm');
Route::post('/submit/syllabus','SuperAdminController@storeSyllabus');
Route::get('/view/notice/student/{csrf}','CollegeController@viewNoticeStudent');
Route::get('/post/notice/college/view/{csrf}','CollegeController@viewNoticeCollege');
Route::get('/show/notice/{id}/{csrf}','CollegeController@showNoticeDetails');
Route::post('/college/set/semester','CollegeController@updateSemester');
Route::get('/show/syllabus/student/{csrf}/{subject_code}','CollegeController@showSyllabusStudent');
Route::post('/add/teacher/college','SuperAdminController@addTeacher');
Route::get('/teacher/view/syllabus/{csrf}','CollegeController@showSyllabusTeacher');
Route::get('/syllabus/view/teacher/{id}/{csrf}','CollegeController@showSyllabusDetailsTeacher');
Route::post('/add/references','CollegeController@addReferences');
Route::get('/personal/discussion/student/{csrf}/{id}','PostController@studentPersonalDiscussion');
Route::get('/teacher/personal/discussion/{csrf}/{id}','PostController@teacherPersonalDiscussion');
Route::get('/teacher/batch/{id}/{csrf}','PostController@teachersBatch');
Route::post('/set/answer','PostController@setAnswer');
Route::get('/show/notifications/{csrf}','NotificationController@showAll');
Route::post('/markAsRead','NotificationController@markAsRead');
Route::get('/show/teachers/{csrf}','CollegeController@showTeachers');






// ajax requests
Route::post('/ajax/rating','PostController@rating');
Route::post('/ajax/admire','PostController@admire');
Route::post('/ajax/mentorInfo','PostController@mentorInfo');
Route::post('/ajax/mutualfollow','UserInfoController@online');
Route::post('/ajax/vote','PostController@vote');
Route::post('/ajax//notificationCount','NotificationController@count');



