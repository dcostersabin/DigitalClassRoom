<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Redirect;
use App\Followers;

class UserInfoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function profile(Request $request)
    {
        if(isset($request->id) && isset($request->csrf) && strcmp($request->csrf,csrf_token())==0)
        {
            $user_id = $request->id;
            //logged in user
            $auth = auth::user()->id;
            //check if the user is following the requested user or not
            $follow_check = DB::select('select * from followers where user_id = ? and following = ?', [$auth,$user_id]);
            if(count($follow_check)>0)
            {
                $follow = "yes";
            }
            else
            {
                $follow = "no";
            }

            //users public post only visible
            $public_post =DB::select('SELECT users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view ,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id  JOIN post_view_counts on posts.post_id=post_view_counts.post_id  JOIN ratings on ratings.post_id=posts.post_id JOIN reply_counts on reply_counts.post_id = posts.post_id WHERE posts.visibility_id=? AND posts.validity=?  AND users.id=? ORDER BY posts.created_at desc', [1,1,$user_id]);
            $user_info = (DB::select('select users.id,users.type,colleges.college_name,users.name,users.email,users.created_at,users.updated_at,users.email,users.profile_url  from users join colleges on users.college_id=colleges.college_id where users.id = ?', [$user_id]));

            //total post count

            $post_count = DB::select('select count(post_id) as total_post_count from posts where user_id = ?', [$user_id]);

            // total replies

            $replies_count = DB::select('select count(reply_id) as reply from replies where user_id = ?', [$user_id]);

            //total admires

            $admires_count = DB::select('select count(id) as admire from admires where user_id = ?', [$user_id]);

            // admire received

            $admire_received = DB::select('SELECT SUM(count) as received FROM admire_counts JOIN posts on admire_counts.post_id = posts.post_id WHERE posts.user_id = ?', [$user_id]);
            if($admire_received[0]->received == null)
            {
                $admire_received[0]->received = 0;

            }
            // followers count

            $followers_count = DB::select('select count(id) as followers from followers where following = ?', [$user_id]);

            //follwoing count
            $following_count = DB::select('select count(id) as following from followers where user_id = ?', [$user_id]);


            $counts= json_encode(array("total_post_count"=>$post_count[0]->total_post_count,"total_replies_count"=>$replies_count[0]->reply,"total_admires_count"=>$admires_count[0]->admire,"total_admire_received"=>$admire_received[0]->received,"followers_count"=>$followers_count[0]->followers,"following_count"=>$following_count[0]->following));

            // return $counts;

            return view('Profile.profile')->with('user_info',$user_info[0])->with('public',$public_post)->with('additional_info',$counts)->with('follow',$follow);
        }
        else
        {
            return view('Error.error401');
        }
    }



    public function changeProfileImage(Request $request)
    {
       if(strcmp($request->_token,csrf_token())==0 && (auth::user()->id == $request->userid))
       {
           $this->validate($request,[
               'profile_image'=>['mimes:jpeg,jpg,png,gif','required','max:1000000'],
           ]);


           $image=$request->file('profile_image');
           $filename=auth::user()->email.time().".".$image->getClientOriginalExtension();
           $location=public_path("/assets/media/UserImages/".$filename);
           $databaseurl="/assets/media/UserImages/".$filename;
           Image::make($image)->resize(400,400)->save($location);
           $set = DB::update('update users set profile_url= ? where id = ?', [$databaseurl,auth::user()->id]);

           if($set!=1)
           {
               return Redirect::back()->withErrors(['profile_images', 'Something Went Wrong']);
           }
           else
           {
            return Redirect::back();
           }
       }
       else
       {
           return view('Error.error400');
       }
    }




    public function followRequest(Request $request)
    {

        if(strcmp($request->_token,csrf_token())==0)
        {
            $user_id = $request->user_id;
            $following = $request->following;

            // check if the user exists or not

            $check_1 = DB::select('select * from users where id = ?', [$user_id]);
            $check_2 = DB::select('select * from users where id = ?', [$following]);


            if(count($check_1)>0 and count($check_2)>0)
            {
                // check if the user is already following the user or not

                $check_follow = DB::select('select * from followers where user_id = ? and following = ?', [$user_id,$following]);
                // if following remove the data else insert the new data of the user
                if(count($check_follow)>0)
                {

                    $remove = DB::delete('delete from followers where user_id = ? and following = ?', [$user_id,$following]);

                    if($remove == 1)
                    {
                        return "removed";
                    }
                    else
                    {
                        return "error";
                    }
                }
                else
                {
                    $follower = new Followers();
                    $follower->user_id = $user_id;
                    $follower->following = $following;
                    $follower->save();
                    if($follower->id > 0)
                    {
                        return 'following';
                    }
                    else
                    {
                        return 'error';
                    }
                }
            }
            else
            {
                return "error";
            }
        }
        else
        {
            return 'error';
        }

    }



    public function online(Request $request)
    {


       if(strcmp($request->_token,csrf_token())==0)
       {
           $user_id = $request->user_id;
        $mutualFollowers = DB::select('SELECT users.email,users.name,users.id,users.profile_url,users.logged_in FROM followers join users on followers.following = users.id WHERE followers.user_id in (SELECT followers.following FROM followers WHERE followers.following=?) ORDER BY users.logged_in DESC', [$user_id]);
        return $mutualFollowers;
       }
    }


}
