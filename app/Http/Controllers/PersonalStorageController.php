<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PersonalStorage;
use Illuminate\Support\Facades\Auth;
use DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Redirect;





class PersonalStorageController extends Controller
{
    public function index(Request $request)
    {
        if(strcmp($request->csrf,csrf_token())==0)
        {
            $file = DB::select('select personal_storages.created_at,file_size,file_url,type_name,user_filename,personal_storages.id,icon_url from personal_storages join file_types on personal_storages.icon_id=file_types.id where user_id = ? and validity= ? order by personal_storages.created_at desc', [auth::user()->id,1]);
            $trashed = DB::select('select personal_storages.created_at,file_size,file_url,type_name,user_filename,personal_storages.id,icon_url from personal_storages join file_types on personal_storages.icon_id=file_types.id where user_id = ? and validity= ? order by personal_storages.created_at desc', [auth::user()->id,2]);

            return view('PersonalStorage.personalStorage')->with('file',$file)->with('trashed',$trashed);
        }
        else
        {
            return view('Error.error400');
        }
    }



   public function store(Request $request)
   {
    if(strcmp($request->_token,csrf_token())==0)
    {
        $this->middleware('auth');
    $this->validate($request,[
        'filename'=>'required|min:5|string',
        'attachment'=>'required|mimes:doc,html,file,iso,svg,mp3,pdf,jpg,jpeg,png,pptx,ppt,zip,docx|required|max:26214400'
        ]);


        $file = $request->attachment;
        $user_id = $request->user_id;
        $email = auth::user()->email;
        $extension = $file->extension();
        $ext_id = DB::select('select id from file_types where type_name=?', [$extension]);
        $ext_id = $ext_id[0]->id;
        $userfile_name = $request->filename;
        $path = Storage::putFile('PersonalStorage/'.$email,$request->attachment);
        $bytes = filesize($request->attachment);
        $decimals = 2;
        $sz = 'BKMGTP';
        $factor = floor((strlen($bytes) - 1) / 3);
        $size = sprintf("%.{$decimals}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
        $personal_storage = new PersonalStorage();
        $personal_storage->user_filename=$userfile_name;
        $personal_storage->file_url=$path;
        $personal_storage->icon_id=$ext_id;
        $personal_storage->user_id=$user_id;
        $personal_storage->file_size=$size;
        $personal_storage->save();
        return Redirect::back()->withInput()->withErrors(array('success'=>'Your File Was Uploaded Successfully'));
    }
    else
    {
        return view('Error.error400');
    }
   }


   public function download(Request $request)
   {

        if(strcmp($request->_token,csrf_token())==0)
        {
            $file = DB::select('select * from personal_storages where id = ?', [$request->id]);
            $url =$file[0]->file_url;
            return Storage::download($url,$file[0]->user_filename);
        }
        else
        {
            return view('Error.error400');
        }
   }


   public function moveToTrash(Request $request)
   {
       if(strcmp($request->_token,csrf_token())==0)
       {
            $check = DB::select('select * from personal_storages where id = ?', [$request->id]);

            if(count($check)>0)
            {
                // validity as 2 to determine as it was moved to trash
                $trash = DB::update('update personal_storages set validity = ? where id = ?', [2,$request->id]);

               if($trash == 1)
               {
                return Redirect::back()->withInput()->withErrors(array('trash'=>'Your Post Was Successfully Moved To Trash'));
               }
               else
               {
                return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
               }
            }
            else
            {
                return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
            }
       }
       else
       {
        return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
       }
   }


   public function restore(Request $request)
   {

    if(strcmp($request->_token,csrf_token())==0)
    {
         $check = DB::select('select * from personal_storages where id = ?', [$request->id]);

         if(count($check)>0)
         {
             // validity as 1 to determine as it was moved away from trash
             $trash = DB::update('update personal_storages set validity = ? where id = ?', [1,$request->id]);

            if($trash == 1)
            {

             return Redirect::back()->withInput()->withErrors(array('trashRemoved'=>'Your Post Was Successfully Removed From Trash'));
            }
            else
            {
             return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
            }
         }
         else
         {
             return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
         }
    }
    else
    {
     return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
    }

   }




   public function remove(Request $request)
   {

    if(strcmp($request->_token,csrf_token())==0)
    {
         $check = DB::select('select * from personal_storages where id = ?', [$request->id]);

         if(count($check)>0)
         {
             // validity as 1 to determine as it was moved away from trash
             $trash = DB::update('update personal_storages set validity = ? where id = ?', [0,$request->id]);

            if($trash == 1)
            {

             return Redirect::back()->withInput()->withErrors(array('trashRemoved'=>'Your Post Was Successfully Deleted From The Database'));
            }
            else
            {
             return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
            }
         }
         else
         {
             return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
         }
    }
    else
    {
     return Redirect::back()->withInput()->withErrors(array('error'=>'Opps Something Went Wrong'));
    }

   }

}
