<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;
use Image;
use App\Post;
use Mews\Purifier\Facades\Purifier;
use Mews\Purifier\Purifier as MewsPurifier;
use App\PostKeyword;
use App\PostAttachment;
use App\PostCategory;
use PHPUnit\Runner\Exception;
use App\PostTags;
use Illuminate\Support\Facades\Redirect;
use App\AskMentor;
use App\AdmireCounts;
use App\PostView;
use App\PostViewCount;
use App\RatedUser;
use App\Rating;
use App\Admires;
use App\ReplyCount;
use App\Reply;
use App\ReplyAttachments;
use App\User;
use App\Bookmark;
use App\ReplyVote;
use App\ReplyVotedUsers;

class PostController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }


    public function create(Request $request)
    {


        if(isset($request->csrf) && strcmp($request->csrf,csrf_token())==0)
        {


              //total post count

              $post_count = DB::select('select count(post_id) as total_post_count from posts ', []);

              // total replies

              $replies_count = DB::select('select count(reply_id) as reply from replies', []);

              //total admires

              $admires_count = DB::select('select count(id) as admire from admires', []);

              // admire received


              $counts= json_encode(array("total_post_count"=>$post_count[0]->total_post_count,"total_replies_count"=>$replies_count[0]->reply,"total_admires_count"=>$admires_count[0]->admire));

            $category = DB::select('select category_name from post_categories');
            return view('Post.createPost')->with('categories',$category)->with('additional_info',$counts);
        }
        else
        {
            return view('Error.error401');
        }
    }


    public function save(Request $request)
    {


        if(isset($request->_token) && strcmp($request->_token,csrf_token())==0)
        {
            // return $request;
            try{

                    $this->validate($request,[
                        'post_title'=>['required','min:3','max:50','string'],
                        'post_category'=>['required','min:1','string'],
                        'post_keyword'=>['required','min:3','max:50','string'],
                        'post_data'=>['required','min:25','string'],
                        'post_tags'=>['required','min:3','string','max:100'],
                        'visibility'=>['required','min:1','max:8','string'],
                        'attachment'=>['mimes:jpeg,jpg,png,gif','max:2097152'],
                        'mentor'=>['string','email'],
                        ]);



                    //clean all inputs for xss protection
                    $user_id = $request->user_id;
                    $clean_title = Purifier::clean($request->post_title);
                    $clean_keyword = Purifier::clean($request->post_keyword);
                    $clean_data = Purifier::clean($request->post_data);
                    $clean_tags = Purifier::clean($request->post_tags);
                    $clean_visibility = Purifier::clean($request->visibility);
                    $category = $request->post_category;
                    // validate the data affter they are purified
                    $final_request = new \Illuminate\Http\Request();
                    $final_request->setMethod('POST');
                    $final_request->replace(["post_title"=>$clean_title,"post_keyword"=>$clean_keyword,"post_data"=>$clean_data]);
                    // return $final_request;
                    $this->validate($final_request,[
                        'post_title'=>['required','min:3','max:50','string'],
                        'post_keyword'=>['required','min:3','max:50','string'],
                        'post_data'=>['required','min:25','string'],
                        ]);
                    $post = new Post();
                    // set user id
                    $post->user_id = $user_id;
                    //find keyword id if key doesnt exists insert into table post_keywords
                    $keyword = DB::select('select keyword_id from post_keywords where keyword_name like ?', [$clean_keyword]);
                    if(count($keyword)>0)
                    {
                        $post->keyword_id = $keyword[0]->keyword_id;
                    }
                    else
                    {
                        $keyword_new = new PostKeyword();
                        $keyword_new->keyword_name = $clean_keyword;
                        $keyword_new->save();
                        $post->keyword_id = $keyword_new->id;
                    }
                    if(isset($request->attachment))
                    {
                        // if attachment exist resize and save to PostImages folder inside assets
                        $image=$request->file('attachment');
                        $filename="postimg".time().".".$image->getClientOriginalExtension();
                        $location=public_path("assets/media/PostImages/".$filename);
                        $databaseurl="assets/media/PostImages/".$filename;
                        Image::make($image)->resize(800,400)->save($location);
                        $attachment_image = new PostAttachment;
                        $attachment_image->attachment_url=$databaseurl;
                        $attachment_image->save();
                        $post->attachment_id=$attachment_image->id;
                    }
                    else
                    {
                        // since no attachment id is set to 1 for references check database or set id as per empty url of attachment
                        $post->attachment_id = 1;
                    }
                    // find category id and insert
                    $category  = DB::select('select * from post_categories where category_name like ?',[$category]);
                    if(count($category)>0)
                    {
                        $post->category_id = $category[0]->category_id;
                    }
                    else
                    {
                        throw new Exception("Invalid Category id");
                    }
                    $post->post_data = $clean_data;
                    $post->post_title = $clean_title;

                    // insert tags individually
                    $tags = new PostTags();
                    $tags->tag_name = $clean_tags;
                    $tags->save();
                    $post->tag_id = $tags->id;

                    // select visibility id

                    $visibility = DB::select('select * from post_visibilities where visibility like  ?', [strip_tags($clean_visibility)]);
                    if(count($visibility)>0)
                    {
                        $post->visibility_id = $visibility[0]->visibility_id;
                    }
                    else
                    {
                        throw new Exception("Visibility Now Found");
                    }




                    if($post->visibility_id == 3)
                    {
                        if(isset($request->mentor))
                        {
                            $this->validate($request,[
                                'mentor'=>['email','required','string'],
                            ]);
                            // check if the mentor is teacher or not
                            $check_teacher = DB::select('select id from users where email = ? and type= ?', [$request->mentor,'TEACHER']);
                            if(!(count($check_teacher)>0))
                            {

                                return redirect()->back()->withErrors(array('mentor'=>'The Mentor Assined Is Not A Teacher'))->withInput();

                            }
                            $post->save();
                            $admire_count = new AdmireCounts();
                            $admire_count->post_id = $post->id;
                            $admire_count->save();
                            $post_view_count = new PostViewCount();
                            $post_view_count->post_id = $post->id;
                            $post_view_count->save();
                            $rating = new Rating();
                            $rating->post_id = $post->id;
                            $rating->save();
                            $reply_count = new ReplyCount();
                            $reply_count->post_id = $post->id;
                            $reply_count->save();
                            $mentor = DB::select('select id from users where email = ?', [$request->mentor]);
                            $assign_mentor = new AskMentor();
                            $assign_mentor->post_id = $post->id;
                            $assign_mentor->asked_to = $mentor[0]->id;
                            $assign_mentor->save();
                            if($assign_mentor->id >0 && $post->id >0 && $admire_count->id > 0 && $post_view_count->id > 0 && $rating->id > 0 && $reply_count->id > 0)
                            {
                                return Redirect::back()->withInput()->withErrors(array('posted'=>'Your Post Was Successfully Posted'));

                            }
                            else
                            {
                                throw new Exception("Invalid Input");
                            }
                        }
                        else
                        {
                            return Redirect::back()->withInput()->withErrors(array('mentor'=>'Please Select A Mentor'));

                        }
                    }else{
                        $post->save();
                        $admire_count = new AdmireCounts();
                        $admire_count->post_id = $post->id;
                        $admire_count->save();
                        $post_view_count = new PostViewCount();
                        $post_view_count->post_id = $post->id;
                        $post_view_count->save();
                        $rating = new Rating();
                        $rating->post_id = $post->id;
                        $rating->save();
                        $reply_count = new ReplyCount();
                        $reply_count->post_id = $post->id;
                        $reply_count->save();
                        if($post->id >0 && $admire_count->id > 0 && $post_view_count->id > 0 && $rating->id > 0 && $reply_count->id > 0)
                        {
                            return Redirect::back()->withInput()->withErrors(array('posted'=>'Your Post Was Successfully Posted'));

                        }
                        else
                        {
                            throw new Exception("Invalid Input");

                        }
                    }













            }
            catch(Exception $e)
            {
                return view('Error.error400');
            }
        }
        else
        {
            return view('Error.error401');
        }
    }


    public function mentorInfo(Request $request)
    {

       if(strcmp($request->_token,csrf_token())==0)
       {
        $query = $request->email;

        $lower = trim(strtolower($query));
        if(strcmp($lower,"")==0)
        {
            return "null";
        }
        $final = explode(' ',$lower);
        $str="";
        $i=0;
        $count = count($final);


        foreach ($final as $key => $value) {
            if($i==($count-1))
            {
                $str=$str.' email LIKE "%'.$value.'%"  limit 1';
            }
            else
            {
                $str=$str.' email LIKE "%'.$value.'% or " ';
            }

            $i++;
        }


        $data = DB::select('select profile_url,id,name,email from users where'.$str);

        if(count($data)>0)
        {
            return json_encode($data[0]);
        }

        return "null";

    }
    else
    {
        return null;
    }
}


    public function showPost(Request $request)
    {
        if(strcmp($request->csrf,csrf_token())==0 && ($request->userid == auth::user()->id) )
        {

            $post = json_encode(DB::select('SELECT  users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id JOIN post_view_counts on posts.post_id=post_view_counts.post_id JOIN ratings on ratings.post_id=posts.post_id  JOIN reply_counts on reply_counts.post_id = posts.post_id WHERE posts.post_id= ? LIMIT 1', [$request->id]));
            if(count(json_decode($post))>0)
            {
                // check if the post is bookmarked or not
                $bookmark = DB::select('select * from bookmarks where post_id = ? and user_id = ?', [$request->id,$request->userid]);
                // if count is greter than 0 then it is bookmarked
                if(count($bookmark)>0)
                {
                    $bookmark = 1 ;
                }
                else
                {
                    $bookmark = 0 ;
                }
                $temp = json_decode($post);

                if(strcmp(($temp[0]->visibility),"public")==0)
                {


                    $replies = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by replies.created_at desc', [$request->id]));
                    //best voted reply
                    $voted_reply = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by reply_votes.votes desc limit 1', [$request->id]));
                    //best mentors answer
                    $answer = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ?  and replies.answer= ? order by replies.created_at desc limit 1', [$request->id,1]));

                    //check if the post is viewed or not otherwise insert into post viewed
                    $view_check = DB::select('select * from post_views where post_id = ? and user_id=? limit 1', [$request->id,$request->userid]);
                    if(count($view_check)>0)
                    {

                        return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                    }
                    else
                    {

                        // new instance created as the post is viewed
                        $post_view = new PostView();
                        $post_view->user_id = $request->userid;
                        $post_view->post_id = $request->id;
                        $post_view->save();
                        $temp = json_decode($post);
                        $post_view_count = $temp[0]->post_view +1;
                        $update_post_view = DB::update('update post_view_counts set count = ? where post_id = ?', [$post_view_count,$request->id]);


                        if($post_view->id > 0 && $update_post_view > 0)
                        {
                            // view successfully updated

                            return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                        }
                        else
                        {
                            return view('Error.error401');
                        }
                    }


                }
                // if the post is private that is only shown to particular batch user
                else if(strcmp($temp[0]->visibility,"private")==0)
                {

                    //check the id of posted user with current logged in user
                    $check = DB::select('select batch_id from users where id = ?', [$temp[0]->id]);

                    if(auth::user()->batch_id != $check[0]->batch_id)
                    {
                        return view('Error.error401');
                    }
                    // id of the user sent from the request
                    $user_sent_id = $request->userid;
                    $check_user_sent = DB::select('select batch_id from users where id = ?', [$user_sent_id]);
                    if(count($check)>0 && count($check_user_sent)>0)
                    {
                        if($check_user_sent[0]->batch_id == $check[0]->batch_id)
                        {
                            $replies = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by replies.created_at desc', [$request->id]));
                               //best voted reply
                          $voted_reply = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by reply_votes.votes desc limit 1', [$request->id]));
                            //best mentors answer
                            $answer = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ?  and replies.answer= ? order by replies.created_at desc limit 1', [$request->id,1]));


                            //check if the post is viewed or not otherwise insert into post viewed
                            $view_check = DB::select('select * from post_views where post_id = ? and user_id=? limit 1', [$request->id,$request->userid]);
                            if(count($view_check)>0)
                            {

                                return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                            }
                            else
                            {
                                // new instance created as the post is viewed
                                $post_view = new PostView();
                                $post_view->user_id = $request->userid;
                                $post_view->post_id = $request->id;
                                $post_view->save();
                                $temp = json_decode($post);
                                $post_view_count = $temp[0]->post_view +1;
                                $update_post_view = DB::update('update post_view_counts set count = ? where post_id = ?', [$post_view_count,$request->id]);


                                if($post_view->id >0 && $update_post_view > 0)
                                {
                                    // view successfully updated

                                    return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                                }
                                else
                                {
                                    return view('Error.error401');
                                }
                            }



                        }
                        else
                        {
                            return view('Error.error400');
                        }
                    }
                    else
                    {
                        return view('Error.error400');
                    }
                }


                             // if the post is private that is only shown to particular batch user
                             else if(strcmp($temp[0]->visibility,"personal")==0 && auth::user()->id == $request->userid)
                             {

                                    if(strcmp(auth::user()->type,"STUDENT")==0)
                                    {
                                                   //check the id of posted user with current logged in user



                                 // id of the user sent from the request
                                 $user_sent_id = $request->userid;
                                 $check_user_sent = DB::select('select id from users where id = ?', [$user_sent_id]);
                                 if( count($check_user_sent)>0)
                                 {
                                     if($check_user_sent[0]->id == auth::user()->id)
                                     {
                                         $replies = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by replies.created_at desc', [$request->id]));
                                            //best voted reply
                                       $voted_reply = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by reply_votes.votes desc limit 1', [$request->id]));
                                         //best mentors answer
                                         $answer = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ?  and replies.answer= ? order by replies.created_at desc limit 1', [$request->id,1]));


                                         //check if the post is viewed or not otherwise insert into post viewed
                                         $view_check = DB::select('select * from post_views where post_id = ? and user_id=? limit 1', [$request->id,$request->userid]);
                                         if(count($view_check)>0)
                                         {

                                             return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                                         }
                                         else
                                         {
                                             // new instance created as the post is viewed
                                             $post_view = new PostView();
                                             $post_view->user_id = $request->userid;
                                             $post_view->post_id = $request->id;
                                             $post_view->save();
                                             $temp = json_decode($post);
                                             $post_view_count = $temp[0]->post_view +1;
                                             $update_post_view = DB::update('update post_view_counts set count = ? where post_id = ?', [$post_view_count,$request->id]);


                                             if($post_view->id >0 && $update_post_view > 0)
                                             {
                                                 // view successfully updated

                                                 return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                                             }
                                             else
                                             {
                                                 return view('Error.error401');
                                             }
                                         }



                                     }
                                     else
                                     {
                                         return view('Error.error400');
                                     }
                                 }
                                 else
                                 {
                                     return view('Error.error400');
                                 }
                                    }
                                    else if(strcmp(auth::user()->type,"TEACHER")==0)
                                    {



                            //check the id of posted user with current logged in user
                            $check = DB::select('select post_id,asked_to from ask_mentors where asked_to = ? and post_id ', [auth::user()->id,$request->id]);


                            // id of the user sent from the request
                            $user_sent_id = $request->userid;
                            $check_user_sent = DB::select('select id from users where id = ? and type =  ? ', [$user_sent_id,"TEACHER"]);
                            if(count($check)>0 && count($check_user_sent)>0)
                            {
                                if($check_user_sent[0]->id == $check[0]->asked_to)
                                {
                                    $replies = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by replies.created_at desc', [$request->id]));
                                       //best voted reply
                                  $voted_reply = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ? order by reply_votes.votes desc limit 1', [$request->id]));
                                    //best mentors answer
                                    $answer = json_encode(DB::select('select replies.reply_id,votes,users.id,users.name,users.email,users.profile_url,replies.post_id,replies.reply_data,replies.attachment_id,reply_attachments.attachment_url,replies.created_at from replies join users on replies.user_id = users.id join reply_attachments on replies.attachment_id = reply_attachments.attachment_id join reply_votes on reply_votes.reply_id = replies.reply_id where replies.post_id = ?  and replies.answer= ? order by replies.created_at desc limit 1', [$request->id,1]));


                                    //check if the post is viewed or not otherwise insert into post viewed
                                    $view_check = DB::select('select * from post_views where post_id = ? and user_id=? limit 1', [$request->id,$request->userid]);
                                    if(count($view_check)>0)
                                    {

                                        return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                                    }
                                    else
                                    {
                                        // new instance created as the post is viewed
                                        $post_view = new PostView();
                                        $post_view->user_id = $request->userid;
                                        $post_view->post_id = $request->id;
                                        $post_view->save();
                                        $temp = json_decode($post);
                                        $post_view_count = $temp[0]->post_view +1;
                                        $update_post_view = DB::update('update post_view_counts set count = ? where post_id = ?', [$post_view_count,$request->id]);


                                        if($post_view->id >0 && $update_post_view > 0)
                                        {
                                            // view successfully updated

                                            return view('Post.showpost')->with('post',$post)->with('replies',$replies)->with('bookmark',$bookmark)->with('bestVoted',$voted_reply)->with('answer',$answer);

                                        }
                                        else
                                        {
                                            return view('Error.error401');
                                        }
                                    }



                                }
                                else
                                {
                                    return view('Error.error400');
                                }
                            }
                            else
                            {
                                return view('Error.error400');
                            }




                                    }
                                    else
                                    {
                                        return view('Error.error401');
                                    }
                             }
                             else
                             {
                                 return view('Error.error400');
                             }

            }
            else
            {
                return view('Error.error400');
            }

        }
        else
        {
            return view('Error.error401');
        }
    }


    public function rating(Request $request)
    {
       if(strcmp($request->_token,csrf_token())==0)
       {
           //check if the user and post id is valid or not
            $user = DB::select('select * from users where id = ?', [$request->userid]);
            $post = DB::select('select * from posts where post_id = ?', [$request->postid]);
            if(count($user)>0 && count($post)>0)
            {
                //check if the user has already rated the post or not
                $check = DB::select('select * from rated_users where user_id = ? and post_id = ?', [$request->userid,$request->postid]);
                //if rated update else create new instances
                if(count($check)>0)
                {
                    //previous rating given
                    $user_rate_given = (int) $check[0]->rating_given;
                    $rating = (int) $request->rating;
                    // dont update if the new rating given is same as previous
                    if($user_rate_given == $rating)
                    {
                        return "success";
                    }
                    else
                    {
                        // update both rating given by user and posts rating
                         $current_rating = DB::select('select * from ratings where post_id = ?', [$request->postid]);
                         $current_post_rating = $current_rating[0]->rating;
                         $updated = $current_post_rating - $user_rate_given;
                         $first_update = DB::update('update ratings set rating = ? where post_id = ?', [$updated,$request->postid]);
                         if($first_update > 0)
                         {
                             $after_update = DB::select('select * from ratings where post_id = ?', [$request->postid]);
                             $current_rating_after_update = $after_update[0]->rating;
                             $final_rating = $current_rating_after_update + $request->rating;
                             $final_updtae_post = DB::update('update ratings set rating = ? where post_id = ?', [$final_rating,$request->postid]);
                             $final_user_update = DB::update('update rated_users set rating_given = ? where post_id = ? and user_id =?', [$rating,$request->postid,$request->userid]);
                             if($final_updtae_post > 0 && $final_user_update > 0)
                             {
                                 //updated successfullt
                                 return "success";

                             }
                             else
                             {
                                 return "failed";
                             }
                         }
                         else
                         {
                            return "failed";
                         }
                    }

                }
                else
                {
                    //creating new instances for unrated post by the user
                    $rated_user = new RatedUser();
                    $rated_user->post_id = $request->postid;
                    $rated_user->user_id = $request->userid;
                    $rated_user->rating_given = $request->rating;
                    $rated_user->save();
                    $rating = DB::select('select * from ratings where post_id = ? ', [$request->postid]);
                    if(count($rating)>0)
                    {
                           $rating_value = $rating[0]->rating + $request->rating;
                           $db_update = DB::update('update ratings set rating = ? where id = ?', [$rating_value,$rating[0]->id]);
                           if($db_update > 0)
                           {
                               //new instance created successfully
                               return "success";
                           }
                    }
                    else
                    {
                        return "failed";
                    }
                }

            }
            else
            {
                return "failed";
            }

       }
       else
       {
            return "failed";
       }


    }

    public function admire(Request $request)
    {
       if(strcmp($request->_token,csrf_token())==0)
       {
            // checking if the user and post id is valid or not

            $post_check = DB::select('select * from posts where post_id = ?', [$request->postid]);
            $user_check = DB::select('select * from users where id = ?', [$request->userid]);
            if(count($post_check)>0 && count($user_check)>0)
            {
               // check if the user has already admired or not if not add otherwise remove
               $admire_check = DB::select('select * from admires where post_id = ? and user_id=?', [$request->postid,$request->userid]);
               if(count($admire_check)>0)
               {
                $delete_admire = DB::delete('delete from admires where user_id = ? and post_id =?', [$request->userid,$request->postid]);
                $get_admire = DB::select('select * from admire_counts where post_id = ? limit ?', [$request->postid,1]);
                $current_admire_count = $get_admire[0]->count;
                $updated_admire_count = $current_admire_count - 1;
                $update_table = DB::update('update admire_counts set count = ? where post_id = ?', [$updated_admire_count,$request->postid]);
                if($delete_admire > 0 && $update_table > 0)
                {
                    return "removed";
                }
                else
                {
                    return "failed";
                }
               }
               else
               {
                   $admire = new Admires();
                   $admire->user_id = $request->userid;
                   $admire->post_id = $request->postid;
                   $admire->save();
                   $get_admire = DB::select('select * from admire_counts where post_id = ? limit ?', [$request->postid,1]);
                    $current_admire_count = $get_admire[0]->count;
                    $updated_admire_count = $current_admire_count + 1;
                    $update_table = DB::update('update admire_counts set count = ? where post_id = ?', [$updated_admire_count,$request->postid]);
                    if($admire->id > 0 && $update_table > 0)
                    {
                        return "admired";
                    }
                    else
                    {
                        return "failed";
                    }

               }
            }
            else
            {
                return "failed";
            }
       }
    }


    public static function admireCheck($userid,$postid,$csrf)
    {

        if(strcmp($csrf,csrf_token())==0)
        {
            //check if the user and post id is valid or not
            $user_check = DB::select('select * from users where id = ?', [$userid]);
            $post_check = DB::select('select * from posts where post_id = ?', [$postid]);
            if(count($user_check)>0 && count($post_check)>0)
            {
                $admire_check = DB::select('select * from admires where post_id = ? and user_id', [$postid,$userid]);
                if(count($admire_check)>0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
    }



    public function reply(Request $request)
    {
        if(strcmp($request->_token,csrf_token())==0)
        {


            $this->validate($request,[
                'reply_data'=>['required','string','min:25'],
                'reply_attachment'=>['mimes:jpeg,jpg,png,gif','max:2097152'],
            ]);

                // check if the post and user are valid or not

                $user_check = DB::select('select * from users where id = ?', [$request->userid]);
                $post_check = DB::select('select * from posts where post_id = ?', [$request->postid]);

                if(count($user_check)>0 && count($post_check)>0)
                {
                    // cleaning input data
                 $clean_reply_data = Purifier::clean($request->reply_data);
                 $final_request =   new \Illuminate\Http\Request();
                 $final_request->setMethod('POST');
                 $final_request->replace(["reply_data"=>$clean_reply_data]);
                    // return $final_request;
                    $this->validate($final_request,[
                        'reply_data'=>['required','string','min:25'],
                    ]);
                        $reply = new Reply();

                       if(isset($request->reply_attachment))
                       {

                          $this->validate($request,[
                              'reply_attachment'=>['required','mimes:jpeg,jpg,png,gif','max:2097152'],
                          ]);

                            $reply_attachment = new ReplyAttachments();
                            $image=$request->file('reply_attachment');
                            $filename="replyimg".time().".".$image->getClientOriginalExtension();
                            $location=public_path("assets/media/ReplyImages/".$filename);
                            $databaseurl="assets/media/ReplyImages/".$filename;
                            Image::make($image)->resize(800,400)->save($location);
                            $reply_attachment->attachment_url = $databaseurl;
                            $reply_attachment->save();
                            if($reply_attachment->id > 0)
                            {
                            $reply->reply_data = $clean_reply_data;
                            $reply->user_id = $request->userid;
                            $reply->post_id = $request->postid;
                            $reply->attachment_id = $reply_attachment->id;
                            $reply->save();
                            // incerease reply count
                            $reply_count_check = DB::select('select * from reply_counts where post_id = ?', [$request->postid]);
                            $current_count = $reply_count_check[0]->count;
                            $current_count ++ ;
                            $update = DB::update('update reply_counts set count = ? where id = ?', [$current_count,$reply_count_check[0]->id]);
                            // new instance in reply vote
                            $reply_vote = new ReplyVote();
                            $reply_vote->reply_id = $reply->id;
                            $reply_vote->save();
                            if($reply->id > 0 && $update > 0 && $reply_vote->id > 0)
                            {
                                return Redirect::back()->withInput()->withErrors(array('success'=>'Your Reply Was Successfully Posted Reload To See The Changes'));
                            }
                            else
                            {
                                return view('Error.error400');
                            }


                            }
                            else
                            {
                            return view('Error.error400');
                            }




                       }
                       else
                       {

                           // set reply attachment as 1 for null attachment
                           $reply->reply_data = $clean_reply_data;
                           $reply->attachment_id = 1;
                           $reply->user_id = $request->userid;
                            $reply->post_id = $request->postid;
                            $reply->save();
                           // incerease reply count
                           $reply_count_check = DB::select('select * from reply_counts where post_id = ?', [$request->postid]);
                           $current_count = $reply_count_check[0]->count;
                           $current_count ++ ;
                           $update = DB::update('update reply_counts set count = ? where id = ?', [$current_count,$reply_count_check[0]->id]);
                            // new instance in reply vote
                            $reply_vote = new ReplyVote();
                            $reply_vote->reply_id = $reply->id;
                            $reply_vote->save();
                            if($reply->id >0 && $update > 0 && $reply_vote->id > 0)
                            {
                                return Redirect::back()->withInput()->withErrors(array('success'=>'Your Reply Was Successfully Posted Reload To See The Changes'));
                            }
                            else
                            {
                                return view('Error.error400');
                            }
                       }
                }
                else
                {
                    return view('Error.error400');
                }

        }
    }


    public function batch(Request $request)
    {

        if(strcmp($request->csrf,csrf_token())==0)
        {
            $batch_id = $request->batch_id;
            $batch_post = DB::select('SELECT users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view ,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id  JOIN post_view_counts on posts.post_id=post_view_counts.post_id  JOIN ratings on ratings.post_id=posts.post_id JOIN reply_counts on reply_counts.post_id = posts.post_id WHERE posts.visibility_id=? AND posts.validity=? AND users.batch_id= ? ORDER BY posts.created_at desc', [2,1,$batch_id]);

                return view('Post.batch')->with('batch',$batch_post);
        }
        else
        {
            return view('Error.error400');
        }
    }



    public function search(Request $request)
    {


        $this->validate($request,[
            'queries'=>['required','min:2','string']
        ]);
        $clean_query = Purifier::clean($request->queries);
        //revalidate before executing a query
        $final_request = new  \Illuminate\Http\Request();
        $final_request->setMethod('POST');
        $final_request->replace(["queries"=>$clean_query]);
        $this->validate($final_request,[
            'queries'=>['required','min:2','string']
        ]);

        // search the database if the query is valid

        //divide query into different keywords
        $search_keywords = explode(' ',strip_tags(trim($clean_query)));
        $count = count($search_keywords);

        $str="";
        $i=0;



        foreach ($search_keywords as $key => $value) {
            if($i==($count-1))
            {
                $str=$str.' keyword_name LIKE "%'.$value.'%" or post_title LIKE "%'.$value.'%"'.' or category_name LIKE "%'.$value.'%"'.' or tag_name LIKE"%'.$value.'%"';
            }
            else
            {
                $str=$str.' keyword_name LIKE "%'.$value.'%" or post_title LIKE "%'.$value.'%" or'.'  category_name LIKE "%'.$value.'%"'.' or tag_name LIKE"%'.$value.'%"'.'or';
            }

            $i++;
        }


        $data=DB::select('select users.id,rating,tag_name,admire_counts.count as admirecount,post_view_counts.count as viewcount,reply_counts.count as replycount,users.name,email,profile_url,posts.post_id,posts.created_at,posts.updated_at,posts.user_id,post_title,post_data,post_categories.category_name,attachment_url,post_keywords.keyword_name,posts.attachment_id from posts inner join post_attachments on posts.attachment_id=post_attachments.attachment_id inner join post_categories on posts.category_id=post_categories.category_id
        join post_keywords on posts.keyword_id=post_keywords.keyword_id join users on posts.user_id=users.id  join post_tags on post_tags.tag_id = posts.tag_id join post_view_counts on posts.post_id = post_view_counts.post_id join reply_counts on posts.post_id = reply_counts.post_id join admire_counts on posts.post_id= admire_counts.post_id  join ratings on ratings.post_id = posts.post_id where '.$str.'and posts.visibility_id=?  ORDER BY posts.created_at desc',[1]);


        return view('search.search')->with('post',$data);



    }



    public function bookmark(Request $request)
    {
        if(strcmp($request->csrf,csrf_token())==0 && ($request->user_id== auth::User()->id))
        {
            $bookmarks = DB::select('select post_title,post_data,users.id,users.email,posts.post_id from bookmarks join posts on bookmarks.post_id = posts.post_id  join users on posts.user_id = users.id where bookmarks.user_id = ? order by bookmarks.created_at desc', [auth::User()->id]);
                return view('Post.bookmark')->with('bookmarks',$bookmarks);
        }
        else
        {
            return view('Error.error400');
        }
    }



    public function bookmarkStore(Request $request)
    {
        if(strcmp($request->_token,csrf_token())==0)
        {
            $this->validate($request,[
                'post_id'=>['required','integer'],
                'user_id'=>['required','integer'],
            ]);

            // check if the post is already bookmarked or not

            $check = DB::select('select * from bookmarks where user_id = ? and post_id = ?', [$request->user_id,$request->post_id]);


            // if the returned array is greater than 0 then it is already bookmarked and remove that bookmark else store

            if(count($check)>0)
            {

                // remove the bookmarked

                $delete = DB::delete('delete from bookmarks where post_id = ? and user_id = ?', [$request->post_id,$request->user_id]);
                if($delete > 0)
                {
                    return Redirect::back()->withInput()->withErrors(array('bookmark'=>'This Post Was Successfully Removed From Bookmark'));
                }
                else
                {
                    return view('Error.error401');
                }

            }
            else
            {
                 // create new bookmark instance

                 $bookmark = new Bookmark();
                 $bookmark->post_id = $request->post_id;
                 $bookmark->user_id = $request->user_id;
                 $bookmark->save();
                 if($bookmark->id > 0)
                 {
                     return Redirect::back()->withInput()->withErrors(array('bookmark'=>'This Post Was Successfully Bookmarked'));

                 }
                 else
                 {
                     return view('Error.error401');
                 }

            }
        }
        else
        {
            return view('Error.error401');
        }

    }

    public function vote(Request $request)
    {

        // $this->validate($request,[
        //     'user_name'=>['required','integer'],
        //     'reply_id'=>['required','integer'],
        // ]);

        if(strcmp($request->_token,csrf_token())==0)
        {
            // check if the user has already vote  or not

            $check_vote = DB::select('select * from reply_voted_users where reply_id = ? and user_id = ?', [$request->reply_id,$request->user_id]);

            // if the array is greater than 0 the user has already voted

            if(count($check_vote)>0)
            {
                // delete the record from the talbe if the user has altrady voted

                $remove_vote = DB::delete('delete from reply_voted_users where id = ?', [$check_vote[0]->id]);
                if($remove_vote > 0)
                {

                $get_vote_count = DB::select('select id,votes from reply_votes where reply_id = ?', [$request->reply_id]);

                if(count($get_vote_count)>0)
                {
                    $vote = $get_vote_count[0]->votes;
                    $vote -- ;
                    //update new vote

                    $update_new_vote = DB::update('update reply_votes set votes = ? where id = ?', [$vote,$get_vote_count[0]->id]);

                    if($update_new_vote > 0  && $remove_vote > 0)
                    {
                        return 'removed';
                    }
                    else
                    {
                    return view('Error.error401');
                    }

                }
                else
                {
                    return view('Error.error401');
                }
                }
                else
                {
                    return view('Error.error401');
                }
            }
            else
            {
                // insert new data

                $voted_user = new ReplyVotedUsers();
                $voted_user->reply_id = $request->reply_id;
                $voted_user->user_id = $request->user_id;
                $voted_user->save();

                // increase count by 1

                // GET THE COUNT

                $get_vote_count = DB::select('select id,votes from reply_votes where reply_id = ?', [$request->reply_id]);

                if(count($get_vote_count)>0)
                {
                    $vote = $get_vote_count[0]->votes;
                    $vote ++ ;
                    //update new vote

                    $update_new_vote = DB::update('update reply_votes set votes = ? where id = ?', [$vote,$get_vote_count[0]->id]);

                    if($update_new_vote > 0 && $voted_user->id > 0)
                    {
                       return 'added';
                    }
                    else
                    {
                         // delete previously entered new user data
                    $delete = DB::delete('delete from reply_voted_users where id = ?', [$voted_user->id]);
                    return view('Error.error401');
                    }

                }
                else
                {
                    // delete previously entered new user data
                    $delete = DB::delete('delete from reply_voted_users where id = ?', [$voted_user->id]);
                    return view('Error.error401');
                }

            }

        }
        else
        {
            return view('Error.error401');
        }
    }



    public static function voteCheck($reply_id,$user_id,$csrf)
    {

        if(strcmp($csrf,csrf_token())==0)
        {
            // check if the user has voted  the reply

        $check_vote = DB::select('select * from reply_voted_users where reply_id = ? and user_id = ?', [$reply_id,$user_id]);

        if(count($check_vote)>0)
        {
            return true;
        }
        else
        {
            return false;
        }
        }


    }


    public function studentPersonalDiscussion(Request $request)
    {
        if(strcmp($request->csrf,csrf_token())==0 && strcmp(auth::user()->type,"STUDENT")==0 && auth::user()->id == $request->id)
        {
            // get posts for the users

              $user_post = DB::select('SELECT users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view ,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id  JOIN post_view_counts on posts.post_id=post_view_counts.post_id  JOIN ratings on ratings.post_id=posts.post_id JOIN reply_counts on reply_counts.post_id = posts.post_id WHERE posts.visibility_id=? AND posts.validity=? AND posts.user_id= ? ORDER BY posts.created_at desc', [3,1,auth::user()->id]);


            return view('Post.studentPersonalDiscussion')->with('personal',$user_post);
        }
        else
        {
            return view('Error.error401');
        }
    }

    public function teacherPersonalDiscussion(Request $request)
    {
        if(strcmp(csrf_token(),$request->csrf)==0 && auth::user()->id == $request->id)
        {

               // get posts for the users

               $user_post = DB::select('SELECT ask_mentors.asked_to,users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view ,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id  JOIN post_view_counts on posts.post_id=post_view_counts.post_id  JOIN ratings on ratings.post_id=posts.post_id JOIN reply_counts on reply_counts.post_id = posts.post_id  join ask_mentors on posts.post_id = ask_mentors.post_id WHERE posts.visibility_id=? AND posts.validity=? AND ask_mentors.asked_to= ? ORDER BY posts.created_at desc', [3,1,auth::user()->id]);


            return view('Post.teachersPersonalDiscussion')->with('personal',$user_post);
        }
        else
        {
            return view('Error.error401');
        }
    }


    public function setAnswer(Request $request)
    {
       if(strcmp($request->_token,csrf_token())==0)
       {
           // update the answer to validity 1
           $update = DB::update('update replies set answer = ? where reply_id = ?', [1,$request->reply_id]);
           // get post id

           $post = DB::select('select post_id from replies where reply_id = ?', [$request->reply_id]);

           // update post

           $update_post = DB::update('update posts set validity = ? where post_id = ?', [0,$post[0]->post_id]);
            if($update > 0 && $update_post > 0)
            {

                return redirect()->back()->withErrors(array('answer'=>'Answer Set To The Post'))->withInput();

            }
            else
            {
                return view('Error.error400');
            }
       }
    }

}

