<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(strcmp(auth::user()->type,"STUDENT")==0  || strcmp(auth::user()->type,"TEACHER")==0)
        {
            $user_id = auth::user()->id;


            //post count

            $post_count = DB::select('select count(post_id) as post_count from posts where user_id = ?', [$user_id]);



             // followers count

             $followers_count = DB::select('select count(id) as followers from followers where following = ?', [$user_id]);

             //follwoing count
             $following_count = DB::select('select count(id) as following from followers where user_id = ?', [$user_id]);

             $info = json_encode( array("post_count"=>$post_count[0]->post_count,"followers_count"=>$followers_count[0]->followers,"following_count"=>$following_count[0]->following));

             //trending


            $trending = DB::select('SELECT post_categories.category_name,COUNT(posts.post_id) as counts from posts  join post_categories on post_categories.category_id = posts.category_id GROUP BY category_name ORDER BY counts desc LIMIT 10');

            // new users

            $users = DB::select('select id,logged_in,name,email,profile_url from users ORDER BY created_at Desc LIMIT 5');

            $public_post = DB::select('SELECT users.id,reply_counts.count as reply_count,ratings.rating as total_rating,post_view_counts.count as post_view ,admire_counts.count as admire_count,posts.post_id,posts.post_title,keyword_name,post_data,tag_name,posts.attachment_id,attachment_url,users.profile_url,users.email,users.name,posts.created_at,visibility FROM posts JOIN post_visibilities ON posts.visibility_id=post_visibilities.visibility_id join users ON posts.user_id = users.id JOIN post_attachments on posts.attachment_id=post_attachments.attachment_id join post_categories on posts.category_id=post_categories.category_id JOIN post_keywords on posts.keyword_id=post_keywords.keyword_id JOIN post_tags ON posts.tag_id=post_tags.tag_id  JOIN admire_counts on posts.post_id = admire_counts.post_id  JOIN post_view_counts on posts.post_id=post_view_counts.post_id  JOIN ratings on ratings.post_id=posts.post_id JOIN reply_counts on reply_counts.post_id = posts.post_id WHERE posts.visibility_id=? AND posts.validity=? ORDER BY posts.created_at desc', [1,1]);
            return view('home.home')->with('public',$public_post)->with("info",$info)->with('trending',$trending)->with('users',$users);
        }
        else if(strcmp(auth::user()->type,"COLLEGE")==0)
        {
            //get teachers
            $teachers = DB::select('select count(id) as teachers from users where users.type  like "TEACHER" ', []);
            // select batches
            $batchses = DB::select('select batch_id,batch_no,semester from batches   order by batch_no desc', []);
            // get subjects
            $subjects = DB::select('select subject_id,name,semester from subjects', []);
            return view('home.home')->with('batch',$batchses)->with('teachers',$teachers[0]->teachers)->with('subjects',$subjects);
        }
    }

}
