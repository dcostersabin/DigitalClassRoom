<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use DB;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'signup-terms'=>['required','min:1'],
            'college'=>['required','string','min:2'],
            'program'=>['required','string','min:2'],
            'batch'=>['required','string','min:2'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $college_name_upper = strtoupper($data['college']);
        $program_name_upper = strtolower($data['program']);
        $batch = $data['batch'];


        // finding college id from the database
        $college = DB::select('select college_id  from colleges where college_name= ? ',[$college_name_upper]);
        if(count($college)>0)
        {
            $college_id = $college[0]->college_id;

        }


        // finding batch id from the database
        $batch = DB::select('select batch_id from batches where batch_no = ?', [$batch]);
        if(count($batch)>0)
        {
            $batch_id = $batch[0]->batch_id;

        }


        // finding program id from the database
        $program = DB::select('select program_id from programs where program_name = ?', [$program_name_upper]);
        if(count($program)>0)
        {
            $program_id = $program[0]->program_id;

        } 




        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'college_id'=>$college_id,
            'batch_id'=>$batch_id,
            'program_id'=>$program_id,
        ]);
    }
}
