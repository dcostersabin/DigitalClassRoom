<?php

namespace App\Http\Controllers;

use App\Notice;
use App\NoticeAttachment;
use App\NoticeView;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Auth;
use Mockery\Exception;
use Mews\Purifier\Facades\Purifier;
use Image;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;

class CollegeController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

   public function postNoticeIndex(Request $request)
   {
      if(strcmp($request->csrf,csrf_token())==0 && strcmp(auth::user()->type,"COLLEGE")==0)
      {
          // get batches

          $batches = json_encode(DB::select('select batch_id,batch_no from batches'));


          // get programs

          $programs = json_encode(DB::select('select program_id,program_name from programs '));

          // notice types

          $types = json_encode(DB::select('select id,notice_type from notice_types'));

        return view('College.postNotice')->with('batches',$batches)->with('programs',$programs)->with('types',$types);
      }
      else {
          return view('Error.error401');
      }
   }



   public function noticeStore(Request $request)
   {
       if(strcmp($request->_token,csrf_token())==0 && strcmp(auth::user()->type,"COLLEGE")==0)
       {
        //    return $request;
         try
         {
            $this->validate($request,[
                'notice_title'=>['required','min:15','max:100','string'],
                'program'=>['required'],
                'batch'=>['required'],
                'type'=>['required'],
                'attachment'=>['mimes:jpeg,jpg,png,gif','max:2097152'],
                'notice_details'=>['required','min:25'],

            ]);

           $clean_notice_title =Purifier::clean($request->notice_title);
           $clean_notice_details =Purifier::clean($request->notice_details);
           $clean_program =Purifier::clean($request->program);
           $clean_batch =Purifier::clean($request->batch);
           $clean_type =Purifier::clean($request->type);
           // final validtaion after cleaning the dirty inputs
           $final_request = new \Illuminate\Http\Request();
           $final_request->setMethod('POST');
           $final_request->replace(["notice_title"=>$clean_notice_title,"notice_details"=>$clean_notice_details,"batch"=>$clean_batch,"program"=>$clean_program,"type"=>$clean_type]);
           $this->validate($final_request,[
            'notice_title'=>['required','min:15','max:100','string'],
            'program'=>['required',],
            'batch'=>['required',],
            'type'=>['required',],
            'attachment'=>['mimes:jpeg,jpg,png,gif','max:2097152'],
            'notice_details'=>['required','min:25'],

        ]);

            $notice = new Notice();

            if(isset($request->attachment))
            {
                // if attachment exist resize and save to NoticeImages folder inside assets
                $image=$request->file('attachment');
                $filename="noticeimg".time().".".$image->getClientOriginalExtension();
                $location=public_path("assets/media/NoticeImages/".$filename);
                $databaseurl="assets/media/NoticeImages/".$filename;
                Image::make($image)->resize(800,400)->save($location);
                $attachment_image = new NoticeAttachment();
                $attachment_image->attachment_url=$databaseurl;
                $attachment_image->save();
                $notice->attachment_id=$attachment_image->id;
            }
            else
            {
                // since no attachment id is set to 1 for references check database or set id as per empty url of attachment
                $notice->attachment_id = 1;
            }

            $notice->notice_type_id = $request->type;
            $notice->notice_title = $clean_notice_title;
            $notice->notice_data = $clean_notice_details;
            $notice->batch = $request->batch;
            $notice->program_id = $request->program;
            $notice->save();

            if($notice->id > 0)
            {
                return Redirect::back()->withInput()->withErrors(array('posted'=>'Your Noitce Was Successfully Posted'));
            }
            else
            {
                return view('Error.error400');
            }


         }
         catch(Exception $e)
         {
            return view('Error.error400');
         }
       }
       else {
           return view('Error.error401');
       }
   }


   public function viewSyllabusStudent(Request $request)
   {
      if(strcmp($request->csrf,csrf_token())==0)
      {
          // get semester of user
          $semester = DB::select('select semester from batches where batch_id = ?', [auth::user()->batch_id]);

        // get all the subjects

        $subjects = DB::select('select * from subjects where semester = ?', [$semester[0]->semester]);


        return view('syllabus.student')->with('subjects',$subjects);
      }
      else {
          return view('Error.error401');
      }
   }


   public function viewSyllabusCollege(Request $request)
   {
       if((strcmp($request->csrf,csrf_token())==0 )&& (strcmp(auth::user()->type,"COLLEGE")==0))
       {
             // get subjects from db

        $subject = DB::select('select * from subjects join programs on subjects.program_id = programs.program_id ');

           return view('syllabus.index')->with('subjects',$subject);
       }
       else {
           return view('Error.error401');
       }
   }

   public function viewSubject(Request $request)
   {
    if((strcmp($request->csrf,csrf_token())==0) && (strcmp(auth::user()->type,"COLLEGE")==0))
    {

        // get programs from db

        $program = DB::select('select program_id,program_name from programs ');


        // get subjects from db

        $subject = DB::select('select subject_id,name,subject_code,programs.program_name,semester from subjects join programs on subjects.program_id = programs.program_id ');

        return view('superadmin.editSubject')->with('programs',$program)->with('subjects',$subject);
    }
    else {
        return view('Error.error401');
    }

   }

   public function viewNoticeStudent(Request $request)
   {
       if(strcmp($request->csrf,csrf_token())==0 && strcmp('STUDENT',auth::user()->type)==0)
       {
        // get notice for specific batches
        $notice = DB::select('SELECT notice_type,batch,notices.id,notice_title,batch_no,program_name,notice_type,notices.created_at,notices.attachment_id,notices.notice_data,attachment_url from notices JOIN notice_attachments on notices.attachment_id = notice_attachments.attachment_id join notice_types on notices.notice_type_id = notice_types.id  join batches on notices.batch=batches.batch_id join programs on notices.program_id=programs.program_id where notices.batch = ? and notices.program_id = ? and notices.notify = ? order by notices.created_at desc', [auth::user()->batch_id, auth::user()->program_id,1]);

        if(count($notice)>0)
        {
            //get rescent notice id for comparision
            $rescent_notice = DB::select('SELECT notices.id from notices JOIN notice_attachments on notices.attachment_id = notice_attachments.attachment_id join notice_types on notices.notice_type_id = notice_types.id  join batches on notices.batch=batches.batch_id join programs on notices.program_id=programs.program_id where notices.batch = ? and notices.program_id = ? and notices.notify = ? order by notices.created_at desc limit 1', [auth::user()->batch_id, auth::user()->program_id,1]);
            // check if the data is already inserted or not
            $check = DB::select('select id from notice_views where notice_id = ? and user_id = ?', [$rescent_notice[0]->id, auth::user()->id]);
            if(!(count($check))>0)
            {
                // insert view to the database
                $notice_view = new NoticeView();
                $notice_view->notice_id = $rescent_notice[0]->id;
                $notice_view->user_id = auth::user()->id;
                $notice_view->save();

            }
        }

        return view('College.studentViewNotice')->with('notice',$notice);
       }
       else
       {
           return view('Error.error401');
       }
   }

   public function viewNoticeCollege(Request $request)
   {
     if(strcmp($request->csrf,csrf_token())==0 && strcmp('COLLEGE',auth::user()->type)==0)
     {
           // get all the notices posted
       $notice = DB::select('SELECT notice_type,batch,notices.id,notice_title,batch_no,program_name,notice_type,notices.created_at,notices.attachment_id,notices.notice_data,attachment_url from notices JOIN notice_attachments on notices.attachment_id = notice_attachments.attachment_id join notice_types on notices.notice_type_id = notice_types.id  join batches on notices.batch=batches.batch_id join programs on notices.program_id=programs.program_id order by notices.created_at desc',[]);

       return view('College.viewNoticeCollege')->with('notice',$notice);
     }
     else
     {
         return view('Error.error401');
     }
   }

   public function showNoticeDetails(Request $request)
   {
          if(strcmp($request->csrf,csrf_token())==0)
          {
                // get all the notices posted
            $notice = DB::select('SELECT batches.batch_id,programs.program_id,notice_type,batch,notices.id,notice_title,batch_no,program_name,notice_type,notices.created_at,notices.attachment_id,notices.notice_data,attachment_url from notices JOIN notice_attachments on notices.attachment_id = notice_attachments.attachment_id join notice_types on notices.notice_type_id = notice_types.id  join batches on notices.batch=batches.batch_id join programs on notices.program_id=programs.program_id  where notices.id = ? order by notices.created_at desc',[$request->id]);
            if(count($notice)>0)
            {
                // get total users
                $total_users =  DB::select('select count(users.id) as total_users from users where batch_id = ? and program_id = ? and users.type LIKE "STUDENT" ', [$notice[0]->batch_id,$notice[0]->program_id]);
                // viewed users
                $viewed_user = DB::select('select users.id,users.name,users.email,users.profile_url from notice_views join users on notice_views.user_id = users.id join notices on notices.id=notice_views.notice_id where notice_views.notice_id = ?', [$notice[0]->id]);
                // not viewed users
                $not_viewed = DB::select('SELECT users.id,users.name,users.email,users.profile_url from users WHERE users.id not IN (SELECT user_id from notice_views where notice_views.notice_id= ?) AND users.type LIKE "STUDENT" and users.batch_id =? and users.program_id = ?', [$notice[0]->id,$notice[0]->batch_id,$notice[0]->program_id]);

                return view('College.noticeDetails')->with('notice',$notice)->with('total_users',$total_users)->with('viewed_users',$viewed_user)->with('not_viewed',$not_viewed);

            }
            else
            {
                return view('Error.error400');
            }

          }
          else
          {
              return view('Error.error401');
          }
   }


   public function updateSemester(Request $request)
   {
      if(strcmp($request->_token,csrf_token())==0 && strcmp(auth::user()->type,"COLLEGE")==0)
      {
            $this->validate($request,[
                'batch_no'=>['required','integer','between:2014,2016'],
                'semester'=>['required','integer','between:1,8'],
            ]);

            // check if the batch exitst

            $check = DB::select('select batch_id from batches where batch_no = ?', [$request->batch_no]);

            if(count($check)>0)
            {
                $update = DB::update('update batches set semester = ? where batch_id = ?', [$request->semester,$check[0]->batch_id]);
                if($update >0)
                {
                    return redirect()->back()->withErrors(array('batch'=>'Batch Updated '))->withInput();
                }
                else
                {
                    return view('Error.error401');
                }
            }
            else
            {
                return view('Error.error401');
            }
      }
   }


   public function showSyllabusStudent(Request $request)
   {
        if (strcmp(csrf_token(),$request->csrf)==0 && strcmp(auth::user()->type,'STUDENT')==0)
        {
            // get syllabus id

            $syllabus_id = DB::select('select syllabus_id from subjects where subject_code = ?', [$request->subject_code]);

            if(count($syllabus_id)>0)
            {
                // get syllabus table name from syllabi table

                $syllabus_table = DB::select('select table_name,references_name from syllabi where syllabus_id = ?', [$syllabus_id[0]->syllabus_id]);

                // get content

                $syllabus_content = DB::select('select * from '.$syllabus_table[0]->table_name.' order by chapter_no', []);

                // get references

                // $references_content = DB::select('select * from '.$syllabus_table[0]->references_name, []);

                $references_content = DB::select('select 	chapter_no,link,ref_name,user_id,users.name,users.profile_url,email  from '.$syllabus_table[0]->references_name.' join users on '.$syllabus_table[0]->references_name.'.user_id = users.id order by '.$syllabus_table[0]->references_name.'.created_at desc', []);


                return view('syllabus.syllabus_subject')->with('syllabus',$syllabus_content)->with('references',$references_content);
            }
            else
            {
                return view('Error.error400');
            }

            return view('syllabus.syllabus_subject');
        } else {
           return view('Error.error401');
        }

   }


   public function showSyllabusTeacher(Request $request)
   {
       if(strcmp(csrf_token(),$request->csrf)==0 && strcmp(auth::user()->type,"TEACHER")==0)
       {
           // get teachers semesters
           $teaching = DB::select('select name,subjects.subject_id,subject_code,subjects.semester from teachings join subjects on subjects.subject_id = teachings.subject_id where user_id = ?', [auth::user()->id]);




           return view('College.teacherSyllabus')->with('teaching',$teaching);
       }
       else
       {
           return view('Error.error401');
       }
   }



   public function showSyllabusDetailsTeacher(Request $request)
   {
      if(strcmp(csrf_token(),$request->csrf)==0 && strcmp(auth::user()->type,"TEACHER")==0)
      {


        //check if the subject exists

        $check = DB::select('select subject_id from subjects where subject_id = ?', [$request->id]);

       if(count($check)>0)
       {
           // get content

           $syllabus = DB::select('select table_name,references_name from subjects  join syllabi on subjects.syllabus_id= syllabi.syllabus_id where subject_id = ?', [$request->id]);

         // get syllabus content

         $content = DB::select('select * from '.$syllabus[0]->table_name, []);
         $references = DB::select('select 	chapter_no,link,ref_name,user_id,users.name,users.profile_url,email  from '.$syllabus[0]->references_name.' join users on '.$syllabus[0]->references_name.'.user_id = users.id order by '.$syllabus[0]->references_name.'.created_at desc', []);
        return view('College.syllabusDetailTeacher')->with('syllabus',$content)->with('references',$references)->with('referencesTable',$syllabus[0]->references_name);
       }
       else
       {
           return view('Error.error400');
       }



      }
      else
      {
          return view('Error.error401');
      }
   }

   public function addReferences(Request $request)
   {
       if(strcmp(csrf_token(),$request->_token)==0 && strcmp(auth::user()->type,"TEACHER")==0)
       {
            $this->validate($request,[
                'chapter'=>['required','between:1,12'],
                'references'=>['required'],
                'link'=>['required'],
                'name'=>['required','min:5','max:255']
            ]);

            if (filter_var($request->link, FILTER_VALIDATE_URL) !== false)
            {
                $clean_name = Purifier::clean($request->name);
                if(!((strlen($clean_name)) >0))
                {
                    $clean_name = 'Link To Chapter No '.$request->chapter;
                }
                $date = Carbon::now();

                $insert = DB::insert('insert into '.$request->references.' (chapter_no,link,user_id,ref_name,created_at,updated_at) values (?, ? , ? , ? , ? , ?)', [$request->chapter,$request->link,auth::user()->id,$clean_name,$date->toDateTimeString(),$date->toDateTimeString()]);

                if($insert)
                {
                    return redirect()->back()->withErrors(array('added'=>'References Successfully Added'))->withInput();
                }
                else
                {
                    return view('Error.error400');
                }
            }
            else
            {
               return redirect()->back()->withErrors(array('link'=>'Please Enter A Valid URL'))->withInput();
            }

       }
       else
       {
           return view(Error.error401);
       }
   }

   public function showTeachers(Request $request)
   {
       // get teachers
       $teachers = DB::select('select name,email,profile_url,id from users  where type = ?', ["TEACHER"]);


       return view('College.teachersList')->with('teachers',$teachers);
   }
}
