<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Mews\Purifier\Facades\Purifier;
use App\Syllabus;
use App\Subject;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use TheSeer\Tokenizer\Exception;
use Illuminate\Support\Facades\Redirect;
use Carbon\Carbon;
use App\User;
use Illuminate\Support\Facades\Hash;
use App\Teaching;


class SuperAdminController extends Controller
{
    public function addSubject(Request $request)
    {
        if((strcmp($request->_token,csrf_token())==0)  && (strcmp(auth::user()->type,"COLLEGE")==0))
        {
           $this->validate($request,[
               'subject_name'=>['required','string','min:3','max:100'],
               'subject_code'=>['required','string','min:3','max:50'],
               'program'=>['required','integer','min:1'],
               'semester'=>['required','min:1','integer'],
           ]);

          $clean_subject_name = Purifier::clean($request->subject_name);
          $clean_subject_code = Purifier::clean($request->subject_code);
          $clean_program = Purifier::clean($request->program);
          $clean_semester = Purifier::clean($request->semester);
           // validate the data affter they are purified
           $final_request = new \Illuminate\Http\Request();
           $final_request->setMethod('POST');
           $final_request->replace(["subject_name"=>$clean_subject_name,"subject_code"=>$clean_subject_code,"program"=>$clean_program,"semester"=>$clean_semester]);
           // final validation
           $this->validate($final_request,[
            'subject_name'=>['required','string','min:3','max:100'],
            'subject_code'=>['required','string','min:3','max:50'],
            // 'program'=>['required','integer','min:1'],
            // 'semester'=>['required','min:1','integer'],
        ]);

        //check if the subject code already exist or not
        $check_code = DB::select('select name from subjects where subject_code = ?', [$request->subject_code]);

        if(count($check_code)>0)
        {

            return redirect()->back()->withErrors(array('subject_code'=>'Subject Code Already Exists'))->withInput();

        }

        // check if the program exists or not

        $check_program = DB::select('select * from programs where program_id = ?', [$request->program]);

        if(count($check_program)>0)
        {
                // create data in syllabus table before adding subject into the table

                    $syllabus = new Syllabus();
                    $time = time();
                    $syllabus->table_name = "syllabus".$time;
                    $syllabus->references_name = "references".$time;
                    $syllabus->save();
                    if($syllabus->id > 0)
                    {
                        // if syllabus data is successfully created insert the subject data into the subjects

                        $subject = new Subject();
                        $subject->name = strip_tags($clean_subject_name);
                        $subject->subject_code = strip_tags($clean_subject_code);
                        $subject->program_id = strip_tags($clean_program);
                        $subject->semester = strip_tags($clean_semester);
                        $subject->syllabus_id = $syllabus->id;
                        $subject->save();

                        if($subject->id > 0)
                        {
                            try
                            {
                                Schema::connection('mysql')->create($syllabus->table_name, function($table)
                                {

                                    $table->bigIncrements('chapter_no')->unsigned()->notnull();

                                    $table->string('chapter_name')->notnull();

                                    $table->text('topics')->notnull();

                                    $table->timestamps();





                                });



                                Schema::connection('mysql')->create($syllabus->references_name, function($table) {


                                    $table->bigInteger('chapter_no')->unsigned()->notnull();
                                    $table->string('link')->notnull();
                                    $table->string('ref_name')->notnull();
                                    $table->bigInteger('user_id')->unsigned()->notnull();
                                    $table->timestamps();


                                    $table->foreign('user_id')->references('id')->on('users');




                                });


                                return Redirect::back()->withInput()->withErrors(array('success'=>'Subject Added Successfully'));




                            }
                            catch(Exception $e)
                            {
                                return view('Error.error401');;
                            }

                        }
                        else {
                            return view('Error.error401');
                        }
                    }
                    else
                    {
                        return view('Error.error401');
                    }
        }
        else
        {
            return view('Error.error401');
        }



        }
        else {
            return view('Error.error401');
        }
    }



    public function editSyllabus(Request $request)
    {
      if((strcmp($request->csrf,csrf_token())==0) && (strcmp(auth::user()->type,"COLLEGE")==0))
      {

        // get syllabus table name

        $table_name = DB::select('select table_name from syllabi where syllabus_id = ?', [$request->id]);

        $contents = DB::select('select * from '.$table_name[0]->table_name);

        $subject_name = DB::select('select name from subjects where syllabus_id = ?', [$request->id]);

        if(count($contents)>0)
        {
            $content = true;
        }
        else
        {
            $content = false ;
        }
        return view('syllabus.edit')->with('table_name',$table_name[0]->table_name)->with('content',$content)->with('data',$contents)->with('subject_name',$subject_name[0]->name);
      }
      else
      {
          return view('Error.error401');
      }
    }


    public function generateForm(Request $request)
    {
        $this->validate($request,['chapter'=>['required','min:1','numeric','max:12']]);
       if((strcmp($request->_token,csrf_token()))==0 && (strcmp(auth::user()->type,"COLLEGE")==0))
       {
        return view('syllabus.create')->with('chapter',$request->chapter)->with('table_name',$request->table_name);
       }
       else
       {
           return view('Errors.error401');
       }
    }

    public function storeSyllabus(Request $request)
    {

        if((strcmp($request->_token,csrf_token())==0) && (strcmp(auth::user()->type,"COLLEGE")==0))
        {
        $this->validate($request,[
                    'table_name'=>['required'],
                    'totalchapter'=>['required'],
                    'cname1'=>['min:3','max:350','string'],
                    'cname2'=>['min:3','max:350','string'],
                    'cname3'=>['min:3','max:350','string'],
                    'cname4'=>['min:3','max:350','string'],
                    'cname5'=>['min:3','max:350','string'],
                    'cname6'=>['min:3','max:350','string'],
                    'cname7'=>['min:3','max:350','string'],
                    'cname8'=>['min:3','max:350','string'],
                    'cname9'=>['min:3','max:350','string'],
                    'cname10'=>['min:3','max:350','string'],
                    'cname11'=>['min:3','max:350','string'],
                    'cname12'=>['min:3','max:350','string'],
                    'cdata1'=>['min:25','string'],
                    'cdata2'=>['min:25','string'],
                    'cdata3'=>['min:25','string'],
                    'cdata4'=>['min:25','string'],
                    'cdata5'=>['min:25','string'],
                    'cdata6'=>['min:25','string'],
                    'cdata7'=>['min:25','string'],
                    'cdata8'=>['min:25','string'],
                    'cdata9'=>['min:25','string'],
                    'cdata10'=>['min:25','string'],
                    'cdata11'=>['min:25','string'],
                    'cdata12'=>['min:25','string'],

                ]);
               // purify data
                for($i=1;$i<=$request->totalchapter;$i++)
                {

                    $cname = Purifier::clean($request['cname'.$i]);
                    $cdata = Purifier::clean($request['cdata'.$i]);

                // // validate the data affter they are purified
                $final_request = new \Illuminate\Http\Request();
                $final_request->setMethod('POST');
                $final_request->replace(["cname"=>$cname,"cdata"=>$cdata]);
                $this->validate($final_request,[
                    'cname'=>['required','min:3','max:350','string'],
                    'cdata'=>['required','min:25','string'],
                ]);

                }

                // insert into database
                $counter = 0;
                for($i=1;$i<=$request->totalchapter;$i++)
                {
                    $date = Carbon::now();
                    $insert = DB::insert('insert into '.$request->table_name.' (chapter_name,topics,created_at,updated_at) values (?, ?, ?, ?)', [Purifier::clean($request['cname'.$i]),Purifier::clean($request['cdata'.$i]),$date->toDateTimeString(),$date->toDateTimeString()
                    ]);
                    if($insert)
                    {
                        $counter ++;
                    }

                }

                if($counter == $request->totalchapter)
                {


                    return redirect('/syllabus/view/college/'.csrf_token())->withInput()->withErrors(array('success'=>'Database Updated Successfully'));


                }
                else
                {
                    return view('Error.error401');
                }







        }
        else {
            return view('Error.error401');
        }
    }



    public function addTeacher(Request $request)
    {
       if(strcmp($request->_token,csrf_token())==0 && strcmp(auth::user()->type,"COLLEGE")==0)
       {
            $this->validate($request,[
                'username'=>['required', 'string', 'max:255'],
                'email'=>['required', 'string', 'email', 'max:255', 'unique:users'],
                'teachingSemester'=>['required','between:1,8'],
                'subject'=>['required'],
            ]);




                   $user = new User();
                   $user->email = $request->email;
                   $user->name = $request->username;
                   $user->batch_id = 999;
                   $user->program_id = 999;
                   $user->college_id = 1;
                   $user->password = Hash::make('digitalclassroom');
                   $user->type = "TEACHER";
                  $user->save();

                  if($user->id > 0)
                  {
                      //check if there is existing teachers

                      $check = DB::select('select id from teachings where semester = ? and subject_id = ? ', [$request->semester,$request->subject]);
                        if(count($check)>0)
                        {
                             return redirect()->back()->withErrors(array('teaching'=>'Teacher Already Assigned To Given Semester'))->withInput();
                        }
                        else
                        {
                            $teaching = new Teaching();
                            $teaching->user_id = $user->id;
                            $teaching->semester = $request->teachingSemester;
                            $teaching->subject_id = $request->subject;
                            $teaching->save();
                            if($user->id > 0 && $teaching->id >0)
                            {
                                return redirect()->back()->withErrors(array('userCreated'=>'Teacher Added With Password "digitalclassroom"'))->withInput();
                            }
                            else{
                                return view('Error.error400');
                            }

                        }

                  }
                  else
                  {
                      return view('Error.error400');
                  }


       }
       else
       {
           return view('Error.error401');
       }
    }
}
