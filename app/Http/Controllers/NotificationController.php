<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
   public function showAll(Request $requests)
   {
     if(strcmp($requests->csrf,csrf_token())==0 && (strcmp(auth::user()->type,"TEACHER")==0  || strcmp(auth::user()->type,"STUDENT")==0) )
     {
         // get posts
        $replies_notseen = DB::select('select name,email,replies.user_id,users.profile_url,replies.created_at,replies.post_id,replies.reply_id from replies join posts on replies.post_id = posts.post_id  join users on replies.user_id = users.id where posts.user_id = ?  and replies.notify = ? order by replies.created_at desc', [auth::user()->id,1]);
        $replies_seen = DB::select('select name,email,replies.user_id,users.profile_url,replies.created_at,replies.post_id,replies.reply_id from replies join posts on replies.post_id = posts.post_id  join users on replies.user_id = users.id where posts.user_id = ?  and replies.notify = ? order by replies.created_at desc', [auth::user()->id,0]);
        $replies = array();
        $replies['seen'] = $replies_seen;
        $replies['notseen'] = $replies_notseen;


         // notice view
        if(strcmp(auth::user()->type,"STUDENT")==0)
        {
            $notice = DB::select('select id from notices where id not in (select (notice_id) from notice_views where user_id = ? order by notices.created_at desc)',[auth::user()->id] );

        }
        else
        {
            $notice = 0;
        }
        // followeres

        $followers_seen = DB::select('select followers.id as followid,name,users.profile_url,users.id,email,followers.created_at from followers  join  users on followers.user_id = users.id where following = ?  and followers.notify = ? order by followers.created_at desc', [auth::user()->id,0]);
        $followers_notseen = DB::select('select followers.id as followid,name,users.profile_url,users.id,email,followers.created_at from followers  join  users on followers.user_id = users.id where following = ? and followers.notify = ? order by followers.created_at desc', [auth::user()->id,1]);
        $followers = array();
        $followers['seen'] = $followers_seen;
        $followers['notseen'] = $followers_notseen;


        return view('Notification.all')->with('replies',$replies)->with('notice',$notice)->with('followers',$followers);
     }
     else
     {
         return view('Error.error400');
     }
   }

   public function markAsRead(Request $request)
   {
      if(strcmp(csrf_token(),$request->_token)==0)
      {
            $this->validate($request,[
                'table'=>['required'],
                'id'=>['required'],
            ]);

            switch($request->table)
            {
                case 'replies':
                    $update = DB::update('update replies set notify = ? where reply_id = ?', [0,$request->id]);
                    if($update > 0)
                    {
                         return redirect()->back()->withErrors(array())->withInput();
                    }
                    else
                    {
                        return view('Error.error400');
                    }
                break;

                case 'followers':
                $update = DB::update('update followers set notify = ? where id = ?', [0,$request->id]);
                    if($update > 0)
                    {
                         return redirect()->back()->withErrors(array())->withInput();
                    }
                    else
                    {
                        return view('Error.error400');
                    }
                    break;
                default:
                return redirect()->back()->withErrors(array())->withInput();

            }
      }
      else
      {
          return view('Error.error400');
      }
   }


   public function count(Request $request)
   {

    if(strcmp($request->_token,csrf_token())== 0 )
    {
        // replies
        $replies_notseen = DB::select('select count(replies.reply_id) as replies from replies join posts on replies.post_id = posts.post_id  join users on replies.user_id = users.id where posts.user_id = ?  and replies.notify = ? order by replies.created_at desc', [auth::user()->id,1]);
        $count = $replies_notseen[0]->replies;
        if(strcmp(auth::user()->type,'STUDENT')==0)
        {
            $notice = DB::select('select count(id) as notice from notices where id not in (select (notice_id) from notice_views where user_id = ? order by notices.created_at desc)',[auth::user()->id] );
            $count = $count + $notice[0]->notice;
        }

        $followers_notseen = DB::select('select count(followers.id) as followid from followers join  users on followers.user_id = users.id where following = ? and followers.notify = ? order by followers.created_at desc', [auth::user()->id,1]);

        $count = $count + $followers_notseen[0]->followid;

        if($count == 0)
        {
            return '';
        }
        else
        {
            return $count;
        }
    }
    else
    {
        return 'fail';
    }

   }
}
