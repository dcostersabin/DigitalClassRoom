INSERT INTO `colleges` (`college_id`, `college_name`, `created_at`, `updated_at`) VALUES
(1, 'NCCS', '2019-03-20 12:43:19', '2019-03-20 12:43:19');

INSERT INTO `batches` (`batch_id`, `batch_no`, `created_at`, `updated_at`) VALUES
(1, 2014, '2019-03-20 12:43:19', '2019-03-20 12:43:19'),
(2, 2015, '2019-03-20 12:43:47', '2019-03-20 12:43:47'),
(3, 2016, '2019-03-20 12:44:06', '2019-03-20 12:44:06');
INSERT INTO `batches`(`batch_id`, `batch_no`, `semester`, `created_at`, `updated_at`) VALUES (999,999,999,NOW(),NOW());
INSERT INTO `programs`(`program_id`, `program_name`, `created_at`, `updated_at`) VALUES (999,"TEACHER",NOW(),NOW());

INSERT INTO `programs` (`program_id`, `program_name`, `created_at`, `updated_at`) VALUES
(1, 'BIM', '2019-03-20 12:43:19', '2019-03-20 12:43:19');
INSERT INTO `post_categories` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(1, 'Linux', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(2, 'Apple', '2019-03-20 12:43:32', '2019-03-20 12:43:32');
INSERT INTO `post_categories` (`category_id`, `category_name`, `created_at`, `updated_at`) VALUES
(3, 'Java', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(4, 'Laravel', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(5, 'Windows', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(6, 'Code', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(7, 'Stack', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(8, 'Ruby', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(9, '.Net', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(10, 'PHP', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(11, 'Django', '2019-03-20 12:43:18', '2019-03-20 12:43:18'),
(12, 'Python', '2019-03-20 12:43:32', '2019-03-20 12:43:32');
INSERT INTO `post_attachments` (`attachment_id`, `attachment_url`, `created_at`, `updated_at`) VALUES
(1, '', '2019-03-20 12:43:18', '2019-03-20 12:43:18');
INSERT INTO `reply_attachments` (`attachment_id`, `attachment_url`, `created_at`, `updated_at`) VALUES
(1, '', '2019-03-20 12:43:18', '2019-03-20 12:43:18');


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++==
INSERT INTO post_visibilities(created_at,updated_at,visibility) VALUES (NOW(),NOW(),'public');
INSERT INTO post_visibilities(created_at,updated_at,visibility) VALUES (NOW(),NOW(),'private');
INSERT INTO post_visibilities(created_at,updated_at,visibility) VALUES (NOW(),NOW(),'personal');
++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++
INSERT INTO `file_types` (`id`, `type_name`, `icon_url`, `created_at`, `updated_at`) VALUES
(1, 'ppt', '/assets/media/icons/ppt.svg', '2019-05-18 04:51:51', '2019-05-18 04:51:51'),
(2, 'pdf', '/assets/media/icons/pdf.svg', '2019-05-18 06:01:04', '2019-05-18 06:01:04'),
(3, 'doc', '/assets/media/icons/doc.svg', '2019-05-18 06:06:11', '2019-05-18 06:06:11'),
(4, 'html', '/assets/media/icons/html.svg', '2019-05-18 06:06:11', '2019-05-18 06:06:11'),
(5, 'file', '/assets/media/icons/file.svg', '2019-05-18 06:06:11', '2019-05-18 06:06:11'),
(6, 'iso', '/assets/media/icons/iso.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(7, 'png', '/assets/media/icons/png.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(8, 'jpg', '/assets/media/icons/jpg.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(9, 'jpeg', '/assets/media/icons/jpg.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(10, 'svg', '/assets/media/icons/svg.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(11, 'mp3', '/assets/media/icons/mp3.svg', '2019-05-18 06:06:12', '2019-05-18 06:06:12'),
(12, 'pptx', '/assets/media/icons/ppt.svg', '2019-05-18 06:20:59', '2019-05-18 06:20:59'),
(13, 'zip', '/assets/media/icons/zip.svg', '2019-05-18 07:15:18', '2019-05-18 07:15:18'),
(14, 'docx', '/assets/media/icons/doc.svg', '2019-05-18 07:15:18', '2019-05-18 07:15:18');


++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++

INSERT INTO `notice_attachments`(`attachment_id`, `created_at`, `updated_at`, `attachment_url`) VALUES (1,NOW(),NOW(),'');


INSERT INTO `notice_types`(`id`, `created_at`, `updated_at`, `notice_type`, `notify`) VALUES (1,NOW(),NOW(),'Holiday',1);




