$(document).ready(function () {


    var csrf = $('#csrf').val();
    $.ajax('/ajax//notificationCount',{
        type:'post',
        dataType:'text',
        data:{_token:csrf},
        success:function(data,status,xth)
        {
            if(status=="success")
            {
                $('#notificationCount').text(data);

            }
        }
    });

    if ($("#public").prop('checked') == true) {
        closeBlocks("showMentors");

    }
    closeBlocks("mentorInfo");
    closeBlocks("finalMentor");
    closeBlocks("editProfile");



    const userid = $('meta[name=userid]').attr("content");
    const token =  $('meta[name=_token]').attr("content");


    currentOnline(userid,token);


});


function postVisibility(name) {

    if (name == "public") {
        $("#public").attr('checked', 'checked');
        $("#private").prop('checked', false);
        $("#personal").prop('checked', false);
        $("#visibility").html("Public");
        closeBlocks('showMentors');
        closeBlocks('finalMentor');

    }
    if (name == "private") {
        $("#private").attr('checked', 'checked');
        $("#public").prop('checked', false);
        $("#personal").prop('checked', false);
        $("#visibility").html("Private");
        closeBlocks('showMentors');
        closeBlocks('finalMentor');


    }
    if (name == "personal") {
        $("#personal").attr('checked', 'checked');
        $("#private").prop('checked', false);
        $("#public").prop('checked', false);
        $("#visibility").html("Personal");
        showBlocks('showMentors');



    }


}

function closeBlocks(id) {
    Codebase.blocks("#" + id, 'close');

}

function showBlocks(id) {
    Codebase.blocks("#" + id, 'open');

}

function getMentoeInfo(csrf) {
    var email = $("#block-form-username").val();
    $.ajax('/ajax/mentorInfo/', {
        type: 'post',
        dataType: 'text',
        data: {
            _token: csrf,
            email: email
        },
        success: function (data, status, xrh) {
            if (status == "success") {
                if (data == "null") {
                    closeBlocks("mentorInfo");
                } else {
                    var info = $.parseJSON(data);
                    $("#mentorProfileUrl").attr('src', info.profile_url);
                    $("#mentorName").text(info.name);
                    $("#mentorEmail").text(info.email);

                }
            }
        }
    });

    showBlocks("mentorInfo");
}


function confirmMentor() {
    var url = $("#mentorProfileUrl").prop('src');
    var name = $("#mentorName").text();
    var email = $("#mentorEmail").text();
    $("#finalMentorName").text(name);
    $("#finalMentorEmail").text(email);
    $("#mentor").attr('value', email);
    $("#mentor").attr('name', 'mentor');
    $("#finalMentorProfileUrl").attr('src', url);
    closeBlocks("showMentors");
    showBlocks("finalMentor");
}

function admireRegister(csrf, postid, userid) {

    $.ajax('/ajax/admire', {
        type: 'post',
        dataType: 'text',
        data: {
            _token: csrf,
            postid: postid,
            userid: userid
        },
        success: function (data, status, xrh) {
            if (status == "success") {
                if (data == "admired") {
                    var count = $("#admirecountbutton" + postid).val();
                    count++;
                    $("#admirecountbutton" + postid).val(count);
                    $("#admirecountbutton" + postid).html(count + '&nbsp;' + '<i class="fa fa-fw fa-heart" id="admirebutton' + postid + '"></i>');

                } else if (data == "removed") {
                    var count = $("#admirecountbutton" + postid).val();
                    count--;
                    $("#admirecountbutton" + postid).val(count);
                    $("#admirecountbutton" + postid).html(count + '&nbsp;' + '<i class="fa fa-fw fa-heart-o" id="admirebutton' + postid + '"></i>');

                }
            }
        }
    });
}

function dat(id, csrf, postid, userid) {
    var data = $("#" + id).text();

    var rating;
    switch (data) {
        case "Just Bad!":
            rating = 1;
            break;
        case "Almost There!":
            rating = 2;
            break;
        case "It’s ok!":
            rating = 3;
            break;
        case "That’s nice!":
            rating = 4;
            break;
        case "Incredible!":
            rating = 5;
            break;

    }

    $.ajax('/ajax/rating/', {
        type: 'post',
        dataType: 'text',
        data: {
            _token: csrf,
            postid: postid,
            userid: userid,
            rating: rating
        },
        success: function (data, status, xrh) {
            if (status == "success") {
                if (data == "success") {
                    $("#rating" + postid).text("You Rated This Post As " + rating + " Star");
                } else {
                    $("#rating" + postid).text("Opps Something Went Wrong");
                }
            }
        }
    });



}


function showPost(id) {
    window.location.href = id;
}

function bottom() {
    $("html, body").animate({
        scrollTop: $(document).height() - $(window).height()
    });
}


function follow(user_id,following,csrf)
{
    $.ajax('/user/follow/request',{
        type:'post',
        dataType:'text',
        data:{_token:csrf,user_id:user_id,following:following},
        success:function(data,status,xrh)
        {
            if(status == "success")
            {
               if(data== "following")
               {
                    $("#followButton"+user_id).attr("class","btn btn-rounded btn-hero btn-sm btn-alt-success mb-5");
                    $("#followButton"+user_id).html(" <i class='fa fa-check mr-5' ></i> Following");


               }
               else if(data == "removed")
               {
                $("#followButton"+user_id).attr("class","btn btn-rounded btn-hero btn-sm btn-alt-danger mb-5");
                $("#followButton"+user_id).html("<i class='fa fa-plus mr-5'></i> Follow");

               }
               else
               {

               }
            }
        }
    });
}



function currentOnline(id,csrf)
{

    $.ajax('/ajax/mutualfollow',{
        type:'post',
        dataType:'text',
        data:{user_id:id,_token:csrf},
        success:function(data,status,xrh)
        {
           if(status=="success")
           {
               data = $.parseJSON(data);

                $.each(data,function(index,item){


                        if(item.logged_in == 1)
                        {
                            var online = " <i class='fa fa-circle text-success'></i>";
                        }
                        else
                        {
                            var online = " <i class='fa fa-circle text-secondary'></i>";
                        }
                        $("#onlineUser").append("<li> <a href='/user/profile/"+item.id+"/"+csrf+"'> <img class='img-avatar' src='"+item.profile_url+"' alt=''> "+item.name+""+online+"<div class='font-w400 font-size-xs text-muted'>"+item.email+"</div></a></li>");
                });
           }
        }
    });
}

function personalStorageDownload(id) {
    $("#downloadform" + id).submit();
}

function personalStorageDelete(id) {
    $("#deleteform" + id).submit();
}

function restoreForm(id) {
    $("#restoreForm" + id).submit();
}

function removeTrashForm(id) {
    $("#removeTrashForm" + id).submit();
}

function showShawl() {
    alert();
}


function bookmark(id)
{
    $("#bookmarkForm"+id).submit();
}

function vote(replyid,userid,csrf)
{

    $.ajax('/ajax/vote',{
        type:'post',
        dataType:'text',
        data:{_token:csrf,reply_id:replyid,user_id:userid},
        success:function(data,status,xrh)
        {
            if(status == "success")
            {
                if(data == "added")
                {
                    var count = $("#ajaxValue"+replyid).val();
                    count ++;
                    $("#ajaxValue"+replyid).val(count)
                    $("#voteCount"+replyid).html(count);
                    $("#voteButton"+replyid).attr('class','btn btn-sm btn-circle btn-alt-danger');
                    $("#voteIcons"+replyid).attr('class','fa fa-arrow-circle-down');

                }
                else if(data == "removed")
                {
                    var count = $("#ajaxValue"+replyid).val();
                    count --;
                    $("#ajaxValue"+replyid).val(count)
                    $("#voteCount"+replyid).html(count);
                    $("#voteButton"+replyid).attr('class','btn btn-sm btn-circle btn-alt-info');
                    $("#voteIcons"+replyid).attr('class','fa fa-arrow-circle-up');
                }
            }
        }
    });
}


function showMore(id)
{
   $("#showmore"+id).attr('class','');
   closeBlocks("showMoreButton"+id);
}


function closeAll(id)
{
    for(i=1 ; i< 9 ; i++)
    {
       if(id == i)
       {}
       else{
           closeBlocks('subject'+i);
       }
    }
}


function closeSubjectOption(name,id)
{
    $("#subjectName").text(name+" for semester "+id);
    showBlocks('selectedSubject');
    closeBlocks('subject'+id);
    closeBlocks('teachingSemester');
}




function addReferences(chapterNo)
{
    $("html, body").animate({
        scrollTop: $(window)
    });
    $('#chapterNo').text(chapterNo);
    $('#formChapterNo').val(chapterNo);
    showBlocks('chapterInfo')
    showBlocks('referencesForm');
}


function setAnswer(id)
{
    $('#bestAnswer'+id).submit();
}

function markasread(tag,id)
{
    $('#'+tag+''+id).submit();
}


