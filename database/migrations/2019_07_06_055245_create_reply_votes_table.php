<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReplyVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reply_votes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('reply_id')->unsigned();
            $table->integer('votes')->default(0);
            $table->timestamps();
            $table->foreign('reply_id')->references('reply_id')->on('replies');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reply_votes');
    }
}
