<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNoticesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notices', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('attachment_id')->unsigned()->default(1);
            $table->bigInteger('notice_type_id')->unsigned();
            $table->string('notice_title')->notnull();
            $table->text('notice_data')->notnull();
            $table->bigInteger('batch')->unsigned()->notnull();
            $table->integer('notify')->default(1)->unsigned();
            $table->bigInteger('program_id')->unsigned()->notnull();


            $table->foreign('attachment_id')->references('attachment_id')->on('notice_attachments');
            $table->foreign('notice_type_id')->references('id')->on('notice_types');
            $table->foreign('batch')->references('batch_id')->on('batches');
            $table->foreign('program_id')->references('program_id')->on('programs');


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notices');
    }
}
