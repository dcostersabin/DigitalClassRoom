<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAskMentorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ask_mentors', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->bigInteger('asked_to')->unsigned()->notnull();
            $table->bigInteger('post_id')->unsigned()->notnull();
            $table->integer('notify')->unsigned()->default(1);


            $table->foreign('asked_to')
            ->references('id')
            ->on('users');



            $table->foreign('post_id')
            ->references('post_id')
            ->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ask_mentors');
    }
}
