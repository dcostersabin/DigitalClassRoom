<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->bigIncrements('post_id');
            $table->timestamps();
            $table->bigInteger('user_id')->unsigned()->notnull();
            $table->string('post_title');
            $table->text('post_data');
            $table->bigInteger('category_id')->unsigned()->notnull();
            $table->bigInteger('keyword_id')->unsigned()->notnull();
            $table->bigInteger('attachment_id')->unsigned()->notnull();
            $table->bigInteger('notify')->unsigned()->default(1);
            $table->integer('validity')->unsigned()->default(1);
            $table->bigInteger('visibility_id')->unsigned()->notnull();
            $table->bigInteger('tag_id')->unsigned()->notnull();




            $table->foreign('user_id')
            ->references('id')
            ->on('users');

            $table->foreign('category_id')
            ->references('category_id')
            ->on('post_categories');


            $table->foreign('attachment_id')
            ->references('attachment_id')
            ->on('post_attachments');

            $table->foreign('visibility_id')
            ->references('visibility_id')
            ->on('post_visibilities');


            $table->foreign('keyword_id')
            ->references('keyword_id')
            ->on('post_keywords');

            $table->foreign('tag_id')
            ->references('tag_id')
            ->on('post_tags');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('posts');
    }
}
