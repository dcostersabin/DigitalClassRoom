<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRepliesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('replies', function (Blueprint $table) {
            $table->bigIncrements('reply_id');
            $table->bigInteger('user_id')->unsigned();
            $table->text('reply_data');
            $table->bigInteger('attachment_id')->unsigned();
            $table->bigInteger('post_id')->unsigned();
            $table->boolean('notify')->default(1);
            $table->Integer('answer')->default(0);
            $table->timestamps();
            $table->foreign('user_id')
            ->references('id')
            ->on('users');
            $table->foreign('attachment_id')
            ->references('attachment_id')
            ->on('reply_attachments');
            $table->foreign('post_id')
            ->references('post_id')
            ->on('posts');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('replies');
    }
}
