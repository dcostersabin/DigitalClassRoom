<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSubjectsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('subjects', function (Blueprint $table) {
            $table->bigIncrements('subject_id');
            $table->string('name')->notnull();
            $table->string('subject_code')->notnull()->unique();
            $table->bigInteger('program_id')->notnull()->unsigned();
            $table->integer('semester')->notnull();
            $table->bigInteger('syllabus_id')->notnull()->unsigned();




            $table->foreign('program_id')->references('program_id')->on('programs');
            $table->foreign('syllabus_id')->references('syllabus_id')->on('syllabi');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('subjects');
    }
}
