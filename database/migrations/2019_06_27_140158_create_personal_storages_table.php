<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePersonalStoragesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_storages', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('user_filename');
            $table->string('file_url');
            $table->bigInteger('icon_id')->unsigned()->notnull();
            $table->integer('validity')->unsigned()->default(1);;
            $table->bigInteger('user_id')->unsigned()->notnull();
            $table->string('file_size')->notnull();
            $table->foreign('user_id')->references('id')->on('users');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_storages');
    }
}
