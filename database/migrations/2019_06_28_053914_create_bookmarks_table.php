<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookmarksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookmarks', function (Blueprint $table) {
            $table->bigIncrements('bookmark_id');
            $table->timestamps();
            $table->bigInteger('post_id')->unsigned()->notnull();
            $table->bigInteger('user_id')->unsigned()->noutnull();
            $table->integer('notify')->unsigned()->default(1);
            $table->foreign('post_id')->references('post_id')->on('posts');
            $table->foreign('user_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookmarks');
    }
}
