<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->string('email')->unique();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->string('type')->default('STUDENT');
            $table->bigInteger('batch_id')->unsigned();
            $table->bigInteger('program_id')->unsigned()->notnull();
            $table->bigInteger('college_id')->unsigned()->notnull();
            $table->Integer('notify')->unsigned()->default(1);
            $table->string('profile_url')->default('/assets/media/UserImages/default.png');
            $table->integer('logged_in')->unsigned()->default(0);


            $table->foreign('batch_id')
            ->references('batch_id')
            ->on('batches');

            $table->foreign('program_id')
            ->references('program_id')
            ->on('programs');

            $table->foreign('college_id')
            ->references('college_id')
            ->on('colleges');



        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
