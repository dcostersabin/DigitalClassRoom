@extends('layouts.app')

@php
$color=array("info","warning","success","danger","primary","secondary");
$count=-1;
@endphp

@section('content')
    @if (count($post)>0)
    <table class="table table-striped table-borderless table-vcenter">
            <thead class="thead-light">
                <tr>
                    <th colspan="2">Posts</th>
                    <th class="d-none d-md-table-cell text-center" style="width: 100px;">Replies</th>
                    <th class="d-none d-md-table-cell text-center" style="width: 100px;">Views</th>
                    <th class="d-none d-md-table-cell" style="width: 100px;">Admires</th>
                    <th class="d-none d-md-table-cell" style="width: 100px;"> Rating</th>
                    <th class="d-none d-md-table-cell" style="width: 100px;">Category</th>
                    <th class="d-none d-md-table-cell" style="width: 200px;">Tags</th>

                </tr>
            </thead>
            <tbody>



              @foreach ($post as $data)
              <tr>
                    <td colspan="2">

                        <a class="font-w600 h5" href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ strip_tags($data->post_title) }}</a>
                        <div class="font-size-sm text-muted mt-5">
                                @php
                                $time = strtotime($data->created_at);
                                $new_time = date('Y M D',$time);
                                @endphp
                            <a href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a> on <em>{{ $new_time }}</em>
                        </div>
                    </td>
                    <td class="d-none d-md-table-cell text-center">
                        <a class="font-w600" href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ $data->replycount }}</a>
                    </td>
                    <td class="d-none d-md-table-cell text-center">
                        <a class="font-w600" href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ $data->viewcount }}</a>
                    </td>
                    <td class="d-none d-md-table-cell">
                        <span class="font-size-sm"><a href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ $data->admirecount }}</a></span>
                    </td>
                    <td class="d-none d-md-table-cell">
                            @php
                            try{
                                $actual_rating = floor($data->rating/($data->viewcount));
                            }
                            catch(Exception $e)
                            {
                                $actual_rating = 0;
                            }
                        @endphp
                            <span class="font-size-sm"><a href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ $actual_rating }}</a></span>
                        </td>
                    <td class="d-none d-md-table-cell">
                            <span class="font-size-sm"><a href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{{ $data->category_name }}</a></span>
                        </td>

                    <td class="d-none d-md-table-cell">
                            @php
                            $tags = (string)strip_tags($data->tag_name)."";
                            $tags = explode(',',$tags);




                            @endphp
                            <span class="font-size-sm"> @foreach ($tags as $tag)
                                    @php

                                    if($count==5)
                                    {
                                    $count=0;
                                    }
                                    else
                                    {
                                    $count ++;
                                    }
                                    @endphp
                                    <span class="badge badge-{{ $color[$count] }}">{{ $tag }}</span>
                                    @endforeach</span>
                        </td>


                </tr>

              @endforeach

            </tbody>
        </table>
    @else


                <div class="content content-full">
                    <div class="py-30 text-center">
                        <h1 class="h2 font-w700 mt-30 mb-10">Oops.. You just found an error page..</h1>
                        <h2 class="h3 font-w400 text-muted mb-50">We are sorry but your request contains bad keywords and cannot be fulfilled..</h2>
                        <a class="btn btn-hero btn-rounded btn-alt-secondary" onclick="window.history.back()">
                            <i class="fa fa-arrow-left mr-10"></i> Back to all Errors
                        </a>
                    </div>
                </div>


    @endif
@endsection



