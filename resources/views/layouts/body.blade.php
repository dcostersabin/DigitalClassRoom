<!doctype html>
<html lang="en" class="no-focus">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">

        <title>Digital ClassRoom</title>


        <meta name="userid" content="{{ auth::user()->id }}">
        <meta name="_token" content="{{ csrf_token() }}">
        <meta name="description" content="Digital ClassRoom">
        <meta name="author" content="pixelcave">
        <meta name="robots" content="noindex, nofollow">

        <!-- Open Graph Meta -->
        <meta property="og:title" content="Digital ClassRoom">
        <meta property="og:site_name" content="Codebase">
        <meta property="og:description" content="Digital ClassRoom">
        <meta property="og:type" content="website">
        <meta property="og:url" content="">
        <meta property="og:image" content="">

        <!-- Icons -->
        {{-- <!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
        <link rel="shortcut icon" href="{{ asset("assets/media/favicons/favicon.png") }}">
        <link rel="icon" type="image/png" sizes="192x192" href="{{ asset("assets/media/favicons/favicon-192x192.png") }}">
        <link rel="apple-touch-icon" sizes="180x180" href="assets/media/favicons/apple-touch-icon-180x180.png">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css") }}">
        <!-- END Icons --> --}}

        <!-- Stylesheets -->

        <!-- Page JS Plugins CSS -->
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/slick/slick.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/slick/slick-theme.css") }}">
        {{-- <link rel="stylesheet" href="{{ asset("assets/js/plugins/select2/css/select2.css") }}"> --}}

        <link rel="stylesheet" href="{{ asset("assets/js/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/select2/css/select2.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/ion-rangeslider/css/ion.rangeSlider.skinHTML5.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/dropzonejs/dist/dropzone.css") }}">
        <!-- Fonts and Codebase framework -->
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Muli:300,400,400i,600,700">
        <link rel="stylesheet" id="css-main" href="{{ asset("assets/css/codebase.min.css") }}">

        <link rel="stylesheet" href="{{ asset("assets/js/plugins/simplemde/simplemde.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/cropperjs/cropper.min.css") }}">
        <link rel="stylesheet" href="{{ asset("assets/js/plugins/sweetalert2/sweetalert2.min.css") }}">

        <!-- You can include a specific file from css/themes/ folder to alter the default color theme of the template. eg: -->
        <!-- <link rel="stylesheet" id="css-theme" href="assets/css/themes/flat.min.css"> -->
        <!-- END Stylesheets -->
    </head>
    <body>

        <!-- Page Container -->
        <!--
            Available classes for #page-container:

        GENERIC

            'enable-cookies'                            Remembers active color theme between pages (when set through color theme helper Template._uiHandleTheme())

        SIDEBAR & SIDE OVERLAY

            'sidebar-r'                                 Right Sidebar and left Side Overlay (default is left Sidebar and right Side Overlay)
            'sidebar-mini'                              Mini hoverable Sidebar (screen width > 991px)
            'sidebar-o'                                 Visible Sidebar by default (screen width > 991px)
            'sidebar-o-xs'                              Visible Sidebar by default (screen width < 992px)
            'sidebar-inverse'                           Dark themed sidebar

            'side-overlay-hover'                        Hoverable Side Overlay (screen width > 991px)
            'side-overlay-o'                            Visible Side Overlay by default

            'enable-page-overlay'                       Enables a visible clickable Page Overlay (closes Side Overlay on click) when Side Overlay opens

            'side-scroll'                               Enables custom scrolling on Sidebar and Side Overlay instead of native scrolling (screen width > 991px)

        HEADER

            ''                                          Static Header if no class is added
            'page-header-fixed'                         Fixed Header

        HEADER STYLE

            ''                                          Classic Header style if no class is added
            'page-header-modern'                        Modern Header style
            'page-header-inverse'                       Dark themed Header (works only with classic Header style)
            'page-header-glass'                         Light themed Header with transparency by default
                                                        (absolute position, perfect for light images underneath - solid light background on scroll if the Header is also set as fixed)
            'page-header-glass page-header-inverse'     Dark themed Header with transparency by default
                                                        (absolute position, perfect for dark images underneath - solid dark background on scroll if the Header is also set as fixed)

        MAIN CONTENT LAYOUT

            ''                                          Full width Main Content if no class is added
            'main-content-boxed'                        Full width Main Content with a specific maximum width (screen width > 1200px)
            'main-content-narrow'                       Full width Main Content with a percentage width (screen width > 1200px)
        -->
        <div id="page-container" class="sidebar-o enable-page-overlay side-scroll page-header-fixed page-header-modern">
            <!-- Side Overlay-->
            <aside id="side-overlay">
                    @yield('rightSideBar')
            </aside>
            <!-- END Side Overlay -->

            <!-- Sidebar -->
            <!--
                Helper classes

                Adding .sidebar-mini-hide to an element will make it invisible (opacity: 0) when the sidebar is in mini mode
                Adding .sidebar-mini-show to an element will make it visible (opacity: 1) when the sidebar is in mini mode
                    If you would like to disable the transition, just add the .sidebar-mini-notrans along with one of the previous 2 classes

                Adding .sidebar-mini-hidden to an element will hide it when the sidebar is in mini mode
                Adding .sidebar-mini-visible to an element will show it only when the sidebar is in mini mode
                    - use .sidebar-mini-visible-b if you would like to be a block when visible (display: block)
            -->
            <nav id="sidebar">
                @yield('leftSideBar')
            </nav>
            <!-- END Sidebar -->

            <!-- Header -->
            <header id="page-header">
                    @yield('topBar')
            </header>
            <!-- END Header -->

            <!-- Main Container -->
            <main id="main-container">

                <!-- Page Content -->
                <div class="content">
                    @yield('content')

                  <div class="row col-md-13">



                        <div class="col-md-8">
                                @if ($errors->first('queries') != null)

                                <div class="alert alert-danger alert-dismissable" role="alert">
                                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
                                        <p class="mb-0">{{ $errors->first('queries') }}</p>
                                    </div>

                                @endif
                            @yield('content8')


                        </div>
                        <span class="col-md-4">

                            @yield('content4')
                        </span>

                  </div>
                </div>
                <!-- END Page Content -->
                <input type="hidden" id='csrf' value="{{ csrf_token() }}">
            </main>
            <!-- END Main Container -->

            <!-- Footer -->
            {{-- <footer id="page-footer" class="opacity-0">
                <div class="content py-20 font-size-xs clearfix">
                    <div class="float-right">
                        Crafted with <i class="fa fa-heart text-pulse"></i> by <a class="font-w600" href="https://1.envato.market/ydb" target="_blank">pixelcave</a>
                    </div>
                    <div class="float-left">
                        <a class="font-w600" href="https://1.envato.market/95j" target="_blank">Codebase 3.0</a> &copy; <span class="js-year-copy">2017</span>
                    </div>
                </div>
            </footer> --}}
            <!-- END Footer -->
        </div>
        <!-- END Page Container -->



        <!--
            Codebase JS Core

            Vital libraries and plugins used in all pages. You can choose to not include this file if you would like
            to handle those dependencies through webpack. Please check out assets/_es6/main/bootstrap.js for more info.

            If you like, you could also include them separately directly from the assets/js/core folder in the following
            order. That can come in handy if you would like to include a few of them (eg jQuery) from a CDN.

            assets/js/core/jquery.min.js
            assets/js/core/bootstrap.bundle.min.js
            assets/js/core/simplebar.min.js
            assets/js/core/jquery-scrollLock.min.js
            assets/js/core/jquery.appear.min.js
            assets/js/core/jquery.countTo.min.js
            assets/js/core/js.cookie.min.js
        -->
        <script src="{{ asset("assets/js/codebase.core.min.js") }}"></script>

        <!--
            Codebase JS

            Custom functionality including Blocks/Layout API as well as other vital and optional helpers
            webpack is putting everything together at assets/_es6/main/app.js
        -->
        <script src="{{ asset("assets/js/codebase.app.min.js") }}"></script>

        {{-- <script src="{{ asset("assets/js/plugins/select2/js/select2.full.min.js") }}"></script> --}}
        <script src="{{ asset("assets/js/plugins/jquery-validation/jquery.validate.min.js") }}"></script>
        <script src="{{ asset("assets/js/plugins/jquery-validation/additional-methods.js") }}"></script>



           <!-- Page JS Plugins -->
           <script src="{{ asset("assets/js/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/bootstrap-maxlength/bootstrap-maxlength.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/select2/js/select2.full.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/jquery-auto-complete/jquery.auto-complete.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/masked-inputs/jquery.maskedinput.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/ion-rangeslider/js/ion.rangeSlider.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/dropzonejs/dropzone.min.js") }}"></script>
           <script src="{{ asset("assets/js/plugins/pwstrength-bootstrap/pwstrength-bootstrap.min.js") }}"></script>


        <!-- Page JS Helpers (Select2 plugin) -->
        <script>jQuery(function(){ Codebase.helpers('select2'); });</script>

        <!-- Page JS Code -->
        <script src="assets/js/pages/be_forms_validation.min.js"></script>

        <!-- Page JS Plugins -->
        <script src="{{ asset("assets/js/plugins/chartjs/Chart.bundle.min.js") }}"></script>
        <script src="{{ asset("assets/js/plugins/slick/slick.min.js") }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset("assets/js/pages/be_pages_dashboard.min.js") }}"></script>

        <script src="{{ asset("assets/js/plugins/simplemde/simplemde.min.js") }}"></script>
        <script src="{{ asset("assets/js/plugins/custom/custom.js") }}"></script>
        <script src="{{ asset("assets/js/plugins/jquery-tags-input/jquery.tagsinput.min.js") }}"></script>
        <script src="{{ asset("assets/js/pages/be_forms_plugins.min.js") }}"></script>
        <script>jQuery(function(){ Codebase.helpers(['datepicker', 'colorpicker', 'maxlength', 'select2', 'masked-inputs', 'rangeslider', 'tags-inputs']); });</script>
        @yield('script')
       <!-- Page JS Plugins -->
       <script src="{{ asset("assets/js/plugins/cropperjs/cropper.min.js") }}"></script>

       <!-- Page JS Code -->
       <script src="{{ asset("assets/js/pages/be_comp_image_cropper.min.js") }}"></script>

        <script src="{{ asset("assets/js/plugins/jquery-raty/jquery.raty.js") }}"></script>

        <!-- Page JS Code -->
        <script src="{{ asset("assets/js/pages/be_comp_rating.min.js") }}"></script>
        <script src="{{ asset("assets/js/plugins/sweetalert2/sweetalert2.min.js") }}"></script>



        <script>jQuery(function () {
            Codebase.helpers(['summernote', 'ckeditor', 'simplemde']);
        });</script>




    </body>
</html>
