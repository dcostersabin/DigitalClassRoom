@extends('layouts.body')

@section('leftSideBar')
       <!-- Sidebar Content -->
       <div class="sidebar-content">
        <!-- Side Header -->
        <div class="content-header content-header-fullrow px-15">
            <!-- Mini Mode -->
            <div class="content-header-section sidebar-mini-visible-b">
                <!-- Logo -->
                <span class="content-header-item font-w700 font-size-xl float-left animated fadeIn">
                    <span class="text-dual-primary-dark">D</span><span class="text-primary">CR</span>
                </span>
                <!-- END Logo -->
            </div>
            <!-- END Mini Mode -->

            <!-- Normal Mode -->
            <div class="content-header-section text-center align-parent sidebar-mini-hidden">
                <!-- Close Sidebar, Visible only on mobile screens -->
                <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                <button type="button" class="btn btn-circle btn-dual-secondary d-lg-none align-v-r" data-toggle="layout" data-action="sidebar_close">
                    <i class="fa fa-times text-danger"></i>
                </button>
                <!-- END Close Sidebar -->

                <!-- Logo -->
                <div class="content-header-item">
                    <a class="link-effect font-w700" href="/home">

                        <span class="font-size-xl text-dual-primary-dark">Digital</span><span class="font-size-xl text-primary">ClassRoom</span>
                    </a>
                </div>
                <!-- END Logo -->
            </div>
            <!-- END Normal Mode -->
        </div>
        <!-- END Side Header -->

        <!-- Side User -->
        <div class="content-side content-side-full content-side-user px-10 align-parent">
            <!-- Visible only in mini mode -->
            <div class="sidebar-mini-visible-b align-v animated fadeIn">
                <img class="img-avatar img-avatar32" src="{{ asset(auth::user()->profile_url) }}" alt="">
            </div>
            <!-- END Visible only in mini mode -->

            <!-- Visible only in normal mode -->
            <div class="sidebar-mini-hidden-b text-center">
                <a class="img-link" href="/user/profile/{{ auth::user()->id }}/{{ csrf_token() }}">
                    <img class="img-avatar" src="{{ asset(auth::user()->profile_url) }}" alt="">
                </a>
                <ul class="list-inline mt-10">
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark font-size-xs font-w600 text-uppercase" href="/user/profile/{{ auth::user()->id }}/{{ csrf_token() }}">{{ auth::user()->name }}</a>
                    </li>
                    <li class="list-inline-item">
                        <!-- Layout API, functionality initialized in Template._uiApiLayout() -->
                        <a class="link-effect text-dual-primary-dark" data-toggle="layout" data-action="sidebar_style_inverse_toggle" href="javascript:void(0)">
                            <i class="si si-drop"></i>
                        </a>
                    </li>
                    <li class="list-inline-item">
                        <a class="link-effect text-dual-primary-dark" href="{{ route('logout') }}"
                        onclick="event.preventDefault();
                                         document.getElementById('logout-form').submit();">

                            <i class="si si-logout"></i>

                        </a>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </li>
                </ul>
            </div>
            <!-- END Visible only in normal mode -->
        </div>
        <!-- END Side User -->

        <!-- Side Navigation -->
        <div class="content-side content-side-full">


            @if (strcmp(auth::user()->type,"STUDENT")==0)
            <ul class="nav-main">
                    <li>
                            <a class="active" href="/home"><i class="si si-home"></i><span class="sidebar-mini-hide">Home</span></a>
                        </li>

                <li>
                    <a class="active" href="/batch/{{ auth::user()->batch_id }}/{{ csrf_token() }}"><i class="si si-badge  "></i><span class="sidebar-mini-hide">ClassRoom</span></a>
                </li>
                <li>
                    <a class="active"  href="/personal/discussion/student/{{ csrf_token() }}/{{ auth::user()->id }}"><i class="si si-lock "></i><span class="sidebar-mini-hide">Personal Discussion</span></a>
                </li>
                <li>
                    <a class="active" href="/create/{{ csrf_token() }}"><i class="si  si-pencil "></i><span class="sidebar-mini-hide">Ask Question</span></a>
                </li>
                <li>
                        <a class="active" href="/user/profile/{{ auth::user()->id }}/{{ csrf_token() }}"><i class="si   si-user "></i><span class="sidebar-mini-hide">{{ auth::user()->name }}</span></a>
                 </li>
                 <li>
                    <a class="active" href="/bookmarks/{{ csrf_token() }}/{{ auth::user()->id }}"><i class="fa  fa-bookmark-o "></i><span class="sidebar-mini-hide">Bookmarks</span></a>
                </li>
                 <li>
                    <a class="active" href="/show/notifications/{{ csrf_token() }}"><i class="si si-bell"></i><span class="sidebar-mini-hide">Notifications   </span></a>
                </li>
                <li>
                    <a class="active" href="/syllabus/view/{{ csrf_token() }}"><i class="si  si-list   "></i><span class="sidebar-mini-hide">Syllabus</span></a>
                </li>
                <li>
                    <a class="active" href="/view/notice/student/{{ csrf_token() }}"><i class="si  si-envelope "></i><span class="sidebar-mini-hide">College Notice</span></a>
                </li>
                <li>
                    <a class="active" href="/personal/storage/{{ csrf_token() }}"><i class="si  si-cloud-upload "></i><span class="sidebar-mini-hide">Personal Storage</span></a>
                </li>




            </ul>
            @endif


            @if (strcmp(auth::user()->type,"TEACHER")==0)
            <ul class="nav-main">
                    <li>
                            <a class="active" href="/home"><i class="si si-home"></i><span class="sidebar-mini-hide">Home</span></a>
                        </li>


                <li>
                    <a class="active" href="/teacher/personal/discussion/{{ csrf_token() }}/{{ auth::user()->id }}"><i class="si si-lock "></i><span class="sidebar-mini-hide">Personal Discussion</span></a>
                </li>
                <li>
                    <a class="active" href="/create/{{ csrf_token() }}"><i class="si  si-pencil "></i><span class="sidebar-mini-hide">Ask Question</span></a>
                </li>
                <li>
                        <a class="active" href="/user/profile/{{ auth::user()->id }}/{{ csrf_token() }}"><i class="si   si-user "></i><span class="sidebar-mini-hide">{{ auth::user()->name }}</span></a>
                 </li>
                 <li>
                    <a class="active" href="/bookmarks/{{ csrf_token() }}/{{ auth::user()->id }}"><i class="fa  fa-bookmark-o "></i><span class="sidebar-mini-hide">Bookmarks</span></a>
                </li>

                  <li>
                    <a class="active" href="/show/notifications/{{ csrf_token() }}"><i class="si si-bell"></i><span class="sidebar-mini-hide">Notifications   </span></a>
                </li>
                <li>
                    <a class="active" href="/teacher/view/syllabus/{{ csrf_token() }}"><i class="si  si-list   "></i><span class="sidebar-mini-hide">Syllabus</span></a>
                </li>

                <li>
                    <a class="active" href="/personal/storage/{{ csrf_token() }}"><i class="si  si-cloud-upload "></i><span class="sidebar-mini-hide">Personal Storage</span></a>
                </li>


            </ul>
            @endif




            @if (strcmp(auth::user()->type,"COLLEGE")==0)

            <ul class="nav-main">
                <li>
                    <a class="active" href="/home"><i class="si si-home"></i><span class="sidebar-mini-hide">Home</span></a>
                </li>
                <li>
                    <a class="active" href="/post/notice/college/{{ csrf_token() }}"><i class="si si-note"></i><span class="sidebar-mini-hide">Post Notice</span></a>
                </li>
                <li>
                    <a class="active" href="/post/notice/college/view/{{ csrf_token() }}"><i class="si si-eye"></i><span class="sidebar-mini-hide">View Notice</span></a>
                </li>
                <li>
                    <a class="active" href="/edit/subject/college/{{ csrf_token() }}"><i class="si si-book-open"></i><span class="sidebar-mini-hide">Edit Subjects</span></a>
                </li>
                <li>
                    <a class="active" href="/syllabus/view/college/{{ csrf_token() }}"><i class="si si-list"></i><span class="sidebar-mini-hide">Edit Syllabus</span></a>
                </li>
            </ul>


            @endif



        </div>
        <!-- END Side Navigation -->
    </div>
    <!-- Sidebar Content -->
@endsection
