@extends('layouts.app')
@if (strcmp(auth::user()->type,"STUDENT")==0 || strcmp(auth::user()->type,"TEACHER")==0)

@php
$color=array("info","warning","success","danger","primary","secondary");
$count=-1;
@endphp

@section('content8')

<!-- END User -->

<!-- Updates -->




@foreach ($public as $data)

<div class="col-md-13 ">

    <div class="block block-rounded ">
        <div class="block block-border block-fx-shadow">

            <div class="block-content block-content-full border-t">
                <div class="media">
                    <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">

                    <div class="media-body">
                        @php
                        $time = strtotime($data->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
                        <p class="mb-0">
                            <a class="font-w600"
                                href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                            <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                            <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em>&nbsp;&nbsp;
                            <a class="text-black"><small
                                    class="badge btn-alt-primary">{{ ucwords($data->visibility) }}</small></a>

                        </p>

                        @php
                        $tags = (string)strip_tags($data->tag_name)."";
                        $tags = explode(',',$tags);




                        @endphp

                        <div>
                            @foreach ($tags as $tag)
                            @php

                            if($count==5)
                            {
                            $count=0;
                            }
                            else
                            {
                            $count ++;
                            }
                            @endphp
                            <span class="badge badge-{{ $color[$count] }}">{{ $tag }}</span>
                            @endforeach
                        </div>


                        <div>
                            <a class="h4 text-black"
                                href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{!!
                                strip_tags($data->post_title) !!}</a>

                            @if(str_word_count($data->post_data)>=500)
                            @php
                            $text=substr($data->post_data,0,500);
                            $text=strip_tags($text);

                            $text=$text."........";
                            @endphp
                            <p>{!! $text !!}</p>
                            @else
                            {!! $data->post_data !!}
                            @endif
                        </div>

                        {{-- @if ($data->attachment_id !=1)

                            <div class="col-xl-4">
                                <a class="block block-link-pop text-center" href="javascript:void(0)">
                                    <div class="block-content">
                                        <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                        alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                        <p class="font-w600"></p>

                    </div>

                    </a>

                </div>




                @endif --}}






            </div>
        </div>


        <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">

            <div class="push">



                <span class="btn-group btn-group-sm " role="group" aria-label="btnGroup1">

                    <button type="button" class="btn btn-sm btn-rounded btn-alt-danger mr-10"
                        onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                        {{ $data->post_view }} <i class="fa fa-fw fa-eye"></i>
                    </button>
                    @php
                    $check=app('\App\Http\Controllers\PostController')->admireCheck(auth::user()->id,$data->post_id,csrf_token());
                    @endphp

                    @if ($check)
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                        id="admirecountbutton{{$data->post_id  }}"
                        onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                        value="  {{ $data->admire_count }}">
                        {{ $data->admire_count }} <i class="fa fa-fw fa-heart"
                            id="admirebutton{{ $data->post_id }}"></i>
                    </button>
                    @else
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                        id="admirecountbutton{{$data->post_id  }}"
                        onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                        value="  {{ $data->admire_count }}">
                        {{ $data->admire_count }} <i class="fa fa-fw fa-heart-o"
                            id="admirebutton{{ $data->post_id }}"></i>
                    </button>
                    @endif
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-success mr-10"
                        onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                        {{ $data->reply_count }} <i class="fa fa-fw fa-comment-o"></i>
                    </button>

                    <span disabled="disabled" class="btn btn-sm btn-rounded text-red mr-10"
                        id="star{{ $data->post_id }}"></span>
                </span>
                @php
                try{
                $actual_rating = floor($data->total_rating/($data->post_view));
                }
                catch(Exception $e)
                {
                $actual_rating = 0;
                }
                @endphp
                {{-- <span class="js-rating pull-right" data-score="{{ $actual_rating }}"
                data-star-on="fa fa-fw fa-2x fa-star text-warning"
                data-star-off="fa fa-fw fa-2x fa-star text-muted" data-target="#star{{ $data->post_id }}"
                onclick="dat('star{{ $data->post_id }}','{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"></span>
                --}}


                <span class="pull-right text-black">
                    Average Rating: {{ $actual_rating }}
                </span>



            </div>

        </div>
    </div>
</div>

</div>
</div>

@endforeach






@endsection

@section('content4')

<!-- Account -->
<div class="block block-rounded text-center font-w600">
    <div class="block-content block-content-full bg-gd-sea">
        <img class="img-avatar img-avatar-thumb" src="{{ asset(auth::user()->profile_url) }}" alt="">
    </div>
    <div class="block-content block-content-full">
        <div class="border-b pb-15 mb-15">
            {{ auth::user()->name }}<br>
            <a class="text-muted font-w400" href="javascript:void(0)">{{ auth::user()->email }}</a>
        </div>
        <div class="row gutters-tiny">
            <div class="col-4">
                @php
                $temp = json_decode($info);
                @endphp
                <div class="font-size-xs text-muted">Post Updates</div>
                <a class="font-size-lg" href="javascript:void(0)">{{ $temp->post_count }}</a>
            </div>
            <div class="col-4">
                <div class="font-size-xs text-muted">Following</div>
                <a class="font-size-lg" href="javascript:void(0)">{{ $temp->following_count }}</a>
            </div>
            <div class="col-4">
                <div class="font-size-xs text-muted">Followers</div>
                <a class="font-size-lg" href="javascript:void(0)">{{ $temp->followers_count }}</a>
            </div>
        </div>
    </div>
</div>
<!-- END Account -->

<!-- Worldwide Trends -->
<div class="block block-rounded">
    <div class="block-header">
        <h3 class="block-title font-w600">Trending Categories</h3>
        <div class="block-options">


        </div>
    </div>
    <div class="block-content">

        @foreach ($trending as $item)
        <a class="font-w600" href="javascript:void(0)">#{{ $item->category_name }}</a>
        <p class="text-muted">{{ $item->counts }} Updates</p>

        @endforeach
    </div>
</div>

<div class="block block-rounded">
    <div class="block-header">
        <h3 class="block-title font-w600">Who to Follow</h3>

    </div>
    <div class="block-content block-content-full">
        <ul class="nav-users pull-all">

            @foreach ($users as $item)
            <li>
                <a href="/user/profile/{{ $item->id }}/{{ csrf_token() }}">
                    <img class="img-avatar" src="{{ asset($item->profile_url) }}" alt="">
                    @if ($item->logged_in == 1)
                    <i class="fa fa-circle text-success"></i>
                    @else
                    <i class="fa fa-circle text-secondary"></i>
                    @endif{{ $item->name }}
                    <div class="font-w400 font-size-xs text-muted">{{ $item->email }}</div>
                </a>
            </li>
            @endforeach

        </ul>
    </div>
    <div class="block-content block-content-full border-t">
        <a href="javascript:void(0)">
            <i class="fa fa-users mr-5"></i> Find people you know
        </a>
    </div>
</div>
<!-- END Who to follow -->

<!-- About -->
{{-- <div class="block block-rounded">
        <div class="block-content block-content-full text-muted font-size-sm">
            &copy; <span class="js-year-copy"></span>
            <a class="text-muted ml-5" href="javascript:void(0)">About Us</a>
            <a class="text-muted ml-5" href="javascript:void(0)">Copyright</a>
        </div>
        <div class="block-content block-content-full border-t">
            <a href="javascript:void(0)">
                <i class="fa fa-external-link-square mr-5"></i> Advertise with Us
            </a>
        </div>
    </div> --}}


@endsection

@endif



@if (strcmp(auth::user()->type,"COLLEGE")==0)
@section('content8')
<div class="col-md-13">
        @if ($errors->first('userCreated') != null)
        <!-- Success Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">{{ $errors->first('userCreated') }}</p>

        </div>
        <!-- END Success Alert -->
        @endif
</div>
<div class="row gutters-tiny invisible" data-toggle="appear">
    <!-- Row #3 -->
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="fa fa-users fa-4x text-primary"></i>
                    </div>
                    <div class="font-size-h4 font-w600">{{ $teachers }} Teachers</div>
                    <div class="text-muted">Manage Teachers List </div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-primary" onclick="window.location.href='/show/teachers/{{ csrf_token() }}'">
                            <i class="fa fa-cog mr-5"></i> Manage list
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="fa fa-user-plus fa-4x text-info"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Add Teachers</div>
                    <div class="text-muted">You are doing great!</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-info" onclick="showBlocks('addTeacher');closeBlocks('updateBatches');closeBlocks('updateForm');">
                            <i class="fa fa-plus mr-5"></i> Add One Now
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si si-graduation fa-4x text-success"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Update Semesters</div>
                    <div class="text-muted">Manage Semesters Accordingly</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-success" onclick="showBlocks('updateBatches');closeBlocks('addTeacher');closeBlocks('updateForm');">
                            <i class="si  si-action-redo "></i> Update Batches
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si si-note fa-4x text-warning"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Post Notice</div>
                    <div class="text-muted">Manage Semesters Accordingly</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-warning" onclick="window.location.href='/post/notice/college/{{ csrf_token() }}'">
                            <i class="si  si-pencil "></i> Post Notices
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si si-note fa-4x text-danger"></i>
                    </div>
                    <div class="font-size-h4 font-w600">View Notice</div>
                    <div class="text-muted">Show Notices Posted</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-alt-danger" onclick="window.location.href='/post/notice/college/view/{{ csrf_token() }}'">
                            <i class="si   si-eye  "></i> Show
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si  si-book-open  fa-4x text-elegance"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Add Subject</div>
                    <div class="text-muted">Add Subjects To The Database</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-default bg-elegance-light" onclick="window.location.href='/edit/subject/college/{{ csrf_token() }}'">
                            <i class="fa   fa-plus  "></i> Add Subject
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="col-md-4">
        <div class="block">
            <div class="block-content block-content-full">
                <div class="py-20 text-center">
                    <div class="mb-20">
                        <i class="si  si-book-open  fa-4x text-flat"></i>
                    </div>
                    <div class="font-size-h4 font-w600">Edit Syllabus</div>
                    <div class="text-muted">Show Notices Posted</div>
                    <div class="pt-20">
                        <a class="btn btn-rounded btn-default bg-flat-light" onclick="window.location.href='/syllabus/view/college/{{ csrf_token() }}'">
                            <i class="fa   fa-plus  "></i> Add Subject
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END Row #3 -->
</div>
@endsection


@section('content4')


<div class=@if ($errors->first('username') != null || $errors->first('email') != null || $errors->first('teachingSemester') != null || $errors->first('subject')!= null ||  $errors->first('teaching')!= null )
"container"
@else
"container d-none"
@endif id="addTeacher">
    <div class="row">
        <div class="col-md-12">

            <form action="/add/teacher/college" method="post">
                @csrf
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Add A Teacher</h3>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-alt-primary">
                                <i class="fa fa-check"></i> Submit
                            </button>
                            <button type="reset" class="btn btn-sm btn-alt-danger">
                                <i class="fa fa-repeat"></i> Reset
                            </button>
                        </div>
                    </div>
                    <div class="block-content">
                        <div class="form-group row justify-content-center">
                            <label class="col-sm-8" for="block-form-username3">Username</label>
                            <div class="col-sm-8">
                                <input type="text" class="form-control" id="block-form-username3" name="username" placeholder="Enter teachers username.." @if (old('username')!= null)
                                    value={{old('username')}}

                                @endif>
                               @if ($errors->first('username') != null)
                               <div class="text-danger">{{ $errors->first('username') }}</div>
                               @endif
                            </div>

                        </div>

                        <div class="form-group row justify-content-center">
                            <label class="col-sm-8" for="block-form-username3">Email</label>
                            <div class="col-sm-8">
                                <input type="email" class="form-control" id="block-form-username3" name="email" placeholder="Enter teachers Email.."   @if (old('username')!= null)
                                value={{old('email')}}

                            @endif>
                                @if ($errors->first('email') != null)
                                <div class="text-danger">{{ $errors->first('email') }}</div>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row justify-content-center" id="teachingSemester">
                            <label class="col-sm-8" for="block-form-username3"> Teaching Semester</label>
                            <div class="col-sm-8">


                                <div class="col-md-13">
                                    <select class="form-control"  name="teachingSemester">
                                        <option value="">Please select Semester</option>

                                        @for ($i = 1; $i < 9; $i++)
                                        <option value="{{ $i}}" onclick="showBlocks('subject{{ $i }}');closeAll('{{ $i }}')">{{ $i }}</option>
                                        @endfor

                                    </select>
                                    @if ($errors->first('teachingSemester') != null)
                                    <div class="text-danger">{{ $errors->first('teachingSemester') }}</div>
                                    @endif
                                    @if ($errors->first('teaching') != null)
                                    <div class="text-danger">{{ $errors->first('teaching') }}</div>
                                    @endif
                                </div>

                            </div>

                        </div>
                        <div class="form-group row justify-content-center d-none" id="selectedSubject">
                            <label class="col-sm-8" for="block-form-username3">Selected <span id="subjectName" class="text-danger"></span></label>

                        </div>


                        @for ($i = 1; $i < 9; $i++)
                        <div class="form-group row justify-content-center d-none" id="subject{{ $i }}">
                            <label class="col-sm-8" for="block-form-username3">Semester {{ $i }} Subjects</label>


                        @foreach ($subjects as $item)
                        @if ($i == $item->semester)
                        <div class="col-sm-8">
                            <label class="css-control css-control-primary css-switch">
                                <input type="checkbox" class="css-control-input" name="subject" value="{{ $item->subject_id }}"  onclick="closeSubjectOption('{{ $item->name }}','{{ $i }}')">
                                <span class="css-control-indicator"></span> {{ $item->name }}
                            </label>
                        </div>

                        @endif
                        @endforeach
                    </div>
                        @endfor


                    </div>
                </div>
            </form>
        </div>
</div>
</div>

<div class="container">
    <div class="col-md-12">

        @if ($errors->first('batch') != null)
        <!-- Success Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">{{ $errors->first('batch') }}</p>

        </div>
        <!-- END Success Alert -->
        @endif

        <div class="block d-none" id="updateBatches">
            <div class="block-header block-header-default">
                <h3 class="block-title">Batches</h3>
                <div class="block-options">
                    <button type="button" class="btn btn-sm btn-alt-primary" onclick="showBlocks('updateForm')">
                        <i class="si si-action-redo"></i> Update
                    </button>

                </div>
            </div>
            <div class="block-content">
                <table class="table table-bordered table-vcenter">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 50px;">#</th>
                            <th>Batch No</th>
                            <th class="d-none d-sm-table-cell" style="width: 15%;">Semester</th>

                        </tr>
                    </thead>
                    <tbody>

                        @php
                        $counter = 0;
                        @endphp
                        @foreach ($batch as $item)
                        @if (!($item->batch_no == 999))
                        @php
                        $counter ++;
                        @endphp
                        <tr>
                            <th class="text-center" scope="row">{{ $counter }}</th>
                            <td>{{ $item->batch_no }}</td>
                            <td class="d-none d-sm-table-cell">
                                {{ $item->semester }}
                            </td>
                        </tr>
                        @endif
                        @endforeach

                    </tbody>
                </table>
            </div>

        </div>



        <div id="updateForm" class="@if ($errors->first('batch_no') != null || $errors->first('semester') != null)

       @else
        d-none
       @endif">
            <form method="post" action="/college/set/semester">
                @csrf
                <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Edit Semester</h3>
                        <div class="block-options">
                            <button type="submit" class="btn btn-sm btn-alt-primary">
                                <i class="fa fa-check"></i> Submit
                            </button>

                        </div>
                    </div>
                    <div class="block-content">

                        <div class="form-group row justify-content-center">
                            <div class="col-sm-12">

                                <div class="form-material">
                                    <select class="form-control" id="material-select" name="batch_no">
                                        <option value="">Batch No</option>

                                        @foreach ($batch as $item) @if (!($item->batch_no == 999))
                                        <option value="{{ $item->batch_no }}">{{ $item->batch_no }}</option>
                                        @endif
                                        @endforeach
                                    </select>
                                    <label for="material-select">Please Select Batch No </label>
                                    <div class="text-danger">
                                        @if ($errors->first('batch_no') != null)
                                        {{ $errors->first('batch_no') }}
                                        @endif
                                    </div>
                                </div>
                                <div class="form-material">
                                    <select class="form-control" id="material-select" name="semester">
                                        <option value="">Semester</option>

                                        @for ($i = 1; $i < 9; $i++) <option value="{{ $i }}">{{ $i }}</option>
                                            @endfor
                                    </select>
                                    <label for="material-select">Please Select Semester </label>
                                    <div class="text-danger">
                                        @if ($errors->first('semester') != null)
                                        {{ $errors->first('semester') }}
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                </div>
            </form>
        </div>
    </div>

</div>
</div>
@endsection
@endif
