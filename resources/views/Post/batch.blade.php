@extends('layouts.app')

@php
$color=array("info","warning","success","danger","primary","secondary");
$count=-1;
@endphp

@section('content')

@if (count($batch)>0)
@foreach ($batch as $data)

<div class="col-md-13 ">

    <div class="block block-rounded ">
        <div class="block block-border block-fx-shadow">

            <div class="block-content block-content-full border-t">
                <div class="media">
                    <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">

                    <div class="media-body">
                        @php
                        $time = strtotime($data->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
                        <p class="mb-0">
                            <a class="font-w600"
                                href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                            <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                            <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em>&nbsp;&nbsp;
                            <a class="text-black"><small
                                    class="badge btn-alt-primary">{{ ucwords($data->visibility) }}</small></a>

                        </p>

                        @php
                        $tags = (string)strip_tags($data->tag_name)."";
                        $tags = explode(',',$tags);




                        @endphp

                        <div>
                            @foreach ($tags as $tag)
                            @php

                            if($count==5)
                            {
                            $count=0;
                            }
                            else
                            {
                            $count ++;
                            }
                            @endphp
                            <span class="badge badge-{{ $color[$count] }}">{{ $tag }}</span>
                            @endforeach
                        </div>


                        <div>
                            <a class="h4 text-black"
                                href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{!!
                                strip_tags($data->post_title) !!}</a>

                            @if(str_word_count($data->post_data)>=500)
                            @php
                            $text=substr($data->post_data,0,500);
                            $text=strip_tags($text);

                            $text=$text."........";
                            @endphp
                            <p>{!! $text !!}</p>
                            @else
                            {!! $data->post_data !!}
                            @endif
                        </div>

                        {{-- @if ($data->attachment_id !=1)

                            <div class="col-xl-4">
                                <a class="block block-link-pop text-center" href="javascript:void(0)">
                                    <div class="block-content">
                                        <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                        alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                        <p class="font-w600"></p>

                    </div>

                    </a>

                </div>




                @endif --}}






            </div>
        </div>


        <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">

            <div class="push">



                <span class="btn-group btn-group-sm " role="group" aria-label="btnGroup1">

                    <button type="button" class="btn btn-sm btn-rounded btn-alt-danger mr-10"
                        onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                        {{ $data->post_view }} <i class="fa fa-fw fa-eye"></i>
                    </button>
                    @php
                    $check=app('\App\Http\Controllers\PostController')->admireCheck(auth::user()->id,$data->post_id,csrf_token());
                    @endphp

                    @if ($check)
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                        id="admirecountbutton{{$data->post_id  }}"
                        onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                        value="  {{ $data->admire_count }}">
                        {{ $data->admire_count }} <i class="fa fa-fw fa-heart"
                            id="admirebutton{{ $data->post_id }}"></i>
                    </button>
                    @else
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                        id="admirecountbutton{{$data->post_id  }}"
                        onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                        value="  {{ $data->admire_count }}">
                        {{ $data->admire_count }} <i class="fa fa-fw fa-heart-o"
                            id="admirebutton{{ $data->post_id }}"></i>
                    </button>
                    @endif
                    <button type="button" class="btn btn-sm btn-rounded btn-alt-success mr-10"
                        onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                        {{ $data->reply_count }} <i class="fa fa-fw fa-comment-o"></i>
                    </button>

                    <span disabled="disabled" class="btn btn-sm btn-rounded text-red mr-10"
                        id="star{{ $data->post_id }}"></span>
                </span>
                @php
                try{
                $actual_rating = floor($data->total_rating/($data->post_view));
                }
                catch(Exception $e)
                {
                $actual_rating = 0;
                }
                @endphp
                {{-- <span class="js-rating pull-right" data-score="{{ $actual_rating }}"
                data-star-on="fa fa-fw fa-2x fa-star text-warning"
                data-star-off="fa fa-fw fa-2x fa-star text-muted" data-target="#star{{ $data->post_id }}"
                onclick="dat('star{{ $data->post_id }}','{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"></span>
                --}}


                <span class="pull-right text-black">
                    Average Rating: {{ $actual_rating }}
                </span>



            </div>

        </div>
    </div>
</div>

</div>
</div>
@endforeach
@else
    <h3>No Queries Posted Yet</h3>
@endif

@endsection
