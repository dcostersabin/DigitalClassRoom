@extends('layouts.app')


@section('content')

@if ($errors->first('bookmark')!=null)

<div class="col-md-13">
    <!-- Danger Alert -->
    <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
        <p class="mb-0">

            @if ($errors->first('bookmark')!=null)
            {{ $errors->first('bookmark') }}
            @endif
        </p>

    </div>
    <!-- END Danger Alert -->
</div>

@endif

    @if (count($bookmarks)>0)
    <div class="block">
        <div class="block-content block-content-full tab-content overflow-hidden">
            <!-- Classic -->
                <div class="font-size-h3 font-w600 py-30 mb-20 text-center border-b">
                    <span class="text-primary font-w700">{{ count($bookmarks) }}</span> Bookmark found
                </div>
                <div class="row items-push">
                @foreach ($bookmarks as $item)


                    <div class="col-lg-6">
                            <form action="/bookmark/store" method="post">
                        <h4 class="h5 mb-5">
                            <a href="/post/showpost/{{ $item->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{!! strip_tags($item->post_title) !!}</a>
                                @csrf
                                <input type="hidden" name="post_id" value="{{ $item->post_id }}">
                                <input type="hidden" name="user_id" value="{{ auth::user()->id }}">
                                <button class="btn btn-sm btn-alt-danger" type="submit"><i class="fa fa-close"></i></button>


                        </h4>
                    </form>
                        <div class="font-sm text-earth mb-5"><a class="text-earth"  href="/user/profile/{{ $item->id }}/{{ csrf_token() }}">{{ $item->email }}</a></div>
                        @if(str_word_count($item->post_data)>=500)
                        @php
                        $text=substr($item->post_data,0,500);
                        $text=strip_tags($text);
                        @endphp
                        <p class="font-sm text-muted">{!! $text !!}</p>
                        @else
                        <p class="font-sm text-muted">  {!! $item->post_data !!}</p>
                        @endif

                    </div>



                @endforeach
            </div>
            <!-- END Classic -->
        </div>
    </div>
    @else

    <div class="col-md-13">
        <!-- Info Alert -->
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Information</h3>
            <p class="mb-0">No Post's Has Been Bookmarked Yet. View More Post's To Add To Bookmarks</p>
        </div>
        <!-- END Info Alert -->
    </div>


    @endif


@endsection



