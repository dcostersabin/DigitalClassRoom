@extends('layouts.app')

@section('content')

    @if ($errors->first('posted')!=null)
    <div class="col-md-13">
            <!-- Success Alert -->
            <div class="alert alert-success alert-dismissable" role="alert">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
                <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                <p class="mb-0">{{ $errors->first('posted') }}</p>
            </div>
            <!-- END Success Alert -->
        </div>
    @endif

<div class="row">
    <div class="col-md-12 ">

        <div class="block" id="showMentors">
            <div class="block-header block-header-default">
                <h3 class="block-title">Select Your Mentor</h3>
                <div class="block-options">
                    <button type="button" class="btn btn-sm btn-alt-primary" onclick="confirmMentor()">
                        <i class="fa fa-check"></i> Ask
                    </button>
                    <button type="button" class="btn btn-sm btn-alt-danger" onclick="closeBlocks('showMentors')">
                        <i class="fa fa-close"></i> Close
                    </button>
                </div>
            </div>
            <div class="block-content">
                <div class="form-group row justify-content-center">
                    <label class="col-sm-8" for="block-form-username">Mentors Email</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="block-form-username" name="block-form-username"
                            placeholder="Enter Mentors Email..." onkeyup="getMentoeInfo('{{ csrf_token() }}')">
                    </div>
                </div>
                <div class=" block form-group row justify-content-center" id="mentorInfo">
                    <div class="col-md-8 ">
                        <a class="block block-link-shadow text-center">
                            <div class="block-content block-content-full">
                                <img class="img-avatar" src="assets/media/avatars/avatar16.jpg" alt=""
                                    id="mentorProfileUrl">
                            </div>
                            <div class="block-content block-content-full block-content-sm bg-body-light">
                                <div class="font-w600 mb-5" id="mentorName">name</div>
                                <div class="font-size-sm text-muted" id="mentorEmail">Web Developer</div>
                            </div>
                            {{--  <div class="block-content">
                                <div class="row items-push">
                                    <div class="col-6">
                                        <div class="mb-5"><i class="si si-notebook fa-2x"></i></div>
                                        <div class="font-size-sm text-muted">4 Notes</div>
                                    </div>
                                    <div class="col-6">
                                        <div class="mb-5"><i class="si si-camera fa-2x"></i></div>
                                        <div class="font-size-sm text-muted">14 Photos</div>
                                    </div>
                                </div>
                            </div>  --}}
                        </a>
                    </div>
                </div>

            </div>
        </div>

    </div>

    <div class="col-md-6 col-xl-3">
        <a class="block block-link-shadow" href="javascript:void(0)" id="finalMentor">
            <div class="block-content block-content-full clearfix">
                <div class="float-right">
                    <img class="img-avatar" src="{{ asset("assets/media/avatars/avatar8.jpg") }}" alt="No Mentor Assigned" id="finalMentorProfileUrl">
                </div>
                <div class="float-left mt-10">
                    <div class="font-w600 mb-5" id="finalMentorName">Andrea Gardner</div>
                    <div class="font-size-sm text-muted" id="finalMentorEmail">Web Designer</div>
                </div>
            </div>
        </a>
    </div>





    @if ( $errors->first('post_title')!=null || $errors->first('visibility')!=null ||
    $errors->first('post_category')!=null || $errors->first('post_data')!=null || $errors->first('post_keyword')!=null
    || $errors->first('post_tags')!=null || $errors->first('attachment')!=null || $errors->first('mentor')!=null)
    <div class="col-md-12">
        <!-- Danger Alert -->
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
            @if ($errors->first('post_title')!=null)
            <p class="mb-0">Post Title Must Be Greater Than 3 Characters And Less Than 50 Characters</p><br>
            @endif
            @if ($errors->first('visibility')!=null)
            <p class="mb-0">Please Select Post Visibility</p><br>
            @endif
            @if ($errors->first('post_category')!=null)
            <p class="mb-0">Please Select Post Category</p><br>
            @endif
            @if ($errors->first('post_data')!=null)
            <p class="mb-0">Post Details Must Be Greater Than 25 Characters</p><br>
            @endif
            @if ($errors->first('post_keyword')!=null)
            <p class="mb-0">Post Keyword Must Be Greater Than 3 Characters And Less Than 50 Characters</p><br>
            @endif
            @if ($errors->first('post_tags')!=null)
            <p class="mb-0">{{ $errors->first('post_tags') }}</p><br>
            @endif
            @if ($errors->first('attachment')!=null)
            <p class="mb-0">Attachment Is Not Valid</p><br>
            @endif
            @if ($errors->first('mentor')!=null)
            <p class="mb-0">Please Assign A Mentor To Your Post  (hint: The Assigned Mentor Might Not Be A Teacher)</p><br>
            @endif
        </div>
        <!-- END Danger Alert -->
    </div>





    @endif


    <div class="col-md-12">
        <!-- Floating Labels -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Enter You're Query's Details</h3>

                <div class="block-options">
                    <div id="visibility" class="btn-block-option">
                        Public
                    </div>
                </div>
            </div>

            <div class="block-content">
                <form action="/post/create/" method="post" enctype="multipart/form-data">

                    @csrf
                    <input type="hidden" name="user_id" value={{ auth::user()->id }}>
                    <input type="hidden" name="" id="mentor" value="">
                    <div class="col-md-12">
                        <!-- Colors -->


                        <div class="row no-gutters items-push">

                            <div class="col-4">
                                <label class="css-control css-control-info css-switch">
                                    <input type="checkbox" class="css-control-input" checked="checked" name="visibility"
                                        value="public" id="public" onclick="postVisibility('public')">
                                    <span class="css-control-indicator"></span>Public
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="css-control css-control-success css-switch">
                                    <input type="checkbox" class="css-control-input" name="visibility" value="private"
                                        onclick="postVisibility('private')" id="private">
                                    <span class="css-control-indicator"></span>Private
                                </label>
                            </div>
                            <div class="col-4">
                                <label class="css-control css-control-danger css-switch">
                                    <input type="checkbox" class="css-control-input" name="visibility" value="personal"
                                        id="personal" onclick="postVisibility('personal')">
                                    <span class="css-control-indicator"></span>Personal
                                </label>
                            </div>


                        </div>
                    </div>
                    <!-- END Colors -->




                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control" id="material-addon-icon" name="post_title"
                                    placeholder="Please Enter Post Title" value="{{ old('post_title') }}">
                                <label for="material-addon-icon">Post Title (make title specific to your query)</label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-fw fa-pencil"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material input-group">
                                <input type="text" class="form-control" id="material-addon-icon" name="post_keyword"
                                    placeholder="Please Enter Post KeyWord" value="{{ old('post_keyword') }}">
                                <label for="material-addon-icon">Post KeyWord (make keywords short and sweet so other
                                    can find easily)</label>
                                <div class="input-group-append">
                                    <span class="input-group-text">
                                        <i class="fa fa-fw fa-pencil"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material input-group">
                                <input type="file" class="form-control" id="material-addon-icon" name="attachment"
                                    placeholder="Please Enter Post KeyWord" value="{{ old('attachment') }}">
                                <label for="material-addon-icon">Post Attachments (Optional. File Should Not Exceed More
                                    Than 25MB)</label>

                            </div>
                        </div>
                    </div>




                    <div class="form-group row">
                        <div class="col-md-6">
                            <div class="form-material floating">
                                <select class="form-control" id="material-select2" name="post_category">
                                    <option></option><!-- Empty value for demostrating material select box -->
                                    @foreach ($categories as $item)
                                    <option value={{ $item->category_name}}>{{ $item->category_name }}</option>
                                    @endforeach
                                </select>
                                <label for="material-select2">Please Select A Category</label>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-material">
                                <input type="text" class="js-tags-input form-control" data-height="34px"
                                    id="example-tags3" name="post_tags" value="Random">
                                <label for="example-tags3">Add Tags To Your Posts</label>
                            </div>
                        </div>
                    </div>




                    <div class="block-content">

                        <div class="form-group row">
                            <div class="col-12">
                                <!-- SimpleMDE Container -->
                                <textarea class="js-simplemde" id="simplemde" name="post_data">@if ((old('post_data'))==null) Please Enter Your Query @else {{ old('post_data') }}
                                        @endif
                                          </textarea>
                            </div>
                        </div>

                    </div>




                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <!-- END Floating Labels -->
    </div>
    @endsection
