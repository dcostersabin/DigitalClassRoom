@extends('layouts.app')
@php
$color=array("info","warning","success","danger","primary","secondary");
$count=-1;
@endphp

@section('content8')

<!-- END User -->

<!-- Updates -->



<div class="col-md-13">
        @if ($errors->first('vote') != null)
        <div class="col-md-13">
                <!-- Danger Alert -->
                <div class="alert alert-success alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0">

                        @if ($errors->first('vote')!=null)
                        {{ $errors->first('vote') }}
                        @endif
                    </p>

                </div>
                <!-- END Danger Alert -->
            </div>
        @endif

        @if ($errors->first('voteRemoved') != null)
        <div class="col-md-13">
                <!-- Danger Alert -->
                <div class="alert alert-danger alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
                    <p class="mb-0">

                        @if ($errors->first('voteRemoved')!=null)
                        {{ $errors->first('voteRemoved') }}
                        @endif
                    </p>

                </div>
                <!-- END Danger Alert -->
            </div>
        @endif


    @if ($errors->first('success')!=null)

    <div class="col-md-13">
        <!-- Danger Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">

                @if ($errors->first('success')!=null)
                {{ $errors->first('success') }}
                @endif
            </p>

        </div>
        <!-- END Danger Alert -->
    </div>

    @endif
    @if ($errors->first('answer')!=null)

    <div class="col-md-13">
        <!-- Danger Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">

                @if ($errors->first('answer')!=null)
                {{ $errors->first('answer') }}
                @endif
            </p>

        </div>
        <!-- END Danger Alert -->
    </div>

    @endif


    @if ($errors->first('bookmark')!=null)

    <div class="col-md-13">
        <!-- Danger Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">

                @if ($errors->first('bookmark')!=null)
                {{ $errors->first('bookmark') }}
                @endif
            </p>

        </div>
        <!-- END Danger Alert -->
    </div>

    @endif


    @if ($errors->first('reply_data')!=null || $errors->first('attachment')!=null)
    <div class="col-md-13">
        <!-- Danger Alert -->
        <div class="alert alert-danger alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
            <p class="mb-0">

                @if ($errors->first('reply_data')!=null)
                {{ $errors->first('reply_data') }}
                @endif
            </p>
            <p class="mb-0">

                @if ($errors->first('reply_attachment')!=null)
                {{ $errors->first('reply_attachment') }}
                @endif
            </p>
        </div>
        <!-- END Danger Alert -->
    </div>
    @endif

</div>

<div class="col-md-13">



    <div class="block block-rounded ">

        @foreach (json_decode($post) as $data)

        <div class="block block-border block-fx-shadow">
            <div class="block-content block-content-full border-t">

                <div class="media">
                    <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">
                    <div class="media-body">
                        @php
                        $time = strtotime($data->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
                        <p class="mb-0">
                            <a class="font-w600"
                                href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                            <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                            <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em>&nbsp;&nbsp;
                            <a class="text-black"><small
                                    class="badge btn-alt-primary">{{ ucwords($data->visibility) }}</small></a>


                                @if ($bookmark == 1)
                                <span class="badge btn-alt-danger"><i class="fa fa-bookmark"></i></span>

                                @endif

                        </p>
                        @php
                        $tags = (string)strip_tags($data->tag_name)."";
                        $tags = explode(',',$tags);




                        @endphp

                        <div>
                            @foreach ($tags as $tag)
                            @php

                            if($count==5)
                            {
                            $count=0;
                            }
                            else
                            {
                            $count ++;
                            }
                            @endphp
                            <span class="badge badge-{{ $color[$count] }}">{{ $tag }}</span>
                            @endforeach
                        </div>

                        <div>
                            <a class="h4 text-black"
                                href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{!!
                                $data->post_title !!}</a>

                            @if ($data->attachment_id !=1)

                            <div class="col-xl-12">

                                <div class="block-content">
                                    <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                                        alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                                    <p class="font-w600"></p>

                                </div>



                            </div>




                            @endif

                            <p>{!! $data->post_data !!}</p>

                        </div>








                    </div>
                </div>
                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">

                    <div class="push">



                        <span class="btn-group btn-group-sm " role="group" aria-label="btnGroup1">

                            <button type="button" class="btn btn-sm btn-rounded btn-alt-danger mr-10"
                                onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                                {{ $data->post_view }} <i class="fa fa-fw fa-eye"></i>
                            </button>
                            @php
                            $check=app('\App\Http\Controllers\PostController')->admireCheck(auth::user()->id,$data->post_id,csrf_token());
                            @endphp

                            @if ($check)
                            <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                                id="admirecountbutton{{$data->post_id  }}"
                                onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                                value="  {{ $data->admire_count }}">
                                {{ $data->admire_count }} <i class="fa fa-fw fa-heart"
                                    id="admirebutton{{ $data->post_id }}"></i>
                            </button>
                            @else
                            <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10"
                                id="admirecountbutton{{$data->post_id  }}"
                                onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"
                                value="  {{ $data->admire_count }}">
                                {{ $data->admire_count }} <i class="fa fa-fw fa-heart-o"
                                    id="admirebutton{{ $data->post_id }}"></i>
                            </button>
                            @endif

                            <button type="button" class="btn btn-sm btn-rounded btn-alt-success  mr-10"
                                onclick="bottom()">
                                {{ $data->reply_count }} <i class="fa fa-fw fa-comment-o"></i>
                            </button>

                            <input type="hidden" id="star{{ $data->post_id }}csrf" value="{{ csrf_token() }}">
                            <span disabled="disabled" class="btn btn-sm btn-rounded text-red mr-10"
                                id="star{{ $data->post_id }}"></span>
                        </span>
                        @php
                        try{
                        $actual_rating = $data->total_rating/($data->post_view);
                        }
                        catch(Exception $e)
                        {
                        $actual_rating = 0;
                        }
                        @endphp
                        <span class="text-danger" id="rating{{ $data->post_id }}"></span>
                        <span class="js-rating pull-right" data-score="{{ $actual_rating }}"
                            data-star-on="fa fa-fw fa-2x fa-star text-warning"
                            data-star-off="fa fa-fw fa-2x fa-star text-muted" data-target="#star{{ $data->post_id }}"
                            onclick="dat('star{{ $data->post_id }}','{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"></span>




                    </div>

                </div>
            </div>
        </div>




        @endforeach

        @foreach (json_decode($replies) as $data)
        <div class="col-xl-13">

            <div class="block block-rounded">


                <div class="block-content block-content-full">



                    <div class="media">


                        <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">
                        <div class="media-body">
                            <p class="mb-0">
                                <a class="font-w600"
                                    href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                                <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                                @php
                                $time = strtotime($data->created_at);
                                $new_time = date('Y M D',$time);
                                $voteCheck=app('\App\Http\Controllers\PostController')->voteCheck($data->reply_id,auth::user()->id,csrf_token());
                                @endphp
                                <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em> &nbsp;
                                <span class="text-earth"> Votes : <span id="voteCount{{ $data->reply_id }}">{{ $data->votes }}</span> </span> &nbsp;
                                <input type="hidden" id="ajaxValue{{ $data->reply_id }}" value="{{ $data->votes }}">
                                <span >

                                    @if ($voteCheck)
                                    <button id="voteButton{{ $data->reply_id }}" class="btn btn-sm btn-circle btn-alt-danger" onclick="vote('{{ $data->reply_id }}','{{ auth::user()->id }}','{{ csrf_token() }}')"><i class="fa  fa-arrow-circle-down" id="voteIcons{{ $data->reply_id }}"></i></button>

                                    @else
                                    <button id="voteButton{{ $data->reply_id }}" class="btn btn-sm btn-circle btn-alt-info" onclick="vote('{{ $data->reply_id }}','{{ auth::user()->id }}','{{ csrf_token() }}')"><i class="fa  fa-arrow-circle-up" id="voteIcons{{ $data->reply_id }}"></i></button>

                                    @endif

                                </span>
                                @php
                                    $temp = json_decode($post);
                                @endphp

                               @if (!(count(json_decode($answer))>0) && strcmp(auth::user()->type,"TEACHER")==0)
                               <span><button class="btn btn-sm btn-alt-danger m-md-2" onclick="setAnswer('{{ $data->reply_id }}')">Best Answer</button></span>

                               @elseif (!(count(json_decode($answer))>0) && strcmp(auth::user()->type,"STUDENT")==0 && (strcmp($temp[0]->visibility,"private")==0) && $temp[0]->id == auth::user()->id)
                               <span><button class="btn btn-sm btn-alt-danger m-md-2" onclick="setAnswer('{{ $data->reply_id }}')">Best Answer</button></span>
                               @endif

                                <form action="/set/answer" method="post" id="bestAnswer{{ $data->reply_id }}">
                                @csrf
                                <input type="hidden" name="reply_id" value="{{ $data->reply_id }}">
                                </form>

                            </p>

                            {!! $data->reply_data !!}
                            @if ($data->attachment_id != 1)
                            <div class="col-xl-12">

                                <div class="block-content">
                                    <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                                        alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                                    <p class="font-w600"></p>

                                </div>



                            </div>
                            @endif

                        </div>
                    </div>
                </div>

            </div>
        </div>
        @endforeach

        <div class="block">
            <form action="/post/reply/" method="post" enctype="multipart/form-data">
                @csrf
                <div class="block-header block-header-default">
                    <h3 class="block-title">Add Your Answer</h3>
                    <div class="block-options">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-alt-success">Submit</button>
                        </div>
                    </div>
                </div>
                <div class="block-content">

                    <input type="hidden" name="userid" value="{{ auth::user()->id }}">
                    @php
                    $temp = json_decode($post);
                    @endphp
                    <input type="hidden" name="postid" value="{{  $temp[0]->post_id }}">
                    <div class="form-group row">
                        <div class="col-12">
                            <!-- SimpleMDE Container -->
                            <textarea class="js-simplemde" id="simplemde" name="reply_data">@if ((old('reply_data'))==null) Please Enter Your Reply @else {{ old('reply_data') }}
                                @endif
                                  </textarea>
                        </div>
                    </div>
                    <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material input-group">
                                <input type="file" class="form-control" id="material-addon-icon" name="reply_attachment"
                                    placeholder="Please Enter Post KeyWord" value="{{ old('reply_attachment') }}">
                                <label for="material-addon-icon">Attachments (Optional. File Should Not Exceed More
                                    Than 25MB)</label>

                            </div>
                        </div>
                    </div>
                </div>



            </form>
        </div>
    </div>

</div>



@endsection


@section('content4')
<div class="col-md-13">
        <div class="row">
                <div class="col-6 col-md-4 col-xl-4">
                    <a class="block block-link-pop text-center" href="javascript:void(0)">
                        <div class="block-content">
                            <p class="font-size-h1">
                                <strong>{{ count(json_decode($replies)) }}</strong>
                            </p>
                            <p class="font-w600">Total Replies</p>
                        </div>
                    </a>
                </div>
                @if ($bookmark == 0)
                <div class="col-6 col-md-4 col-xl-4">
                    <a class="block block-link-shadow text-center" href="javascript:void(0)">
                        <div class="block-content" onclick="bookmark('{{ $temp[0]->post_id }}')">
                            <p class="mt-5">
                                <i class="text-primary fa fa-bookmark-o fa-4x"></i>
                            </p>
                            <p class="font-w600">Bookmark</p>
                        </div>
                    </a>
                </div>


                @endif

                <form action="/bookmark/store" method="post" id="bookmarkForm{{ $temp[0]->post_id }}">
                    <input type="hidden" name="post_id" value="{{ $temp[0]->post_id }}">
                    @csrf
                    <input type="hidden" name="user_id" value="{{ auth::user()->id }}">
                </form>


                @if (count(json_decode($answer))>0)
                <div class="col-6 col-md-4 col-xl-4">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content">
                                <p class="mt-5">
                                    <i class="text-success fa fa-check fa-4x"></i>
                                </p>
                                <p class="font-w600"> Verified Answer</p>
                            </div>
                        </a>
                    </div>
                @else
                <div class="col-6 col-md-4 col-xl-4">
                        <a class="block block-link-shadow text-center" href="javascript:void(0)">
                            <div class="block-content" >
                                <p class="mt-5">
                                    <i class="text-danger fa fa-close fa-4x"></i>
                                </p>
                                <p class="font-w600"> Verified Answer</p>
                            </div>
                        </a>
                    </div>
                @endif

              </div>
    @if (count(json_decode($answer))>0)
    @foreach (json_decode($answer) as $data)
    <div class="col-xl-13">

        <div class="block block-rounded">



            <div class="block-content block-content-full">

                    <div class="block-title">
                           <h5 class="text-info">Best Verified Answer By</h5>
                        </div>

                <div class="media">


                    <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">
                    <div class="media-body">
                        <p class="mb-0">
                            <a class="font-w600"
                                href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                            <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                            @php
                            $time = strtotime($data->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                            <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em> &nbsp;
                            <div class="text-earth"> Votes : <span id="voteCount{{ $data->reply_id }}">{{ $data->votes }}</span> </div> &nbsp;
                            <input type="hidden" id="ajaxValue{{ $data->reply_id }}" value="{{ $data->votes }}">




                        </p>

                        {!! $data->reply_data !!}
                        @if ($data->attachment_id != 1)
                        <div class="col-xl-12">

                            <div class="block-content">
                                <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                                    alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                                <p class="font-w600"></p>

                            </div>



                        </div>
                        @endif

                    </div>
                </div>
            </div>

        </div>
    </div>
    @endforeach

    @else
    <div class="row">
            <div class="col-md-12">
                <!-- Primary Alert -->
                <div class="alert alert-info alert-dismissable" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <h3 class="alert-heading font-size-h4 font-w400">No Best Answer Found</h3>
                    <p class="mb-0">There are no replies that satisfies this query add a reply <br><br> <button
                            onclick="bottom()" class="btn btn-sm btn-alt-danger">Add Your Reply</button></p>
                </div>
                <!-- END Primary Alert -->
            </div>
        </div>
    @endif

</div>


@foreach (json_decode($bestVoted) as $data)
<div class="col-xl-13">

    <div class="block block-rounded">



        <div class="block-content block-content-full">

                <div class="block-title">
                       <h5 class="text-info">Best Voted Answer By</h5>
                    </div>

            <div class="media">


                <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">
                <div class="media-body">
                    <p class="mb-0">
                        <a class="font-w600"
                            href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                        <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                        @php
                        $time = strtotime($data->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
                        <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em> &nbsp;
                        <div class="text-earth"> Votes : <span id="voteCount{{ $data->reply_id }}">{{ $data->votes }}</span> </div> &nbsp;
                        <input type="hidden" id="ajaxValue{{ $data->reply_id }}" value="{{ $data->votes }}">




                    </p>

                    {!! $data->reply_data !!}
                    @if ($data->attachment_id != 1)
                    <div class="col-xl-12">

                        <div class="block-content">
                            <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                                alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                            <p class="font-w600"></p>

                        </div>



                    </div>
                    @endif

                </div>
            </div>
        </div>

    </div>
</div>
@endforeach

@endsection
