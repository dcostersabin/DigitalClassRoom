@extends('layouts.app')


@section('content')

<div class="row">

    @php
    $item = json_decode($additional_info);
@endphp

<div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1">
                    <strong>{{ $item->total_post_count }}</strong>
                </p>
                <p class="font-w600"><i class="fa fa-pencil text-muted mr-5"></i>Total Posts</p>
            </div>
        </a>
    </div>



    <div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1 text-success">
                    <strong>{{ $item->total_replies_count }}</strong>
                </p>
                <p class="font-w600">
                    <i class="fa fa-reply text-muted mr-5"></i> Total Replies
                </p>
            </div>
        </a>
    </div>
    <div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1 text-danger">
                    <strong>{{ $item->total_admires_count }}</strong>
                </p>
                <p class="font-w600">
                    <i class="fa fa-heart text-muted mr-5"></i> Total Admires Given
                </p>
            </div>
        </a>
    </div>
    <div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1 text-flat">
                    <strong>{{ $item->total_admire_received }}</strong>
                </p>
                <p class="font-w600">
                    <i class="fa fa-heart-o text-muted mr-5"></i> Admire Received
                </p>
            </div>
        </a>
    </div>
    <div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1 text-warning">
                    <strong>{{ $item->followers_count }}</strong>
                </p>
                <p class="font-w600">
                    <i class=" si si-user-follow  text-muted mr-5"></i> Followers
                </p>
            </div>
        </a>
    </div>
    <div class="col-6 col-md-4 col-xl-2">
        <a class="block block-link-pop text-center" href="javascript:void(0)">
            <div class="block-content">
                <p class="font-size-h1 text-pulse">
                    <strong>{{ $item->following_count }}</strong>
                </p>
                <p class="font-w600">
                    <i class="si si-user-following  text-muted mr-5"></i> Following
                </p>
            </div>
        </a>
    </div>
</div>
@if ($errors->first('profile_image') != null)
<div class="col-md-13">
    <!-- Danger Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
        <p class="mb-0">{{ $errors->first('profile_image') }}</p>
    </div>
    <!-- END Danger Alert -->
</div>
@endif
@php
$color=array("info","warning","success","danger","primary","secondary");
$count=-1;
@endphp


<div class="">
    <div class="col-md-13">


            <div class="block-content">
                <div class="bg-image bg-image-bottom" style="background-image: url('{{ asset("assets/media/photos/photo17@2x.jpg") }}');">
                    <div class="bg-primary-dark-op py-30">
                        <div class="content content-full text-center">
                            <!-- Avatar -->
                            <div class="mb-15">
                                <a class="img-link" >
                                    <img class="img-avatar img-avatar96 img-avatar-thumb" src="{{ asset($user_info->profile_url) }}" alt="">
                                </a>
                            </div>
                            <!-- END Avatar -->

                            <!-- Personal -->
                            <h1 class="h3 text-white font-w700 mb-10">{{ $user_info->name }}</h1>
                            <h2 class="h5 text-white-op">
                                <a class="text-primary-light" href="javascript:void(0)">{{ $user_info->email }}</a>
                            </h2>
                            <!-- END Personal -->

                            <!-- Actions -->
                            @if (auth::user()->id != $user_info->id)



                                @if (strcmp($follow,"yes")==0)
                                            <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-success mb-5" id="followButton{{ auth::user()->id }}" onclick="follow('{{ auth::user()->id }}','{{ $user_info->id }}','{{ csrf_token() }}')" >
                                                <i class="fa fa-check mr-5" ></i> Following
                                            </button>
                                @else
                                        <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-danger mb-5" id="followButton{{ auth::user()->id }}" onclick="follow('{{ auth::user()->id }}','{{ $user_info->id }}','{{ csrf_token() }}')" >
                                            <i class="fa fa-plus mr-5"></i> Follow
                                        </button>
                                @endif
                            @else

                            <button type="button" class="btn btn-rounded btn-hero btn-sm btn-alt-primary mb-5" onclick="showBlocks('editProfile')">
                                <i class="fa fa-pencil mr-5"></i> Edit
                            </button>
                            @endif

                            <!-- END Actions -->
                        </div>
                    </div>
                </div>

        </div>
    </div>




    <div class="col-md-13">
        <div class="block-content">
            <div id="editProfile">
                <form action="/profile/changeImage" method="post"  enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="userid" value="{{ auth::user()->id }}">
                    <div class="block">
                        <div class="block-header block-header-default">
                            <h3 class="block-title">Change Profile Picture</h3>
                            <div class="block-options">
                                <button type="submit" class="btn btn-sm btn-alt-primary">
                                    <i class="fa fa-check"></i> Submit
                                </button>
                                <button type="button" class="btn btn-sm btn-alt-danger" onclick="closeBlocks('editProfile')">
                                    <i class="fa fa-close"></i> Close
                                </button>
                            </div>
                        </div>
                        <div class="block-content">
                            <div class="form-group row justify-content-center">
                                <label class="col-sm-8" for="block-form-username3">Upload A Picture</label>
                                <div class="col-sm-8">
                                    <input type="file" class="form-control" name="profile_image" id="profile_image">
                                </div>
                            </div>

                        </div>
                    </div>
                </form>




            </div>
        </div>
    </div>





                <div class="col-md-12">

                        @foreach ($public as $data)

<div class="col-md-13 ">
        <div class="block block-rounded ">
            <div class="block block-border block-fx-shadow">

                <div class="block-content block-content-full border-t">
                    <div class="media">
                        <img class="img-avatar img-avatar48 mr-20" src="{{ asset($data->profile_url) }}" alt="">

                        <div class="media-body">
                        @php
                        $time = strtotime($data->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
                            <p class="mb-0">
                                <a class="font-w600" href="/user/profile/{{ $data->id }}/{{ csrf_token() }}">{{ $data->name }}</a>
                                <a class="text-muted" href="javascript:void(0)">{{ $data->email }}</a>
                                <em class="text-muted">&bull;&nbsp;{{ $new_time }}</em>&nbsp;&nbsp;
                                <a class="text-black"><small class="badge btn-alt-primary">{{ ucwords($data->visibility) }}</small></a>

                            </p>

                            @php
                                $tags = (string)strip_tags($data->tag_name)."";
                                $tags = explode(',',$tags);




                            @endphp

                            <div>
                                @foreach ($tags as $tag)
                                @php

                            if($count==5)
                            {
                                $count=0;
                            }
                            else
                            {
                                $count ++;
                            }
                            @endphp
                                    <span class="badge badge-{{ $color[$count] }}">{{ $tag }}</span>
                            @endforeach
                            </div>


                            <div>
                                <a class="h4 text-black" href="/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}">{!! strip_tags($data->post_title) !!}</a>

                                @if(str_word_count($data->post_data)>=500)
                                @php
                                $text=substr($data->post_data,0,500);
                                $text=strip_tags($text);

                                $text=$text."........";
                                @endphp
                                <p>{!! $text !!}</p>
                                @else
                                {!! $data->post_data !!}
                                @endif
                            </div>

                            {{-- @if ($data->attachment_id !=1)

                            <div class="col-xl-4">
                                <a class="block block-link-pop text-center" href="javascript:void(0)">
                                    <div class="block-content">
                                        <img class="img-fluid img-thumb bg-gray" src="{{ asset($data->attachment_url) }}"
                                            alt="" onclick="window.location.href='{{ asset($data->attachment_url) }}'">
                                        <p class="font-w600"></p>

                                    </div>

                                </a>

                            </div>




                            @endif --}}






                        </div>
                    </div>


                    <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">

                        <div class="push">



                            <span class="btn-group btn-group-sm " role="group" aria-label="btnGroup1">

                                <button type="button" class="btn btn-sm btn-rounded btn-alt-danger mr-10" onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                                   {{ $data->post_view }} <i class="fa fa-fw fa-eye"></i>
                                </button>
                                @php
                                     $check=app('\App\Http\Controllers\PostController')->admireCheck(auth::user()->id,$data->post_id,csrf_token());
                                @endphp

                                @if ($check)
                                <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10" id="admirecountbutton{{$data->post_id  }}"
                                    onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')" value="  {{ $data->admire_count }}">
                                    {{ $data->admire_count }} <i class="fa fa-fw fa-heart" id="admirebutton{{ $data->post_id }}"></i>
                                    </button>
                                @else
                                <button type="button" class="btn btn-sm btn-rounded btn-alt-primary mr-10" id="admirecountbutton{{$data->post_id  }}"
                                    onclick="admireRegister('{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')" value="  {{ $data->admire_count }}">
                                    {{ $data->admire_count }} <i class="fa fa-fw fa-heart-o" id="admirebutton{{ $data->post_id }}"></i>
                                    </button>
                                @endif
                                <button type="button" class="btn btn-sm btn-rounded btn-alt-success mr-10" onclick="showPost('/post/showpost/{{ $data->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}')">
                                    {{ $data->reply_count }}    <i class="fa fa-fw fa-comment-o"></i>
                                 </button>

                                <span disabled="disabled" class="btn btn-sm btn-rounded text-red mr-10"
                                    id="star{{ $data->post_id }}"></span>
                            </span>
                            @php
                                try{
                                    $actual_rating = floor($data->total_rating/($data->post_view));
                                }
                                catch(Exception $e)
                                {
                                    $actual_rating = 0;
                                }
                            @endphp
                            {{-- <span class="js-rating pull-right" data-score="{{ $actual_rating }}"
                                data-star-on="fa fa-fw fa-2x fa-star text-warning"
                                data-star-off="fa fa-fw fa-2x fa-star text-muted" data-target="#star{{ $data->post_id }}"
                                onclick="dat('star{{ $data->post_id }}','{{ csrf_token() }}','{{ $data->post_id }}','{{ auth::user()->id }}')"></span> --}}


                                <span class="pull-right text-black">
                                    Average Rating: {{ $actual_rating }}
                                </span>



                        </div>

                    </div>
                </div>
            </div>

        </div>
    </div>

                        @endforeach

                </div>





@endsection
