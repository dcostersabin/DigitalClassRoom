@extends('layouts.app')

@section('content')



@if (($notice) != 0)
<div class="row">
        <div class="col-md-12">
                <!-- Info Alert -->
                <div class="alert alert-info alert-dismissable" role="alert">

                    <h3 class="alert-heading font-size-h4 font-w400">You Have An Unseen Notice</h3>
                    <p class="mb-0">Please Visit College Notice Page On The Left Tab</p>
                </div>
                <!-- END Info Alert -->
            </div>
</div>
@endif
<div class="row">
        @foreach ($replies['notseen'] as $item)
        <div class="col-md-4">
                <a class="block block-rounded block-link-shadow " href="javascript:void(0)">
                        <div class="block-content block-content">
                                <div class="block-content block-content clearfix">
                                        <div class="text-left float-right ">
                                            <div class="font-w600 mb-0">{{ $item->name }}</div>
                                            <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                        </div>
                                        <div class="float-left">
                                            <img class="img-avatar" src={{ asset($item->profile_url) }} alt="">
                                        </div>
                                    </div>
                            </div>
                    <div class="block-content block-content-full">
                            @php
                            $time = strtotime($item->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                        <p class="font-size-sm text-muted float-sm-right mb-0"><em>{{ $new_time }}</em>
                             <span><button class="btn btn-sm btn-outline-info mr-3 ml-4 " onclick="window.location.href='/post/showpost/{{ $item->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}'">Show Post</button></span>
                            <span><button class="btn btn-sm btn-outline-danger mr-3 ml-4 " onclick="markasread('markReply','{{ $item->reply_id }}')">Mark As Read</button></span></p>
                        <h4 class="font-size-default text-primary mb-0">
                            <i class="fa fa-bell text-muted mr-5"></i> New Reply

                        </h4>
                    </div>

                    <form action="/markAsRead" method="post" id="markReply{{ $item->reply_id }}">
                    @csrf
                    <input type="hidden" name="table" value="replies">
                    <input type="hidden" name="id" value="{{ $item->reply_id }}">
                </form>

                </a>
        </div>
        @endforeach


        @foreach ($followers['notseen'] as $item)
        <div class="col-md-4">
                <a class="block block-rounded block-link-shadow " href="javascript:void(0)">
                        <div class="block-content block-content">
                                <div class="block-content block-content clearfix ">
                                        <div class="float-right mr-3">
                                                <img class="img-avatar" src={{ asset(auth::user()->profile_url) }} alt="">
                                            </div>
                                        <div class="float-left">
                                            <img class="img-avatar" src={{ asset($item->profile_url) }} alt="">
                                        </div>

                                             <div class=" float-left ml-3">
                                                <div class="font-w600 ">{{ $item->name }}</div>
                                                <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                             </div>

                                    </div>

                            </div>
                    <div class="block-content block-content-full">
                            @php
                            $time = strtotime($item->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                        <p class="font-size-sm text-muted float-sm-right mb-0"><em>{{ $new_time }}</em>
                            <span><button class="btn btn-sm btn-outline-warning mr-3 ml-4 " onclick="window.location.href='/user/profile/{{ $item->id }}/{{ csrf_token() }}'">Show Profile</button></span>
                            <span><button class="btn btn-sm btn-outline-danger mr-3 ml-4 " onclick="markasread('markFollowers','{{ $item->followid }}')">Mark As Read</button></span></p>
                        <h4 class="font-size-default text-primary mb-0">
                            <i class="fa fa-bell text-muted mr-5"></i> New Follower

                        </h4>
                    </div>
                    <form action="/markAsRead" method="post" id="markFollowers{{ $item->followid }}">
                        @csrf
                        <input type="hidden" name="table" value="followers">
                        <input type="hidden" name="id" value="{{ $item->followid }}">
                    </form>
                </a>
        </div>
        @endforeach
        @foreach ($followers['seen'] as $item)
        <div class="col-md-4">
                <a class="block block-rounded block-link-shadow bg-gray-light" href="javascript:void(0)">
                        <div class="block-content block-content">
                                <div class="block-content block-content clearfix">
                                        <div class="float-right ml-3">
                                                <img class="img-avatar" src={{ asset(auth::user()->profile_url) }} alt="">
                                            </div>
                                        <div class="float-left">
                                            <img class="img-avatar" src={{ asset($item->profile_url) }} alt="">
                                        </div>
                                        <div class=" float-left ml-3">
                                            <div class="font-w600 ">{{ $item->name }}</div>
                                            <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                         </div>
                                    </div>
                            </div>
                    <div class="block-content block-content-full">
                            @php
                            $time = strtotime($item->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                        <p class="font-size-sm text-muted float-sm-right mb-0"><em>{{ $new_time }}</em>
                             <span><button class="btn btn-sm btn-outline-warning mr-3 ml-4 " onclick="window.location.href='/user/profile/{{ $item->id }}/{{ csrf_token() }}'">Show Profile</button></span></p>
                        <h4 class="font-size-default text-primary mb-0">
                            <i class="fa fa-bell text-muted mr-5"></i>  New Follower

                        </h4>
                    </div>

                </a>
        </div>
        @endforeach
        @foreach ($replies['seen'] as $item)
        <div class="col-md-4">
                <a class="block block-rounded block-link-shadow bg-gray-light " href="javascript:void(0)">
                        <div class="block-content block-content">
                                <div class="block-content block-content clearfix">
                                        <div class="text-right float-right mt-1">
                                            <div class="font-w600 mb-0">{{ $item->name }}</div>
                                            <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                        </div>
                                        <div class="float-left">
                                            <img class="img-avatar" src={{ asset($item->profile_url) }} alt="">
                                        </div>
                                    </div>
                            </div>
                    <div class="block-content block-content-full">
                            @php
                            $time = strtotime($item->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                        <p class="font-size-sm text-muted float-sm-right mb-0"><em>{{ $new_time }}</em>
                             <span><button class="btn btn-sm btn-outline-info mr-3 ml-4 " onclick="window.location.href='/post/showpost/{{ $item->post_id }}/{{ csrf_token() }}/{{ auth::user()->id }}'">Show Post</button></span>
                           </p>
                        <h4 class="font-size-default text-primary mb-0">
                            <i class="fa fa-bell text-muted mr-5"></i> New Reply

                        </h4>
                    </div>

                </a>
        </div>
        @endforeach

</div>
@endsection

