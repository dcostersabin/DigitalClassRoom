@extends('layouts.app')

@section('content')


@foreach ($errors->all() as $item)
{!! $item !!}
@endforeach



<div class="col-md-13">
        <!-- Floating Labels -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Enter Syllabus Details</h3>

            </div>
            <div class="block-content">
                <form action="/submit/syllabus" method="POST" >
                    <input type="hidden" name="table_name" value="{{ $table_name }}">
                    <input type="hidden" name="totalchapter" value="{{ $chapter }}">
                    @csrf
                @for ( $i=1;  $i<=$chapter ; $i++)
                <div class="form-group row">
                        <div class="col-md-12">
                            <div class="form-material floating">
                                <input type="text" class="form-control" id="material-text2" name="cname{{ $i }}">
                                <label for="material-text2">Chapter {{ $i }} Title</label>
                            </div>

                            <div class="form-group row">
                                    <div class="col-12">
                                        <!-- SimpleMDE Container -->
                                        <textarea class="js-simplemde" id="simplemde" name="cdata{{ $i }}">Chapter {{ $i }} Description</textarea>
                                    </div>
                                </div>

                        </div>

                    </div>
                @endfor

                    <div class="form-group row">
                        <div class="col-md-9">
                            <button type="submit" class="btn btn-alt-primary">Submit</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>



@endsection
