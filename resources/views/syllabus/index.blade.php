@extends('layouts.app')

@section('content')

@if ($errors->first('success') != null)
<div class="alert alert-success alert-dismissable" role="alert">
    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
        <span aria-hidden="true">&times;</span>
    </button>
    <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
    <p class="mb-0">{{ $errors->first('success') }}!</p>
</div>
@endif
@if (count($subjects)>0)
<div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Subjects</h3>

        </div>
        <div class="block-content">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th>Name</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Code</th>
                        <th class="text-center" style="width: 100px;">Program</th>
                        <th class="text-center" style="width: 100px;">Semester</th>
                        <th class="text-center" style="width: 100px;">Edit</th>
                    </tr>
                </thead>
                <tbody>


                        @foreach ($subjects as $item)
                        <tr>
                            <th class="text-center" scope="row">{{ $item->subject_id }}</th>
                        <th class="text-center" scope="row">{{ $item->name }}</th>
                        <th class="text-center" scope="row">{{ $item->subject_code }}</th>
                        <th class="text-center" scope="row">{{ $item->program_name }}</th>
                        <th class="text-center" scope="row">{{ $item->semester }}</th>
                        <th class="text-center" scope="row"><button class="btn btn-sm btn-info" onclick="window.location.href='/edit/syllabus/{{ csrf_token() }}/{{ $item->syllabus_id }}'">Edit</button></th>


                        </tr>

                        @endforeach





                </tbody>
            </table>
        </div>
    </div>
@else
<div class="col-md-13">
        <!-- Success Alert -->
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
            <p class="mb-0">No Subjects Added To The Database</p>
        </div>
        <!-- END Success Alert -->
    </div>
@endif



@endsection
