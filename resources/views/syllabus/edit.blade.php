@extends('layouts.app')

@section('content8')
@if ($errors->first('success') != null)
<div class="col-md-13" id="message">
    <!-- Success Alert -->
    <div class="alert alert-success alert-dismissable" role="alert" id="alertType">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
        <p class="mb-0" id="messageDiv">{{ $errors->first('success') }}</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif
@if ($content)


@section('content')
<div class="block">
        <div class="block-header block-header-default">
                <h3 class="block-title">{{ $subject_name }}</h3>
                <div class="block-options">
                    Syllabus Details
                </div>
            </div>
    <div class="block-content">
        <table class="table table-bordered table-vcenter">
            <thead>
                <tr>
                    <th class="text-center" style="width: 50px;">#</th>
                    <th class="d-none d-sm-table-cell" style="width: 25%;">Chapter Name</th>
                    <th>Topics</th>


                </tr>
            </thead>
            <tbody>
                @foreach ($data as $item)
                <tr>
                    <th class="text-center" scope="row">{{ $item->chapter_no }}</th>
                    <td>{{ strip_tags($item->chapter_name) }}</td>
                    <td class="text-black d-sm-table-cell">
                        {!! $item->topics !!}
                    </td>

                </tr>
                @endforeach



            </tbody>
        </table>
    </div>
</div>

@endsection




@else
<div class="col-md-13" id="message">
    <!-- Success Alert -->
    <div class="alert alert-info alert-dismissable" role="alert" id="alertType">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
        <p class="mb-0" id="messageDiv">No Data Present To Show Insert Data Using The Form</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif
@endsection

@section('content4')
@if (!$content)

@if ($errors->first('chapter') != null)
<div class="col-md-13" id="message">
    <!-- Success Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert" id="alertType">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
        <p class="mb-0" id="messageDiv">{{ $errors->first('chapter') }}</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif
<!-- Inline Form -->
<div class="col-md-10">
    <div class="block">

        <div class="block-header block-header-default">
            <h3 class="block-title">Syllabus Table Generator </h3>
            <div class="block-options">
                <button type="button" class="btn-block-option">
                    <i class="si si-wrench"></i>
                </button>
            </div>
        </div>
        <div class="block-content block-content-full" id="form">

            <form class="form-inline" method="get" action="/generate/form">
                @csrf
                <input type="hidden" name="table_name" value="{{ $table_name }}">
                <label class="sr-only" for="example-if-email">Number Of Chapters</label>
                <input type="text" class="form-control mb-2 mr-sm-2 mb-sm-0" id="chapter" name="chapter"
                    placeholder="Enter Number Of Chapters">

                <button type="submit" class="btn btn-alt-primary" onclick="generateSyllabusForm()">Generate
                    Table</button>
            </form>
        </div>
    </div>
</div>
<!-- END Inline Form -->


@endif
@endsection
