@extends('layouts.app')

@section('content')
@if (count($subjects)>0)
<!-- Striped Table -->
<div class="block">
    <div class="block-header block-header-default">
        <h3 class="block-title">Subjects</h3>
        <div class="block-options">
            <div class="block-options-item">
                <code>Total Subjects {{ count($subjects) }}</code>
            </div>
        </div>
    </div>
    <div class="block-content">
        <table class="table table-striped table-vcenter ">
            <thead>
                <tr  class="text-center">
                    <th class="text-center" style="width: 50px;">#</th>
                    <th>Subject Name</th>
                    <th class="text-center" style="width: 200px;">Subject Code</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;">Semester</th>
                    <th class="d-none d-sm-table-cell" style="width: 15%;"></th>

                </tr>
            </thead>
            <tbody>
                @php
                    $counter = 0;
                @endphp
                @foreach ($subjects as $item)
                @php
                    $counter ++;
                @endphp
                <tr class="text-center">
                        <th class="text-center" scope="row">{{ $counter }}</th>
                        <td>{{ $item->name }}</td>
                        <td>{{ $item->subject_code }}</td>
                        <td class="d-none d-sm-table-cell">
                            <span class="badge badge-primary">{{ $item->semester }}</span>
                        </td>
                        <td><button class="btn btn-sm btn-alt-primary" onclick="window.location.href='/show/syllabus/student/{{ csrf_token() }}/{{ $item->subject_code }}'">Show Syllabus</button></td>
                    </tr>
                @endforeach

            </tbody>
        </table>
    </div>
</div>
<!-- END Striped Table -->
@else
<h4>Opps No Subject Added</h4>
@endif
@endsection

