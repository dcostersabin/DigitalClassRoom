@extends('layouts.app')
@section('content')

@if (isset($notice) && count($notice)>0)

<div class="content">

    <h2 class="content-heading d-print-none">
        <button type="button" class="btn btn-sm btn-rounded btn-success float-right "
            onclick="window.location.href='/post/notice/college/{{ csrf_token() }}'">New Notice</button>
        Posted Notice
    </h2>
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">{{ count($notice) }} Notices Posted</h3>

        </div>
        <div class="block-content">

            <!-- Table -->
            <div class="table-responsive push">
                <table class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" style="width: 60px;">#</th>
                            <th>Notice Title</th>
                            <th class="text-center" style="width: 90px;">Batch</th>
                            <th class="text-center" style="width: 120px;">Program</th>
                            <th class="text-center" style="width: 120px;">Notice Type</th>
                            <th class="text-center " style="width:120px;">Created At</th>
                            <th class="text-center" style="width: 90px;">GoTo</th>
                        </tr>
                    </thead>
                    <tbody>
                        @php
                            $counter = 0
                        @endphp

                        @foreach ($notice as $data)
                        @php
                            $counter ++
                        @endphp
                        <tr>
                            <td class="text-center">{{ $counter }}</td>
                            <td class="text-muted h5">
                            {!! $data->notice_title!!}

                            </td>
                            <td class="text-center">
                                <span class="badge badge-pill badge-primary">{{ $data->batch_no }}</span>
                            </td>
                            @php
                            $time = strtotime($data->created_at);
                            $new_time = date('Y M D',$time);
                            @endphp
                            <td class="text-center">{{ $data->program_name }}</td>
                            <td class="text-center">{{ $data->notice_type }}</td>
                            <td class="text-center">{{ $new_time }}</td>
                            <td class="text-center"> <button class="badge badge-pill badge-info" onclick="window.location.href='/show/notice/{{ $data->id }}/{{ csrf_token() }}'">Show</button></td>
                        </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
            <!-- END Table -->


        </div>
    </div>
</div>
@else
<div class="col-md-13">
    <!-- Success Alert -->
    <div class="alert alert-info alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
        <p class="mb-0">No Notice Added To The Database <button type="button" class="btn btn-sm btn-rounded btn-info  "
            onclick="window.location.href='/post/notice/college/{{ csrf_token() }}'">New Notice</button></p>

    </div>
    <!-- END Success Alert -->
</div>
@endif
@endsection



