@extends('layouts.app')




@section('content')
@if ($errors->first('notice_title') != null || $errors->first('notice_details') != null || $errors->first('batch') != null || $errors->first('program') != null || $errors->first('type') != null || $errors->first('attachment') != null)
<div class="col-md-12">
    <!-- Danger Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
        @if ($errors->first('notice_title')!=null)
        <p class="mb-0">{{ $errors->first('notice_title') }}</p><br>
        @endif
        @if ($errors->first('notice_details')!=null)
        <p class="mb-0">{{ $errors->first('notice_details') }}</p><br>
        @endif
        @if ($errors->first('batch')!=null)
        <p class="mb-0">{{ $errors->first('batch') }}</p><br>
        @endif
        @if ($errors->first('program')!=null)
        <p class="mb-0">{{ $errors->first('program') }}</p><br>
        @endif
        @if ($errors->first('type')!=null)
        <p class="mb-0">{{ $errors->first('type') }}</p><br>
        @endif
        @if ($errors->first('attachment')!=null)
        <p class="mb-0">{{ $errors->first('attachment') }}</p><br>
        @endif

    </div>
    <!-- END Danger Alert -->
</div>



@endif


@if ($errors->first('posted') != null)
<div class="col-md-12">
    <!-- Danger Alert -->
    <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>

        <p class="mb-0">{{ $errors->first('posted') }}</p><br>

    </div>

</div>
@endif
<div class="col-md-12">
    <!-- Normal Form -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Post Notice</h3>
            <div class="block-options">

            </div>
        </div>
        <div class="block-content">
            <form action="/post/notice/store" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="example-nf-email">Notice Title</label>
                    <input type="text" class="form-control" id="example-nf-email" name="notice_title" placeholder="Enter Notice Title" value="{{ old('notice_title') }}">
                </div>

                <div class="form-group row">
                    <label class="col-12" for="example-select">Program</label>
                    <div class="col-md-12">
                        <select class="form-control" id="example-select" name="program">
                            <option value="">Please select A Program</option>
                            @foreach (json_decode($programs) as $item)
                          @if ($item->program_id != 999)
                          <option value="{{ $item->program_id }}">{{ $item->program_name }}</option>
                          @endif
                            @endforeach

                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-12" for="example-select">Batch</label>
                    <div class="col-md-12">
                        <select class="form-control" id="example-select" name="batch">
                            <option value="">Please select A Batch</option>
                            @foreach (json_decode($batches) as $item)
                          @if ($item->batch_id != 999)
                          <option value="{{ $item->batch_id }}">{{ $item->batch_no }}</option>
                          @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-12" for="example-select">Type</label>
                    <div class="col-md-12">
                        <select class="form-control" id="example-select" name="type">
                            <option value="">Please select Notice Type</option>
                            @foreach (json_decode($types) as $item)
                            <option value="{{ $item->id }}">{{ $item->notice_type }}</option>
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label class="col-12">Attachment</label>
                    <div class="col-12">
                        <div class="custom-file">
                            <input type="file" class="custom-file-input" id="example-file-input-custom" name="attachment" data-toggle="custom-file-input">
                            <label class="custom-file-label" for="example-file-input-custom">Choose file</label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                        <label class="col-12">Notice Details</label>
                    <div class="col-12">
                        <!-- SimpleMDE Container -->
                        <textarea class="js-simplemde" id="simplemde" name="notice_details" >@if ((old('notice_details'))==null) Description @else {{ old('notice_details') }}
                            @endif</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-alt-primary">Submit</button>
                </div>
            </form>
        </div>
    </div>
@endsection

