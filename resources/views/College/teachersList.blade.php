@extends('layouts.app')

@section('content')
<div class="row">
@foreach ($teachers as $item)

        <!-- Row #1 -->
        <div class="col-md-6 col-xl-3" onclick="window.location.href='/user/profile/{{ $item->id }}/{{ csrf_token() }}'">
            <a class="block block-link-shadow" href="javascript:void(0)">
                <div class="block-content block-content-full clearfix">
                    <div class="float-right">
                        <img class="img-avatar" src={{ asset($item->profile_url) }} alt="">
                    </div>
                    <div class="float-left mt-10">
                        <div class="font-w600 mb-5">{{ $item->name }}</div>
                        <div class="font-size-sm text-muted">{{ $item->email }}</div>
                    </div>
                </div>
            </a>
        </div>

@endforeach
</div>
@endsection

