@extends('layouts.app')

@section('content8')
@if (count($syllabus)>0)
<div class="content">

@foreach ($syllabus as $item)
<div class="col-md-13">
        <div class="block block-rounded">
            <div class="block-header block-header-default">
                <h4 >Chapter {{ $item->chapter_no }} : <small class="text-muted h4">{{ strip_tags($item->chapter_name) }}</small> </h4>
                <button class="btn btn-sm btn-alt-primary" onclick="addReferences('{{ $item->chapter_no }}')"> <i class="fa fa-plus"></i> References</button>
            </div>
            <div class="block-content">
                {!! $item->topics !!}
            </div>
        </div>
    </div>
@endforeach
</div>
@else
<h4>No Content Added To Syllabus</h4>
@endif
@endsection



@section('content4')
@if ($errors->first('added')!= null )
<div class="col-md-12">
    <!-- Danger Alert -->
    <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Added</h3>
        <p class="mb-0">{{ $errors->first('added') }} </p>
    </div>
    <!-- END Danger Alert -->
</div>
@endif
@if ($errors->first('chapter')!= null || $errors->first('link') != null || $errors->first('name') != null)
<div class="col-md-12">
    <!-- Danger Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
        <p class="mb-0">{{ $errors->first('chapter') }} <br> {{ $errors->first('link') }} <br> {{ $errors->first('name') }}</p>
    </div>
    <!-- END Danger Alert -->
</div>
@endif
<div class="col-md-12 d-none"
 id="referencesForm">
    <form action="/add/references" method="post">
        @csrf
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Add References</h3>
                <div class="block-options">
                    <button type="submit" class="btn btn-sm btn-alt-primary">
                        <i class="fa fa-check"></i> Submit
                    </button>
                    <button type="reset" class="btn btn-sm btn-alt-danger">
                        <i class="fa fa-repeat"></i> Reset
                    </button>
                </div>
            </div>
            <div class="block-content">
                <div class="form-group row justify-content-center d-none" id="chapterInfo">
                    <label class="col-sm-8" for="block-form-username3">Add References For Chapter <small class="text-danger h5" id="chapterNo"></small></label>
                    <div class="col-sm-8">
                        <input type="hidden" class="form-control" name="chapter" value="" id="formChapterNo">
                        <input type="hidden" class="form-control"  name="references" value="{{ $referencesTable }}" >

                    </div>

                </div>
                <div class="form-group row justify-content-center">
                    <label class="col-sm-8" for="block-form-password3">Reference Name @if ($errors->first('name'))
                        <span class="text-danger">{{ $errors->first('name') }}</span>
                    @endif</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="block-form-password3" name="name" placeholder="Enter Reference Name...">
                    </div>

                </div>
                <div class="form-group row justify-content-center">
                    <label class="col-sm-8" for="block-form-password3">URL @if ($errors->first('link'))
                        <span class="text-danger">{{ $errors->first('link') }}</span>
                    @endif</label>
                    <div class="col-sm-8">
                        <input type="text" class="form-control" id="block-form-password3" name="link" placeholder="Enter Reference URL...">
                    </div>

                </div>


            </div>
        </div>
    </form>
</div>

<div class="col-md-12">
        @if (!(count($references)>0))


        <!-- Success Alert -->
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">No References Added</h3>
            <p class="mb-0">No references has benn added yet!</p>
        </div>
        <!-- END Success Alert -->
        @endif
</div>
<div class="col-md-12">
        <!-- Striped Table -->
        <div class="block">
            <div class="block-header block-header-default">
                <h3 class="block-title">Chapters Overview</h3>
                <div class="block-options">
                    <div class="block-options-item">
                        <code>Total Chapters {{ count($syllabus) }}</code>
                    </div>
                </div>
            </div>
            <div class="block-content">
                <table class="table table-striped table-vcenter ">
                    <thead>
                        <tr  class="text-left">

                            <th>Chapter No</th>
                            <th class="text-center" >Chapter Name</th>


                        </tr>
                    </thead>
                    <tbody>

                        @foreach ($syllabus as $item)

                        <tr >
                                <td class="text-left">{{ $item->chapter_no }}</td>
                                <td class="text-center">{{ strip_tags($item->chapter_name) }}</td>

                            </tr>
                        @endforeach

                    </tbody>
                </table>
            </div>
        </div>
        <!-- END Striped Table -->
</div>


<div class="block">
    <div class="block-header">
        <div class="block-title">References</div>
    </div>
    <div class="block-content-full">
        <div class=" row">
            @foreach ($references as $item)

            <div class="col-md-12 col-xl-12">
             <a class="block block-link-shadow bg-gray-light" href="javascript:void(0)">
                 <div class="block-content block-content-full clearfix">
                     <div class="float-right">
                         <img class="img-avatar" src="{{ $item->profile_url }}" alt="">
                     </div>
                     <div class="float-left mt-10">
                         <div class="font-w600 mb-5" onclick="window.location.href='/user/profile/{{ $item->user_id }}/{{ csrf_token() }}'">{{ ucwords($item->name) }}</div>
                         <div class="font-size-sm text-muted mb-5">{{ $item->email }}</div>
                         <div class="font-size-sm text-black mb-5"> Chapter : {{ $item->chapter_no }}</div>
                         <div class="font-size-sm text-black">{{ strip_tags($item->ref_name) }}</div>

                     </div>

                 </div>
                 <div class="block-content">
                     <div class="row items-push text-center">
                         <div class="col-12">

                             <button class="btn btn-sm btn-alt-primary" onclick="window.open('{{ $item->link }}','_newtab')"> <i class="fa fa-external-link"> Visit Link</i></button>
                         </div>

                     </div>
                 </div>
             </a>
         </div>
            @endforeach
         </div>
    </div>
</div>

@endsection
