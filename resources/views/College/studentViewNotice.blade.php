@extends('layouts.app')


@section('content')

@foreach ($notice as $item)
   <div class="block">
       <!-- Project -->
       <div class="block-header block-header-default">
            @php
                        $time = strtotime($item->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
        <h3 class="block-title">{{ strip_tags($item->notice_title) }} <small>{{ $new_time }}</small></h3>
    </div>
       <div class="block-content block-content-full">
        <div class="row py-20">
            <div class="col-sm-4 invisible" data-toggle="appear">
                <!-- Image Slider (.js-slider class is initialized in Helpers.slick()) -->
                <!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->
                <div class="js-slider slick-nav-black slick-dotted-inner slick-dotted-white" data-dots="true" data-arrows="true">
                    @if ($item->attachment_id == 1)
                    <div>
                        <img class="img-fluid" src="{{ asset('/assets/media/NoticeImages/noticeimagedefault.png') }}" alt="Notice Image">

                    </div>
                    @else
                    <div>
                        <img class="img-fluid" src="{{ asset($item->attachment_url) }}" alt="Notice Image">
                    </div>
                    @endif

                </div>
                <!-- END Image Slider -->

                <!-- Project Info -->
                <table class="table table-striped table-borderless mt-20">
                    <tbody>
                        <tr>
                            <td class="font-w600">Notice Type</td>
                            <td>{{ $item->notice_type }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Batch</td>
                            <td>{{ $item->batch_no }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Program</td>
                            <td>{{ $item->program_name }}</td>
                        </tr>

                    </tbody>
                </table>
                <!-- END Project Info -->
            </div>
            <div class="col-sm-8 nice-copy">
                <!-- Project Description -->
                <h3 class="mb-10">{{ strip_tags($item->notice_title) }}</h3>
                <div class="text-muted">
                    {!! strip_tags(substr($item->notice_data,0,2000)) !!}<span class="d-none" id="showmore{{ $item->id }}">{!! strip_tags(substr($item->notice_data,2000)) !!}</span> <span><button class="btn btn-sm " id="showMoreButton{{ $item->id }}" onclick="showMore('{{  $item->id}}')">Show More</button></span>

                </div>
            </div>
        </div>
    </div>
    <!-- END Project -->
   </div>
@endforeach

@endsection


