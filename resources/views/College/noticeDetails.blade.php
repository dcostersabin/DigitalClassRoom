@extends('layouts.app')

@section('content8')
@foreach ($notice as $item)
   <div class="block">
       <!-- Project -->
       <div class="block-header block-header-default">
            @php
                        $time = strtotime($item->created_at);
                        $new_time = date('Y M D',$time);
                        @endphp
        <h3 class="block-title">{{ strip_tags($item->notice_title) }} <small>{{ $new_time }}</small></h3>
    </div>
       <div class="block-content block-content-full">
        <div class="row py-20">
            <div class="col-sm-4 invisible" data-toggle="appear">
                <!-- Image Slider (.js-slider class is initialized in Helpers.slick()) -->
                <!-- For more info and examples you can check out http://kenwheeler.github.io/slick/ -->
                <div class="js-slider slick-nav-black slick-dotted-inner slick-dotted-white" data-dots="true" data-arrows="true">
                    @if ($item->attachment_id == 1)
                    <div>
                        <img class="img-fluid" src="{{ asset('/assets/media/NoticeImages/noticeimagedefault.png') }}" alt="Notice Image">

                    </div>
                    @else
                    <div>
                        <img class="img-fluid" src="{{ asset($item->attachment_url) }}" alt="Notice Image">
                    </div>
                    @endif

                </div>
                <!-- END Image Slider -->

                <!-- Project Info -->
                <table class="table table-striped table-borderless mt-20">
                    <tbody>
                        <tr>
                            <td class="font-w600">Notice Type</td>
                            <td>{{ $item->notice_type }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Batch</td>
                            <td>{{ $item->batch_no }}</td>
                        </tr>
                        <tr>
                            <td class="font-w600">Program</td>
                            <td>{{ $item->program_name }}</td>
                        </tr>

                    </tbody>
                </table>
                <!-- END Project Info -->
            </div>
            <div class="col-sm-8 nice-copy">
                <!-- Project Description -->
                <h3 class="mb-10">{{ strip_tags($item->notice_title) }}</h3>
                <div class="text-muted">
                  {!! $item->notice_data !!}

                </div>
            </div>
        </div>
    </div>
    <!-- END Project -->
   </div>
@endforeach

@endsection
@section('content4')


<div class="content">
        <div class="col-md-13">
                <div class="">
                    <div class="block">
                        <div class="block-header">
                            <h3 class="block-title">Views {{ count($viewed_users) }} of {{ $total_users[0]->total_users }}</h3>
                        </div>
                        <div class="block-content">
                                @if (count($viewed_users)>0)
                                <div class="h5 text-muted text-left">
                                        Seen By :
                                    </div>
                                @endif

                            @foreach ($viewed_users as $item)
                           <div class="col-md-13 col-xl-13 ">
                                <a class="block block-link-shadow" href="/user/profile/{{ $item->id }}/{{ csrf_token() }}">
                                    <div class="block-content block-content-full clearfix">
                                        <div class="float-right">
                                            <img class="img-avatar" src="{{ asset($item->profile_url) }}" alt="">
                                        </div>
                                        <div class="float-left mt-10">
                                            <div class="font-w600 mb-5">{{ $item->name }}</div>
                                            <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           @endforeach
                          @if (count($not_viewed)>0)
                          <div class="h5 text-muted text-left">
                                Not Seen By:
                            </div>


                          @endif
                            @foreach ($not_viewed as $item)
                           <div class="col-md-13 col-xl-13 ">
                                <a class="block block-link-shadow" href="/user/profile/{{ $item->id }}/{{ csrf_token() }}">
                                    <div class="block-content block-content-full clearfix bg-gray-light">
                                        <div class="float-right">
                                            <img class="img-avatar" src="{{ asset($item->profile_url) }}" alt="">
                                        </div>
                                        <div class="float-left mt-10">
                                            <div class="font-w600 mb-5">{{ $item->name }}</div>
                                            <div class="font-size-sm text-muted">{{ $item->email }}</div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                           @endforeach
                        </div>
                    </div>
                </div>
</div>


@endsection
