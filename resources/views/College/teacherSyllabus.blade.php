@extends('layouts.app')

@section('content')

@if (count($teaching)>0)
<div class="row gutters-tiny invisible" data-toggle="appear">
    @foreach ($teaching as $item)


    <div class="col-md-6">
        <a class="block block-link-shadow overflow-hidden" href="javascript:void(0)">
            <div class="block-content block-content-full">
                <div class="text-right">
                    <i class="fa fa-book  fa-2x text-corporate" onclick="window.location.href='/syllabus/view/teacher/{{ $item->subject_id }}/{{ csrf_token() }}'">
                      <small>Show Syllabus</small>
                    </i>
                </div>
                <div class="row py-20">
                    <div class="col-6 text-right border-r">
                        <div class="invisible" data-toggle="appear" data-class="animated fadeInLeft">
                            <div class="font-size-h3 font-w600 text-info">{{ $item->subject_code }}</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">{{ $item->name }}</div>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="invisible" data-toggle="appear" data-class="animated fadeInRight">
                            <div class="font-size-h3 font-w600 text-success">{{ $item->semester }}</div>
                            <div class="font-size-sm font-w600 text-uppercase text-muted">Semester</div>
                        </div>
                    </div>
                </div>
            </div>
        </a>
    </div>

    @endforeach
</div>
@else

    <h3>No Subjects Allocated Till Now Please Contact College Administration</h3>

@endif

@endsection
