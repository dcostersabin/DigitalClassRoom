@extends('layouts.app')

@section('content8')
@if ($errors->first('success') != null)
<div class="col-md-13">
    <!-- Success Alert -->
    <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
        <p class="mb-0">{{ $errors->first('success') }}</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif


@if ($errors->first('subject_name') != null || $errors->first('subject_code') != null || $errors->first('program_id') != null || $errors->first('semester') != null )
<div class="col-md-12">
    <!-- Danger Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
        @if ($errors->first('subject_name')!=null)
        <p class="mb-0">{{ $errors->first('subject_name') }}</p><br>
        @endif
        @if ($errors->first('subject_code')!=null)
        <p class="mb-0">{{ $errors->first('subject_code') }}</p><br>
        @endif
        @if ($errors->first('program')!=null)
        <p class="mb-0">{{ $errors->first('program') }}</p><br>
        @endif
        @if ($errors->first('semester')!=null)
        <p class="mb-0">{{ $errors->first('semester') }}</p><br>
        @endif


    </div>
    <!-- END Danger Alert -->
</div>


@endif


    @if (count($subjects)>0)
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Subjects</h3>

        </div>
        <div class="block-content">
            <table class="table table-bordered table-vcenter">
                <thead>
                    <tr>
                        <th class="text-center" style="width: 50px;">#</th>
                        <th>Name</th>
                        <th class="d-none d-sm-table-cell" style="width: 15%;">Code</th>
                        <th class="text-center" style="width: 100px;">Program</th>
                        <th class="text-center" style="width: 100px;">Semester</th>
                        <th class="text-center" style="width: 100px;">Edit</th>
                        <th class="text-center" style="width: 100px;">Delete</th>
                    </tr>
                </thead>
                <tbody>


                        @foreach ($subjects as $item)
                        <tr>
                            @foreach ($item as $data)
                         <th class="text-center" scope="row">{{$data }}</th>

                            @endforeach
                            <th class="text-center" scope="row"><button class="btn btn-sm btn-info">Edit</button></th>
                            <th class="text-center" scope="row"><button class="btn btn-sm btn-danger">Delete</button></th>
                        </tr>
                         {{-- <th class="text-center" scope="row">{{ $item->subject_id }}</th>
                        <th class="text-center" scope="row">{{ $item->name }}</th>
                        <th class="text-center" scope="row">{{ $item->subject_code }}</th>
                        <th class="text-center" scope="row">{{ $item->program_name }}</th>
                        <th class="text-center" scope="row">{{ $item->semester }}</th>  --}}
                        @endforeach





                </tbody>
            </table>
        </div>
    </div>
    @else
    <div class="col-md-13">
        <!-- Success Alert -->
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Alert</h3>
            <p class="mb-0">No Subjects Added To The Database</p>
        </div>
        <!-- END Success Alert -->
    </div>
    @endif
@endsection



@section('content4')
<div class="col-md-13">
    <!-- Normal Form -->
    <div class="block">
        <div class="block-header block-header-default">
            <h3 class="block-title">Add Subject</h3>

        </div>
        <div class="block-content">
            <form action="/add/subject" method="post" >
                @csrf
                <div class="form-group">
                    <label for="example-nf-email">Subject Name</label>
                    <input type="text" class="form-control" id="example-nf-email" name="subject_name" placeholder="Enter Subject Name..">
                </div>
                <div class="form-group">
                    <label for="example-nf-email">Subject Code</label>
                    <input type="text" class="form-control" id="example-nf-email" name="subject_code" placeholder="Enter Subject Code..">
                </div>
                <div class="form-group">
                    <label class="col-13" for="example-select">Select Program</label>
                    <div class="col-md-13">
                        <select class="form-control" id="example-select" name="program">
                            <option value="">Please select A Program</option>
                            @foreach ($programs as $item)
                            @if ($item->program_id != 999)
                            <option value="{{ $item->program_id }}">{{ $item->program_name }}</option>
                            @endif
                            @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-13" for="example-select">Select Semester</label>
                    <div class="col-md-13">
                        <select class="form-control" id="example-select" name="semester">
                            <option value="">Please select A Semester</option>
                           @for ( $i=1 ;  $i<9 ; $i++)
                           <option value="{{ $i }}">{{ $i }}</option>
                           @endfor
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <button type="submit" class="btn btn-alt-primary">Add</button>
                </div>
            </form>
        </div>
    </div>
@endsection

