@extends('layouts.app')


@section('content8')
@if ($errors->first('success')!=null)
<div class="col-md-12">
    <!-- Success Alert -->
    <div class="alert alert-success alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
        <p class="mb-0">Your File Was Uploaded Successfully. If The File Doesnt Appear Please Refresh The Page</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif
@if ($errors->first('error')!=null)
<div class="col-md-12">
    <!-- Success Alert -->
    <div class="alert alert-danger alert-dismissable" role="alert">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        <h3 class="alert-heading font-size-h4 font-w400">Error</h3>
        <p class="mb-0">{{ $errors->first('error') }}</p>
    </div>
    <!-- END Success Alert -->
</div>
@endif


@if ($errors->first('trash') != null)

<div class="col-md-12">
        <!-- Success Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">{{ $errors->first('trash') }}</p>
        </div>
        <!-- END Success Alert -->
    </div>

@endif

@if ($errors->first('trashRemoved') != null)

<div class="col-md-12">
        <!-- Success Alert -->
        <div class="alert alert-success alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Success</h3>
            <p class="mb-0">{{ $errors->first('trashRemoved') }}</p>
        </div>
        <!-- END Success Alert -->
    </div>

@endif


@if (count($file)>0)
<div class="row">
        @foreach ($file as $data)


        <div class="col-md-3">

            <div class="block ">

                <div class="block-header text-black">

                        <div class=" h5 text-black"> {{ $data->user_filename }} </div>

                    </div>
                <div class="col-md-13 ">
                    <div class="options-container">
                        <img class="img-fluid options-item" src="{{ asset($data->icon_url) }}" alt="">

                    </div>
                </div>
                <div class="block-content block-content-full block-content-sm ">


                <div class="block-content block-content-full block-content-sm bg-body-light font-size-sm">


                      <div></div>

                           <div> <button type="button" class="btn btn-sm btn-primary mr-5 mb-5" onclick="personalStorageDownload('{{ $data->id }}')">
                                <i class="fa fa-download mr-5"></i>{{ $data->file_size }}
                            </button>
                            <button type="button" class="btn  btn-sm btn-outline-danger mr-6 mb-5 pull-right" onclick="personalStorageDelete('{{ $data->id }}')">
                                    <i class="fa fa-close"></i>
                                </button></div>

                                <form action="/download/personal/storage" method="POST" id="downloadform{{ $data->id }}">
                                    <input type="hidden" name="id" value="{{ $data->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                                <form action="/moveToTrash/personal/storage" method="POST" id="deleteform{{ $data->id }}">
                                    <input type="hidden" name="id" value="{{ $data->id }}">
                                    <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                </form>
                    </div>
                </div>

            </div>



        </div>


        @endforeach
    </div>

@else
<div class="col-md-12">
        <!-- Success Alert -->
        <div class="alert alert-info alert-dismissable" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
            <h3 class="alert-heading font-size-h4 font-w400">Info</h3>
            <p class="mb-0">You Can Upload Your Academic Content Using Digital ClassRooms's Cloud Facility. Get Started Now...</p>
        </div>
        <!-- END Success Alert -->
    </div>

@endif

@endsection


@section('content4')
<div class="col-md-13">
    <!-- Normal Form -->
    <div class="block">
        <div class="block-header  bg-gray ">
            <h3 class="block-title">Upload File</h3>
            <div class="block-options">
                <button type="button" class="btn-block-option">
                    <i class="si  si-cloud-upload "></i>
                </button>
            </div>
        </div>
        <div class="block-content">
            <form action="/submit/file/personal/storage" method="post" enctype="multipart/form-data">
                @csrf
                <div class="form-group">
                    <label for="example-nf-email">File Name</label>
                    <input type="text" class="form-control" id="example-nf-email" name="filename"
                        placeholder="Enter File Name">
                    <div class="text-danger">
                        @if ($errors->first('filename')!=null)
                        {{ $errors->first('filename') }}
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <input type="file" name="attachment">
                    <input type="hidden" name="user_id" value="{{ auth::user()->id }}">

                    <div class="text-danger">
                        @if ($errors->first('attachment')!=null)
                        {{ $errors->first('attachment') }}
                        @endif
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-alt-primary">Upload</button>
                </div>
            </form>
        </div>
    </div>
</div>

@if (count($file)>0)
<div class="col-md-13">
        <div class="block">
                <div class="block-header block-header-default">
                    <h3 class="block-title">Upload History</h3>
                    <div class="block-options">
                        <div class="block-options-item">
                            <i class="fa fa-cloud"></i>
                        </div>
                    </div>
                </div>
                <div class="block-content">
                    <table class="table table-striped table-vcenter">
                        <thead>
                            <tr>
                                <th class="text-center" style="width: 50px;">#</th>
                                <th >Name</th>
                                <th class="d-none d-sm-table-cell" style="width: 35%;">Uploaded At</th>
                                <th class="text-center" style="width: 100px;">Size</th>
                                <th class="text-center" style="width: 100px;">Edit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @php
                                $count = 1;
                            @endphp
                            @foreach ($file as $item)
                            <tr>
                                    <th class="text-center" scope="row">{{ $count }}</th>
                                    @php
                                        $count ++;
                                    @endphp
                                    <td>{{ $item->user_filename }}</td>
                                    <td class="d-none d-sm-table-cell">
                                            @php
                                            $time = strtotime($item->created_at);
                                            $new_time = date('Y M D',$time);
                                            @endphp
                                        <span class="badge badge-secondary">{{ $new_time }}</span>
                                    </td>
                                    <td>{{ $item->file_size }}</td>
                                    <td class="text-center">
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-sm btn-primary" data-toggle="tooltip" title="Download" onclick="personalStorageDownload('{{ $item->id }}')">
                                                <i class="fa fa-download"></i>
                                            </button>
                                            <button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Move To Trash" onclick="personalStorageDelete('{{ $data->id }}')">
                                                <i class="fa fa-times"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
</div>
@endif
    @if (count($trashed)>0)
    <div class="col-md-13">
            <div class="block">
                    <div class="block-header block-header-default">
                        <h3 class="block-title">Trash </h3>
                        <div class="block-options">
                            <div class="block-options-item">
                                <i class="fa  fa-trash "></i>
                            </div>
                        </div>
                    </div>
                    <div class="block-content">
                        <table class="table table-striped table-vcenter">
                            <thead>
                                <tr>
                                    <th class="text-center" style="width: 50px;">#</th>
                                    <th >Name</th>
                                    <th class="d-none d-sm-table-cell" style="width: 35%;">Uploaded At</th>
                                    <th class="text-center" style="width: 100px;">Size</th>
                                    <th class="text-center" style="width: 100px;">Edit</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $count = 1;
                                @endphp
                                @foreach ($trashed as $item)
                                <tr>
                                        <th class="text-center" scope="row">{{ $count }}</th>
                                        @php
                                            $count ++;
                                        @endphp
                                        <td>{{ $item->user_filename }}</td>
                                        <td class="d-none d-sm-table-cell">
                                                @php
                                                $time = strtotime($item->created_at);
                                                $new_time = date('Y M D',$time);
                                                @endphp
                                            <span class="badge badge-secondary">{{ $new_time }}</span>
                                        </td>
                                        <td>{{ $item->file_size }}</td>
                                        <td class="text-center">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-success" data-toggle="tooltip" title="Restore" onclick="restoreForm('{{ $item->id }}')">
                                                    <i class="si si-refresh"></i>
                                                </button>
                                                <button type="button" class="btn btn-sm btn-danger" data-toggle="tooltip" title="Delete From Trash" onclick="removeTrashForm('{{ $item->id }}')">
                                                    <i class="fa fa-times"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    <form action="/restore/personal/storage" method="POST" id="restoreForm{{ $item->id }}">
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                    <form action="/delete/personal/storage" method="POST" id="removeTrashForm{{ $item->id }}">
                                        <input type="hidden" name="id" value="{{ $item->id }}">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                    </form>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
    </div>
    @endif
@endsection
